import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:torange/features/symptoms/presentation/cubit/symptomui_cubit.dart';
import '../../features/symptoms/presentation/bloc/symptom_bloc.dart';
import '../../features/symptoms/presentation/pages/main_symptoms.dart';

import '../../features/appointment/presentation/bloc/appointment_bloc.dart';
import '../../features/appointment/presentation/pages/appointments_page.dart';
import '../../features/authentication/presentation/cubits/page_switcher/page_switcher_cubit.dart';
import '../../features/authentication/presentation/pages/landing_page.dart';
import '../../injection_container.dart';
import '../../features/authentication/data/data_sources/authentication_local_data_source.dart';

class AppRouter {
  Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case LandingPage.pageRoute:
        return _landingRoute(settings);
      case AppointmentsPage.routeName:
        return _appointmentsPageRoute(settings);
      case AddAppointmentPage.routeName:
        return _addAppointmentRoute(settings);
      // case AddAppointmentSecondPage.routeName:
      //   return _addAppointmentSecondPageRoute(settings);
    }
    return sl<AuthenticationLocalDataSource>().isLogged()
        ? _appointmentsPageRoute(settings)
        : _landingRoute(settings);
  }

  PageRoute _landingRoute(RouteSettings settings) => _getRoute(
        builder: (_) => BlocProvider<PageSwitcherCubit>(
          create: (_) => sl<PageSwitcherCubit>(),
          child: LandingPage(isForLogin: settings.arguments),
        ),
        settings: settings,
      );

  PageRoute _appointmentsPageRoute(RouteSettings settings) => _getRoute(
        builder: (_) => BlocProvider<AppointmentBloc>(
          create: (context) => sl<AppointmentBloc>(),
          child: AppointmentsPage(),
        ),
        settings: settings,
      );

  PageRoute _addAppointmentRoute(RouteSettings settings) => _getRoute(
        builder: (_) => MultiBlocProvider(
          providers: [
            BlocProvider<SymptomBloc>(
              create: (context) => sl<SymptomBloc>(),
            ),
            BlocProvider<SymptomuiCubit>(
              create: (context) => sl<SymptomuiCubit>(),
            ),
          ],
          child: AddAppointmentPage(),
        ),
        settings: settings,
      );

  // PageRoute _addAppointmentSecondPageRoute(RouteSettings settings) => _getRoute(
  //       builder: (_) => MultiBlocProvider(
  //         providers: [
  //           BlocProvider<SymptomsBloc>.value(
  //             value: _symptomsBloc,
  //           ),
  //           BlocProvider<SymptomsUiCubit>.value(
  //             value: _symptomsUiCubit,
  //           ),
  //           BlocProvider<AddAppointmentBloc>(
  //             create: (context) => AddAppointmentBloc(
  //               appointmentRepository,
  //               internetCubit,
  //             ),
  //           ),
  //         ],
  //         child: AddAppointmentSecondPage(symptoms: settings.arguments),
  //       ),
  //       settings: settings,
  //     );

  PageRoute _getRoute(
          {@required WidgetBuilder builder, RouteSettings settings}) =>
      Platform.isIOS
          ? CupertinoPageRoute(builder: builder, settings: settings)
          : MaterialPageRoute(builder: builder, settings: settings);
}
