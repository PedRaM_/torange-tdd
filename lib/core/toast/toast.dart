import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../components/text/my_text.dart';
import '../utils/tools.dart';
import '../utils/values.dart';

class MyToast {
  static void showToast({
    @required BuildContext context,
    @required String message,
    Color backgroundColor,
    Color textColor,
    double fontSize,
    bool isFromBottom = true,
    Duration duration,
  }) {
    FToast toast = FToast();
    toast.init(context);
    toast.showToast(
      child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(32),
            color: backgroundColor ?? Color(0xEE777777),
          ),
          padding: EdgeInsets.only(top: 8, bottom: 8, right: 16, left: 16),
          child: MyText(
            text: message,
            fontSize: fontSize ?? 12,
            color: textColor ?? Colors.white,
            fontWeight: MyFonts.normal,
            fontFamily: MyFonts.iranSansFarsi,
            textAlign: TextAlign.center,
            textOverflow: TextOverflow.clip,
          )),
      toastDuration: duration ?? Duration(seconds: 3),
      positionedToastBuilder: (context, child) => Positioned(
          bottom: isFromBottom ? 42.0 : null,
          top: isFromBottom ? null : 42.0,
          left: 24.0,
          right: 24.0,
          child: child),
    );
  }

  static void showErrorToast({
    @required BuildContext context,
    @required String message,
    Duration duration,
    bool isFromBottom = false,
  }) {
    FToast toast = FToast();
    toast.init(context);
    toast.showToast(
      child: TweenAnimationBuilder<double>(
        duration: Duration(milliseconds: 300),
        curve: Curves.ease,
        tween: Tween<double>(begin: 0.0, end: 75.0),
        builder: (context, value, child) {
          return Container(
            height: value,
            width: double.infinity,
            child: Align(
              alignment:
                  isFromBottom ? Alignment.topCenter : Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(32),
                  color: Color(0xEEFAFAFA),
                ),
                padding:
                    EdgeInsets.only(top: 6, bottom: 8, right: 12, left: 12),
                child: Row(
                  textDirection: Tools.textDirection,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.warning_rounded,
                      color: Colors.redAccent,
                      size: 24,
                    ),
                    Flexible(
                      child: Padding(
                        padding: EdgeInsets.only(right: 4, top: 6),
                        child: MyText(
                          text: message,
                          textOverflow: TextOverflow.clip,
                          textAlign: TextAlign.start,
                          color: Colors.red[300],
                          fontSize: 12,
                          fontWeight: MyFonts.normal,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
      toastDuration: duration ?? Duration(seconds: 3),
      positionedToastBuilder: (context, child) => Positioned(
          bottom: isFromBottom ? 0.0 : null,
          top: isFromBottom ? null : 0.0,
          left: 24.0,
          right: 24.0,
          child: child),
    );
  }
}
