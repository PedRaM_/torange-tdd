import 'package:flutter/material.dart';
import '../../utils/values.dart';

class Loading extends StatelessWidget {
  final Color progressColor;
  final double width;

  const Loading(
      {Key key, this.progressColor = MyColors.spaceCadet, this.width = 3})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _myLoading();
  }

  Widget _myLoading() => Center(
        child: GestureDetector(
          onTap: () {},
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(progressColor),
            strokeWidth: width,
          ),
        ),
      );
}
