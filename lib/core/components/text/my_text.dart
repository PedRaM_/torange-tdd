import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../utils/tools.dart';

class MyText extends StatelessWidget {
  final String text;
  final Color color;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final double fontSize;
  final int maxLine;
  final bool hasUnderLine;
  final String fontFamily;
  final bool isEnglish;
  final TextOverflow textOverflow;
  final TextDecoration textDecoration;

  const MyText({
    Key key,
    @required this.text,
    this.color,
    this.fontWeight,
    this.fontSize = 13,
    this.textAlign,
    this.hasUnderLine = false,
    this.maxLine,
    this.textOverflow = TextOverflow.ellipsis,
    this.fontFamily,
    this.isEnglish = false,
    this.textDecoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      isEnglish ? Tools.farsiToEnglish(text) : Tools.englishToFarsi(text),
      maxLines: maxLine,
      textDirection: Tools.textDirection,
      textAlign: textAlign,
      overflow: textOverflow,
      style: TextStyle(
        decoration: hasUnderLine
            ? TextDecoration.underline
            : textDecoration == null
                ? TextDecoration.none
                : textDecoration,
        fontSize: fontSize,
        fontFamily: fontFamily ?? Tools.fontFamily,
        fontWeight: fontWeight,
        color: color,
      ),
    );
  }
}
