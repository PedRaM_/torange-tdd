import 'package:flutter/material.dart';

class BaseDialog extends StatelessWidget {
  final Widget child;
  final bool closeOnBackPressed;
  static const double _padding = 24;

  const BaseDialog({
    Key key,
    this.child,
    this.closeOnBackPressed = true,
  }) : super(key: key);

  Future<bool> _onBackPressed(context) async {
    if (closeOnBackPressed) Navigator.of(context).pop(true); // To close
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => _onBackPressed(context),
      child: GestureDetector(
        onTap: () {
          if (closeOnBackPressed) Navigator.of(context).pop(true); // To close
        },
        child: Dialog(
          insetPadding: EdgeInsets.all(0),
          insetAnimationCurve: Curves.ease,
          elevation: 0,
          backgroundColor: Colors.transparent,
          insetAnimationDuration: Duration(milliseconds: 300),
          child: dialogContent(context),
        ),
      ),
    );
  }

  dialogContent(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Align(
        alignment: Alignment.center,
        child: Container(
          margin: EdgeInsets.only(right: 16, left: 16),
          padding: EdgeInsets.only(top: (_padding) / 2, bottom: _padding / 2),
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height * 0.6,
            maxWidth: MediaQuery.of(context).size.width - 32,
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8), color: Colors.white),
          child: child,
        ),
      ),
    );
  }
}
