import 'package:flutter/material.dart';
import 'base_dialog.dart';
import '../text/my_text.dart';
import '../../utils/tools.dart';
import '../../utils/values.dart';

class TwoActionDialog {
  TwoActionDialog._();

  static void show(
    BuildContext context, {
    @required Function onAcceptClick,
    Function onDismiss,
  }) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) => TwoActionDialogFragment(
        onSubmit: onAcceptClick,
      ),
    ).then((result) {
      if (result != null && result) {
        if (onDismiss != null) onDismiss();
      }
    });
  }
}

class TwoActionDialogFragment extends StatelessWidget {
  final Function onSubmit;

  static const double padding = 16;

  TwoActionDialogFragment({
    this.onSubmit,
  });

  @override
  Widget build(BuildContext context) {
    return _dialogContent(context);
  }

  Widget _dialogContent(BuildContext context) {
    return BaseDialog(child: child(context));
  }

  Widget child(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        textDirection: Tools.textDirection,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Flexible(
            child: Padding(
              padding: EdgeInsets.only(top: 8, left: 16, right: 16),
              child: MyText(
                text: 'آیا از خروج از حساب خود اطمینان دارید؟',
                color: MyColors.spaceCadet,
                fontSize: 12,
                textAlign: TextAlign.start,
                textOverflow: TextOverflow.clip,
                fontWeight: MyFonts.medium,
              ),
            ),
          ),
          SizedBox(
            height: 14.0,
            width: double.infinity,
          ),
          Row(
            textDirection: Tools.textDirection,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              _cancelButton(context),
              _submitButton(context),
            ],
          )
        ],
      );

  Widget _submitButton(BuildContext context) => Padding(
        padding: EdgeInsets.only(left: 8, right: 0),
        child: FlatButton(
          onPressed: () {
            if (onSubmit != null) onSubmit();
            Navigator.of(context).pop(true);
          },
          child: MyText(
            text: 'تایید',
            color: MyColors.spaceCadet,
            fontSize: 12,
            fontWeight: MyFonts.medium,
          ),
        ),
      );

  Widget _cancelButton(BuildContext context) => FlatButton(
        onPressed: () => Navigator.of(context).pop(false),
        child: MyText(
          text: 'انصراف',
          color: Color(0xFFacacac),
          fontSize: 12,
          fontWeight: MyFonts.medium,
        ),
      );
}
