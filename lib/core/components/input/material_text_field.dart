import 'package:flutter/material.dart';
import '../../utils/values.dart';

class MaterialTextField extends StatelessWidget {
  final FocusNode focusNode;
  final FocusNode nextFocusNode;
  final TextInputAction textInputAction;
  final TextDirection textDirection;
  final String errorText;
  final TextEditingController controller;
  final bool isPassword;
  final String label;
  final bool isEnglish;
  final double fontSize;
  final double hintFontSize;
  final FontWeight fontWeight;
  final TextInputType inputType;
  final Color hintColor;
  final Color textColor;
  final Color borderColor;
  final int maxLength;
  final bool isHintEnglish;
  final bool isTextEnglish;
  final bool isLanguageEnglish;
  final Function(String) onChange;
  final TextCapitalization textCapitalization;
  final Function onTap;
  final bool hasInteractive;
  final Widget icon;
  final bool isDense;
  final int maxLine;
  final TextAlignVertical textAlignVertical;
  final Function onSubmit;
  final bool handleOnSubmit;
  final String hint;

  const MaterialTextField({
    Key key,
    this.focusNode,
    this.errorText,
    this.nextFocusNode,
    this.isPassword = false,
    this.textInputAction,
    this.label = "",
    this.controller,
    this.isEnglish = false,
    this.fontSize = 14,
    this.hintFontSize = 14,
    this.fontWeight = FontWeight.w400,
    this.inputType = TextInputType.visiblePassword,
    this.isHintEnglish = false,
    this.isTextEnglish = false,
    this.isLanguageEnglish = false,
    this.hintColor = MyColors.hintTextColor,
    this.textColor = MyColors.combuGreen,
    this.borderColor = MyColors.combuGreen,
    this.maxLength = 50,
    this.onChange,
    this.textCapitalization,
    this.icon,
    this.hasInteractive = true,
    this.onTap,
    this.textDirection = TextDirection.rtl,
    this.isDense = false,
    this.maxLine = 1,
    this.textAlignVertical = TextAlignVertical.center,
    this.onSubmit,
    this.handleOnSubmit = false,
    this.hint,
  }) : super(key: key);

  _errorSection({String error = ""}) => Positioned(
        bottom: 0,
        left: 10,
        child: Transform.translate(
          offset: Offset(0, 4),
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 6, right: 6),
              child: Text(
                error,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: MyColors.errorColor,
                  fontFamily: MyFonts.yekan,
                  fontSize: 9,
                  fontWeight: MyFonts.normal,
                ),
              ),
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Center(
          child: Directionality(
            textDirection: textDirection,
            child: TextField(
              enableInteractiveSelection: hasInteractive,
              obscureText: isPassword,
              textCapitalization: textCapitalization ?? TextCapitalization.none,
              cursorColor: borderColor,
              cursorRadius: Radius.circular(16),
              cursorWidth: 2,
              onTap: onTap,
              textDirection:
                  isLanguageEnglish ? TextDirection.ltr : TextDirection.rtl,
              keyboardType: inputType,
              focusNode: focusNode,
              onChanged: (text) {
                if (onChange != null) onChange(text);
              },
              onSubmitted: (text) {
                if (onSubmit != null) onSubmit();
                if (!handleOnSubmit) {
                  if (nextFocusNode == null) {
                    FocusScope.of(context).unfocus();
                    return;
                  }
                  FocusScope.of(context).requestFocus(nextFocusNode);
                }
              },
              controller: controller,
              textInputAction: textInputAction,
              textAlign: TextAlign.start,
              maxLength: maxLength,
              maxLines: maxLine,
              textAlignVertical: textAlignVertical,
              style: TextStyle(
                color: textColor,
                fontFamily:
                    isLanguageEnglish ? MyFonts.roboto : MyFonts.iranSansFarsi,
                fontSize: fontSize,
                fontWeight: fontWeight,
              ),
              decoration: _MyTextDecoration.textFieldStyle(
                  label: label,
                  hintColor: hintColor,
                  fontSize: hintFontSize,
                  isDense: isDense,
                  fontWeight: fontWeight,
                  icon: icon,
                  borderColor: borderColor,
                  hasFocus: focusNode.hasFocus,
                  hint: hint,
                  hasError: errorText != ""),
            ),
          ),
        ),
        errorText == "" ? SizedBox() : _errorSection(error: errorText),
      ],
    );
  }
}

class _MyTextDecoration {
  static InputDecoration textFieldStyle({
    String label,
    bool isDense = false,
    hintColor = MyColors.hintTextColor,
    borderColor = MyColors.combuGreen,
    FontWeight fontWeight = MyFonts.normal,
    double fontSize = 14,
    bool hasFocus = false,
    Widget icon,
    String hint,
    bool isEnglish = false,
    bool hasError = false,
  }) {
    return InputDecoration(
      alignLabelWithHint: true,
      labelText: label,
      suffixIcon: icon,
      counterText: "",
      isDense: isDense,
      contentPadding: EdgeInsets.only(top: 16, bottom: 16, right: 16, left: 16),
      labelStyle: TextStyle(
        color: hasError
            ? MyColors.errorColor
            : hasFocus
                ? MyColors.amazon
                : hintColor,
        fontFamily: isEnglish ? MyFonts.roboto : MyFonts.yekan,
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
      hintStyle: TextStyle(
        color: hasError
            ? MyColors.errorColor
            : hasFocus
                ? MyColors.combuGreen
                : hintColor,
        fontFamily: isEnglish ? MyFonts.roboto : MyFonts.yekan,
        fontSize: fontSize,
        fontWeight: fontWeight,
      ),
      hintText: hint ?? '',
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: hasError ? MyColors.errorColor : borderColor,
        ),
        borderRadius: BorderRadius.circular(6),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
            color: hasError ? MyColors.errorColor : MyColors.amazon, width: 2),
        borderRadius: BorderRadius.circular(6),
      ),
    );
  }
}
