import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../text/my_text.dart';
import '../../utils/tools.dart';
import '../../utils/values.dart';

class MyScaffold extends StatelessWidget {
  final Widget drawer;
  final Color backgroundColor;
  final bool hasAppbar;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Widget body;
  final BottomNavigationBar bottomNavigationBar;
  final Widget titleWidget;
  final String titleName;
  final Function onFloatingActionButtonClick;
  final Function onBackPressed;
  final double elevation;
  final Widget fabChild;
  final bool hasBackArrow;
  final List<Widget> tabs;
  final Function(int) onTabClick;
  final TabController tabController;
  final Function(GlobalKey) onMoreClick;

  MyScaffold({
    Key key,
    this.scaffoldKey,
    this.drawer,
    @required this.body,
    this.titleName,
    this.titleWidget,
    this.bottomNavigationBar,
    this.elevation = 4,
    this.onFloatingActionButtonClick,
    this.fabChild,
    this.hasAppbar = true,
    this.hasBackArrow = true,
    this.backgroundColor,
    this.onBackPressed,
    this.tabs,
    this.onTabClick,
    this.tabController,
    this.onMoreClick,
  }) : super(key: key);

  static const double BOTTOM_NAVIGATION_HEIGHT = 56;
  final GlobalKey _moreKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return _scaffold;
  }

  Widget get _scaffold => Directionality(
        textDirection: Tools.textDirection,
        child: Scaffold(
          drawer: drawer,
          backgroundColor: backgroundColor,
          key: scaffoldKey,
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
          floatingActionButton: fabChild ?? SizedBox(),
          appBar: !hasAppbar ? null : _appBar,
          body: body,
          bottomNavigationBar: _bottomNavigation,
        ),
      );

  Widget get _appBar => AppBar(
        elevation: elevation,
        title: _title,
        backgroundColor: MyColors.amazon,
        brightness: Brightness.dark,
        iconTheme: IconThemeData(color: Colors.white),
        centerTitle: true,
        leading: onMoreClick == null
            ? SizedBox()
            : Builder(
                builder: (context) => _moreButton,
              ),
        actions: [
          _backButton,
        ],
        bottom: tabs != null
            ? TabBar(
                onTap: onTabClick,
                controller: tabController,
                labelColor: Colors.white,
                unselectedLabelColor: MyColors.unSelectedLabelColor,
                indicatorColor: Colors.white,
                indicatorWeight: 1.5,
                labelStyle: TextStyle(
                  fontWeight: MyFonts.medium,
                  fontSize: 13,
                  color: Colors.white,
                  fontFamily: MyFonts.yekan,
                ),
                unselectedLabelStyle: TextStyle(
                  fontWeight: MyFonts.normal,
                  color: MyColors.amazon,
                  fontSize: 13,
                  fontFamily: MyFonts.yekan,
                ),
                tabs: [for (var tab in tabs) tab],
              )
            : null,
      );

  Widget get _title => AnimatedSwitcher(
        switchInCurve: Curves.ease,
        switchOutCurve: Curves.ease,
        duration: Duration(milliseconds: 250),
        child: titleWidget == null
            ? MyText(
                text: titleName,
                color: Colors.white,
                fontSize: 18,
                fontWeight: MyFonts.bold,
              )
            : titleWidget,
      );

  Widget get _backButton => Padding(
        padding: EdgeInsets.all(8),
        child: SizedBox(
          width: 36,
          height: 36,
          child: AnimatedSwitcher(
            switchInCurve: Curves.ease,
            switchOutCurve: Curves.ease,
            duration: Duration(milliseconds: 250),
            child: hasBackArrow
                ? Builder(
                    builder: (context) => InkWell(
                      customBorder: CircleBorder(),
                      onTap: () {
                        if (onBackPressed == null)
                          Navigator.of(context).pop();
                        else
                          onBackPressed();
                      },
                      child: Center(
                        child: Icon(
                          MyIcons.back,
                          color: Colors.white,
                          size: 16,
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ),
        ),
      );

  Widget get _moreButton => Padding(
        padding: EdgeInsets.all(8),
        child: SizedBox(
          width: 36,
          height: 36,
          child: AnimatedSwitcher(
            switchInCurve: Curves.ease,
            switchOutCurve: Curves.ease,
            duration: Duration(milliseconds: 250),
            child: onMoreClick != null
                ? Builder(
                    builder: (context) => InkWell(
                      customBorder: CircleBorder(),
                      key: _moreKey,
                      onTap: () => onMoreClick(_moreKey),
                      child: Center(
                        child:
                            Icon(Icons.more_vert_rounded, color: Colors.white),
                      ),
                    ),
                  )
                : SizedBox(),
          ),
        ),
      );

  Widget get _bottomNavigation => SizedBox(
        height: bottomNavigationBar != null
            ? MyScaffold.BOTTOM_NAVIGATION_HEIGHT
            : 0,
        child: bottomNavigationBar,
      );
}
