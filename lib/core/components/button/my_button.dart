import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../loading/loading.dart';
import '../text/my_text.dart';
import '../../utils/values.dart';

class MyButton extends StatelessWidget {
  final bool loading;
  final double width;
  final double btnHeight;
  final Function onClick;
  final List<Color> colors;
  final Color textColor;
  final String text;
  final Duration duration;
  final Curve curve;
  final bool isEnglish;

  const MyButton({
    Key key,
    this.text,
    this.loading = false,
    this.width,
    this.btnHeight = 52,
    this.onClick,
    this.colors = const [MyColors.spaceCadet, MyColors.spaceCadet],
    this.textColor = Colors.white,
    this.duration = const Duration(milliseconds: 200),
    this.curve = Curves.ease,
    this.isEnglish = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: btnHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(32),
        gradient: LinearGradient(
          begin: isEnglish ? Alignment.centerLeft : Alignment.centerRight,
          end: isEnglish ? Alignment.centerRight : Alignment.centerLeft,
          colors: colors,
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: onClick == null ? null : () => onClick(),
          borderRadius: BorderRadius.circular(32),
          child: Center(
            child: Padding(
              padding: EdgeInsets.only(left: 16, right: 16),
              child: AnimatedCrossFade(
                duration: duration,
                sizeCurve: curve,
                secondCurve: curve,
                firstCurve: curve,
                crossFadeState: loading
                    ? CrossFadeState.showFirst
                    : CrossFadeState.showSecond,
                firstChild: SizedBox(
                  width: 32,
                  height: 32,
                  child: Center(
                    child: Padding(
                      padding: EdgeInsets.all(4),
                      child: Loading(
                        progressColor: textColor,
                        width: 2,
                      ),
                    ),
                  ),
                ),
                secondChild: MyText(
                  text: text,
                  isEnglish: isEnglish,
                  fontFamily: isEnglish ? MyFonts.roboto : MyFonts.yekan,
                  fontWeight: MyFonts.medium,
                  textOverflow: TextOverflow.fade,
                  maxLine: 1,
                  fontSize: 15,
                  color: textColor,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
