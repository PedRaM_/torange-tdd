import 'package:flutter/material.dart';
import '../../utils/input_converter.dart';
import '../text/my_text.dart';
import '../../utils/values.dart';

class GenderPickerFragment extends StatelessWidget {
  const GenderPickerFragment({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(8), topLeft: Radius.circular(8)),
          color: Colors.white),
      child: SingleChildScrollView(
        child: Column(
          textDirection: TextDirection.rtl,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom: 8),
              child: MyText(
                text: 'انتخاب جنسیت',
                color: MyColors.combuGreen,
                fontSize: 24,
                fontWeight: MyFonts.bold,
              ),
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop(MALE);
                },
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 12, bottom: 12, left: 16, right: 16),
                  child: MyText(
                    text: 'مرد',
                    color: MyColors.combuGreen,
                    fontSize: 14,
                    fontWeight: MyFonts.medium,
                  ),
                ),
              ),
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop(FEMALE);
                },
                child: Padding(
                  padding:
                      EdgeInsets.only(top: 12, bottom: 12, left: 16, right: 16),
                  child: MyText(
                    text: 'زن',
                    color: MyColors.combuGreen,
                    fontSize: 14,
                    fontWeight: MyFonts.medium,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
