import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../text/my_text.dart';
import '../../utils/values.dart';

class ErrorState extends StatelessWidget {
  final String message;
  final String actionText;
  final Function onActionClick;
  final Size size;
  final Color backgroundColor;

  const ErrorState({
    Key key,
    this.message,
    this.onActionClick,
    this.actionText = "تلاش مجدد",
    this.size,
    this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor != null ? backgroundColor : Color(0xFFF8F8F8),
      width: size != null ? size.width : MediaQuery.of(context).size.width,
      height: size != null ? size.height : MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: SvgPicture.asset(
              'assets/svgs/error.svg',
              width:
                  size != null ? size.width : MediaQuery.of(context).size.width,
              height:
                  size != null ? size.width : MediaQuery.of(context).size.width,
            ),
          ),
          Flexible(
            child: Padding(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
              child: MyText(
                text: message,
                color: MyColors.hintTextColor,
                fontSize: 15,
                textAlign: TextAlign.center,
                textOverflow: TextOverflow.clip,
                fontWeight: MyFonts.medium,
              ),
            ),
          ),
          if (onActionClick != null)
            Flexible(
              child: FlatButton(
                onPressed: () => onActionClick(),
                color: MyColors.amazon,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(32),
                ),
                child: ConstrainedBox(
                  constraints: BoxConstraints(minWidth: 75),
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: 12, bottom: 12, right: 16, left: 16),
                    child: MyText(
                      text: actionText,
                      color: Colors.white,
                      textAlign: TextAlign.center,
                      fontWeight: MyFonts.bold,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}
