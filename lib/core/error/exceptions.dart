class ServerException implements Exception {
  final String message;

  ServerException({this.message});
}

class AuthenticationException implements Exception {}

class AuthorizationException implements Exception {}

class CachedException implements Exception {}
