import 'package:meta/meta.dart';

import '../../features/authentication/data/data_sources/authentication_local_data_source.dart';
import '../../features/authentication/data/models/refresh_token_body_model.dart';
import '../../features/authentication/domain/entities/login_result.dart';
import '../data_sources/data_sources.dart';
import '../error/exceptions.dart';

abstract class Repository {
  Future<LoginResult> refreshToken(LoginResult oldTokenInfo);
}

class RepositoryImpl extends Repository {
  final AuthenticationLocalDataSource localAuthDS;
  final DataSources dataSources;

  RepositoryImpl({
    @required this.localAuthDS,
    @required this.dataSources,
  });
  @override
  Future<LoginResult> refreshToken(LoginResult oldTokenInfo) async {
    try {
      await localAuthDS.setRefreshing(true);
      final newToken = await dataSources.refreshToken(RefreshTokenBodyModel(
        accessToken: oldTokenInfo.accessToken,
        refreshToken: oldTokenInfo.refreshToken,
        username: oldTokenInfo.username,
      ));
      await localAuthDS.saveLoginResult(newToken);
      await localAuthDS.setRefreshing(false);
      return newToken;
    } on AuthenticationException {
      _handleError();
      throw AuthenticationException();
    } on AuthorizationException {
      _handleError();
      throw AuthorizationException();
    } on ServerException {
      _handleError();
      throw ServerException();
    }
  }

  void _handleError() async {
    await localAuthDS.setRefreshing(false);
    await localAuthDS.setLogged(false);
  }
}
