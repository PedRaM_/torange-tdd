import 'dart:convert';

import '../error/exceptions.dart';
import '../models/base_response_model.dart';
import '../../features/authentication/data/models/login_result_model.dart';
import '../../features/authentication/data/models/refresh_token_body_model.dart';
import '../../features/authentication/domain/entities/login_result.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class DataSources {
  /// Calls the http://torange.mubabol.ac.ir/api/login/GetAppToken endpoint
  /// with contains [Username] and [AccessToken] and [RefreshToken] in the body to get Token.
  ///
  /// Throws a [ServerException] for all error code.
  Future<LoginResult> refreshToken(RefreshTokenBodyModel body);
}

class DataSourcesImpl implements DataSources {
  final http.Client client;

  DataSourcesImpl({@required this.client});

  @override
  Future<LoginResult> refreshToken(RefreshTokenBodyModel body) async {
    final response = await client.post(
      'http://torange.mubabol.ac.ir/api/login/GetAppToken',
      headers: {
        'Content-Type': 'application/json',
      },
      encoding: Encoding.getByName('utf-8'),
      body: jsonEncode(body.toJson()),
    );
    if (response.statusCode == 200) {
      final baseResponseModel = BaseResponseModel<LoginResultModel>.fromJson(
          json.decode(response.body),
          (data) => LoginResultModel.fromJson(data));
      if (baseResponseModel.isSuccessful)
        return baseResponseModel.data;
      else
        throw AuthenticationException();
    } else
      throw ServerException();
  }
}
