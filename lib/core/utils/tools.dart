import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:persian_datepicker/persian_datetime.dart';
import 'values.dart';

class CountingUtils {
  String letter;
  String count;

  CountingUtils({this.letter = '', this.count = '0'});
}

class Tools {
  static Map<String, String> _utiTable = {
    ".rtf": "public.rtf",
    ".txt": "public.plain-text",
    ".html": "public.html",
    ".htm": "public.html",
    ".xml": "public.xml",
    ".tar": "public.tar-archive",
    ".gz": "org.gnu.gnu-zip-archive",
    ".gzip": "org.gnu.gnu-zip-archive",
    ".tgz": "org.gnu.gnu-zip-tar-archive",
    ".jpg": "public.jpeg",
    ".jpeg": "public.jpeg",
    ".png": "public.png",
    ".avi": "public.avi",
    ".mpg": "public.mpeg",
    ".mpeg": "public.mpeg",
    ".mp4": "public.mpeg-4",
    ".3gpp": "public.3gpp",
    ".3gp": "public.3gpp",
    ".mp3": "public.mp3",
    ".zip": "com.pkware.zip-archive",
    ".gif": "com.compuserve.gif",
    ".bmp": "com.microsoft.bmp",
    ".ico": "com.microsoft.ico",
    ".doc": "com.microsoft.word.doc",
    ".xls": "com.microsoft.excel.xls",
    ".ppt": "com.microsoft.powerpoint.​ppt",
    ".wav": "com.microsoft.waveform-​audio",
    ".wm": "com.microsoft.windows-​media-wm",
    ".wmv": "com.microsoft.windows-​media-wmv",
    ".pdf": "com.adobe.pdf"
  };

  static Map<String, String> _intentTable = {
    ".3gp": "video/3gpp",
    ".apk": "application/vnd.android.package-archive",
    ".asf": "video/x-ms-asf",
    ".avi": "video/x-msvideo",
    ".bin": "application/octet-stream",
    ".bmp": "image/bmp",
    ".c": "text/plain",
    ".class": "application/octet-stream",
    ".conf": "text/plain",
    ".cpp": "text/plain",
    ".doc": "application/msword",
    ".docx":
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    ".xls": "application/vnd.ms-excel",
    ".xlsx":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    ".exe": "application/octet-stream",
    ".gif": "image/gif",
    ".gtar": "application/x-gtar",
    ".gz": "application/x-gzip",
    ".h": "text/plain",
    ".htm": "text/html",
    ".html": "text/html",
    ".jar": "application/java-archive",
    ".java": "text/plain",
    ".jpeg": "image/jpeg",
    ".jpg": "image/jpeg",
    ".js": "application/x-javascript",
    ".log": "text/plain",
    ".m3u": "audio/x-mpegurl",
    ".m4a": "audio/mp4a-latm",
    ".m4b": "audio/mp4a-latm",
    ".m4p": "audio/mp4a-latm",
    ".m4u": "video/vnd.mpegurl",
    ".m4v": "video/x-m4v",
    ".mov": "video/quicktime",
    ".mp2": "audio/x-mpeg",
    ".mp3": "audio/x-mpeg",
    ".mp4": "video/mp4",
    ".mpc": "application/vnd.mpohun.certificate",
    ".mpe": "video/mpeg",
    ".mpeg": "video/mpeg",
    ".mpg": "video/mpeg",
    ".mpg4": "video/mp4",
    ".mpga": "audio/mpeg",
    ".msg": "application/vnd.ms-outlook",
    ".ogg": "audio/ogg",
    ".pdf": "application/pdf",
    ".png": "image/png",
    ".pps": "application/vnd.ms-powerpoint",
    ".ppt": "application/vnd.ms-powerpoint",
    ".pptx":
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    ".prop": "text/plain",
    ".rc": "text/plain",
    ".rmvb": "audio/x-pn-realaudio",
    ".rtf": "application/rtf",
    ".sh": "text/plain",
    ".tar": "application/x-tar",
    ".tgz": "application/x-compressed",
    ".txt": "text/plain",
    ".wav": "audio/x-wav",
    ".wma": "audio/x-ms-wma",
    ".wmv": "audio/x-ms-wmv",
    ".wps": "application/vnd.ms-works",
    ".xml": "text/plain",
    ".z": "application/x-compress",
    ".zip": "application/x-zip-compressed",
    "": "*/*"
  };

  static String englishToFarsi(String text) {
    if (text == null) return "";
    const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    for (int i = 0; i < english.length; i++) {
      text = text.replaceAll(english[i], farsi[i]);
    }
    return text;
  }

  static String farsiToEnglish(String text) {
    if (text == null) return "";
    const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    for (int i = 0; i < english.length; i++) {
      text = text.replaceAll(farsi[i], english[i]);
    }
    return text;
  }

  static String getNumber(int numberInt, bool isEnglish) {
    String numberStr = numberInt.toString();
    const number = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const farsi = [
      'اول',
      'دوم',
      'سوم',
      'چهارم',
      'پنجم',
      'ششم',
      'هفتم',
      'هشتم',
      'نهم',
      'دهم'
    ];
    const english = [
      'First',
      'Second',
      'Third',
      'Fourth',
      'Fifth',
      'Sixth',
      'Seventh',
      'Eighth',
      'Ninth',
      'Tenth'
    ];
    for (int i = 0; i < numberStr.length; i++) {
      numberStr =
          numberStr.replaceAll(number[i], isEnglish ? english[i] : farsi[i]);
    }
    return numberStr;
  }

  static String lookupUti(String contentType) {
    return _utiTable.containsKey(contentType) ? _utiTable[contentType] : "";
  }

  static String lookupIntent(String contentType) {
    return _intentTable.containsKey(contentType)
        ? _intentTable[contentType]
        : "";
  }

  static String getPrice(double price) => FlutterMoneyFormatter(
      amount: price ?? 0.0,
      settings: MoneyFormatterSettings(
        thousandSeparator: ",",
      )).output.withoutFractionDigits;

  static String getPriceWithFreeState(double price) => (price ?? 0.0) == 0.0
      ? "رایگان"
      : FlutterMoneyFormatter(
          amount: price ?? 0.0,
          settings: MoneyFormatterSettings(
            thousandSeparator: ",",
          )).output.withoutFractionDigits;

  static String getFloat(double value) {
    if (value == null) return "0";
    if (value.roundToDouble() == value) return value.toInt().toStringAsFixed(0);
    return value.toStringAsFixed(1);
  }

  static String getFloatWithTwoDigit(double value) {
    if (value == null) return "0";
    if (value.roundToDouble() == value) return value.toInt().toStringAsFixed(0);
    return value.toStringAsFixed(2);
  }

  static String get getCurrentStringTime {
    DateTime now = DateTime.now();
    return '${getNumberWithTowDigit(now.hour)}:${getNumberWithTowDigit(now.minute)}:${getNumberWithTowDigit(now.second)}';
  }

  static Future<bool> chkConnectionWith({String address = 'google.com'}) async {
    try {
      final result = await InternetAddress.lookup(address);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
      return false;
    } on SocketException catch (_) {
      return false;
    }
  }

  static bool checkEmail(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$")
        .hasMatch(email);
  }

  static String getNumberWithTowDigit(int value) {
    return value < 10 ? "0$value" : "$value";
  }

  static int abs(int a) => a < 0 ? -a : a;

  static String getTime(Duration time) {
    int minute = time.inMinutes;
    int second = time.inSeconds % 60;
    String minStr = minute < 10 ? "0$minute" : "$minute";
    String secStr = second < 10 ? "0$second" : "$second";
    return "$minStr:$secStr";
  }

  static bool validateNationalCode(String code) {
    int nationalCode = int.tryParse(code) ?? 0;
    List<int> arrayNationalCode = List(10);

    //extract digits from number
    for (int i = 0; i < 10; i++) {
      arrayNationalCode[i] = (nationalCode % 10);
      nationalCode = nationalCode ~/ 10;
    }

    //Checking the control digit
    int sum = 0;
    for (int i = 9; i > 0; i--) sum += arrayNationalCode[i] * (i + 1);
    int temp = sum % 11;
    if (temp < 2)
      return arrayNationalCode[0] == temp;
    else
      return arrayNationalCode[0] == 11 - temp;
  }

  static String getStringCount(int count) {
    int b = 1000000000;
    int m = 1000000;
    int k = 1000;
    if (count >= b) {
      String result = getFloat(count / b);
      result =
          "${result.endsWith(".0") ? result.substring(0, result.length - 2) : result}B";
      return result;
    }
    if (count >= m) {
      String result = getFloat(count / m);
      result =
          "${result.endsWith(".0") ? result.substring(0, result.length - 2) : result}M";
      return result;
    }
    if (count >= k) {
      String result = getFloat(count / k);
      result =
          "${result.endsWith(".0") ? result.substring(0, result.length - 2) : result}K";
      return result;
    }
    return "$count";
  }

  static CountingUtils getStringUtilCount(int count) {
    int b = 1000000000;
    int m = 1000000;
    int k = 1000;
    if (count >= b) {
      String result = getFloatWithTwoDigit(count / b);
      return CountingUtils(
        letter: 'B',
        count: result.endsWith("0")
            ? result.substring(0, result.length - 1)
            : result,
      );
    }
    if (count >= m) {
      String result = getFloatWithTwoDigit(count / m);
      return CountingUtils(
        letter: 'M',
        count: result.endsWith("0")
            ? result.substring(0, result.length - 1)
            : result,
      );
    }
    if (count >= k) {
      String result = getFloatWithTwoDigit(count / k);
      return CountingUtils(
        letter: 'K',
        count: result.endsWith("0")
            ? result.substring(0, result.length - 1)
            : result,
      );
    }
    return CountingUtils(count: '$count');
  }

  static bool isTablet(BuildContext context) =>
      !(MediaQuery.of(context).size.shortestSide < 600);

  static int getDecimalOfDouble(double value) {
    String doubleAsString = value.toString();
    int indexOfDecimal = doubleAsString.indexOf(".");
    return int.tryParse(doubleAsString.substring(indexOfDecimal)) ?? 0;
  }

  static TextDirection get textDirection => TextDirection.rtl;

  static String get fontFamily => MyFonts.yekan;

  static Duration get pageSwitchDuration => Duration(milliseconds: 300);

  static Map<T, List<S>> groupBy<S, T>(Iterable<S> values, T Function(S) key) {
    var map = <T, List<S>>{};
    for (var element in values) {
      (map[key(element)] ??= []).add(element);
    }
    return map;
  }

  static String formatBytes(int bytes, int decimals) {
    if (bytes <= 0) return "0 B";
    const suffixes = [
      "بایت",
      "کیلوبایت",
      "مگابایت",
      "گیگابایت",
      "ترابایت",
      "PB",
      "EB",
      "ZB",
      "YB"
    ];
    var i = (log(bytes) / log(1024)).floor();
    String result =
        ((bytes / pow(1024, i)).toStringAsFixed(decimals)) + ' ' + suffixes[i];
    return result;
  }

  static String getNumberInString(
      String prefix, int numberStartAtZero, String postfix) {
    if (numberStartAtZero < 10)
      return prefix +
          _getUnderTenNumberString(numberStartAtZero, false) +
          postfix;
    if (numberStartAtZero < 20)
      switch (numberStartAtZero) {
        case 10:
          return prefix + "یازدهم" + postfix;
        case 11:
          return prefix + "دوازدهم" + postfix;
        case 12:
          return prefix + "سیزدهم" + postfix;
        case 13:
          return prefix + "چهاردهم" + postfix;
        case 14:
          return prefix + "پانزدهم" + postfix;
        case 15:
          return prefix + "شانزدهم" + postfix;
        case 16:
          return prefix + "هفدهم" + postfix;
        case 17:
          return prefix + "هجدهم" + postfix;
        case 18:
          return prefix + "نوزدهم" + postfix;
        case 19:
          return prefix + "بیستم" + postfix;
      }

    if (numberStartAtZero < 29) {
      int lastNumber = numberStartAtZero % 10;
      return prefix +
          "بیست و " +
          _getUnderTenNumberString(lastNumber, true) +
          postfix;
    }
    if (numberStartAtZero == 29) return prefix + "سی ام" + postfix;
    if (numberStartAtZero < 39) {
      int lastNumber = numberStartAtZero % 10;
      return prefix +
          "سی و " +
          _getUnderTenNumberString(lastNumber, true) +
          postfix;
    }
    if (numberStartAtZero == 39) return prefix + "چهلم" + postfix;
    if (numberStartAtZero < 49) {
      int lastNumber = numberStartAtZero % 10;
      return prefix +
          "چهل و " +
          _getUnderTenNumberString(lastNumber, true) +
          postfix;
    }
    if (numberStartAtZero == 49) return prefix + "پنجاهم" + postfix;
    if (numberStartAtZero < 59) {
      int lastNumber = numberStartAtZero % 10;
      return prefix +
          "پنجاه و " +
          _getUnderTenNumberString(lastNumber, true) +
          postfix;
    }
    if (numberStartAtZero == 59) return prefix + "شصتم" + postfix;
    if (numberStartAtZero < 69) {
      int lastNumber = numberStartAtZero % 10;
      return prefix +
          "شصت و " +
          _getUnderTenNumberString(lastNumber, true) +
          postfix;
    }
    if (numberStartAtZero == 69) return prefix + "هفتادم" + postfix;
    return '$prefix$numberStartAtZero$postfix';
  }

  static String _getUnderTenNumberString(int number, bool moreThanTen) {
    switch (number) {
      case 0:
        return !moreThanTen ? "اول" : "یکم";
      case 1:
        return "دوم";
      case 2:
        return "سوم";
      case 3:
        return "چهارم";
      case 4:
        return "پنجم";
      case 5:
        return "ششم";
      case 6:
        return "هفتم";
      case 7:
        return "هشتم";
      case 8:
        return "نهم";
      case 9:
        return "دهم";
    }
    return '$number';
  }

  static bool isBarcodeValid(String barcode) {
    String temp = barcode.replaceAll("-", "").trim();
    if (temp.length != 13) return false;
    List<String> tempChar = temp.split('');
    int sum = 0;
    for (int i = 0; i < tempChar.length; i++) {
      int intTemp = int.tryParse(tempChar[i]);
      if (intTemp == null) return false;
      sum = sum + (intTemp * (i % 2 == 0 ? 1 : 3));
    }
    return sum % 10 == 0;
  }

  static double getFileSizeInKB(int length) {
    return length / 1024;
  }

  static String getBarcode(String isbn) {
    String rawNumbers = isbn.replaceAll('-', '');
    if (rawNumbers.length <= 3) return rawNumbers;
    if (rawNumbers.length <= 6)
      return ' ${rawNumbers.substring(0, 3)}-${rawNumbers.substring(3)}';
    if (rawNumbers.length <= 10)
      return '${rawNumbers.substring(0, 3)}-${rawNumbers.substring(3, 6)}'
          '-${rawNumbers.substring(6)}';
    if (rawNumbers.length <= 12)
      return '${rawNumbers.substring(0, 3)}-${rawNumbers.substring(3, 6)}'
          '-${rawNumbers.substring(6, 10)}-${rawNumbers.substring(10)}';
    else
      return '${rawNumbers.substring(0, 3)}-${rawNumbers.substring(3, 6)}'
          '-${rawNumbers.substring(6, 10)}-${rawNumbers.substring(10, 12)}'
          '-${rawNumbers.substring(12)}';
  }

  static bool isDateRangeWrong(String start, String end) {
    PersianDateTime startDate =
        PersianDateTime(jalaaliDateTime: Tools.farsiToEnglish(start));
    PersianDateTime endDate =
        PersianDateTime(jalaaliDateTime: Tools.farsiToEnglish(end));
    return startDate.isAfter(endDate);
  }

  static bool isDateFormatWrong(String date) {
    try {
      PersianDateTime pDate =
          PersianDateTime(jalaaliDateTime: Tools.farsiToEnglish(date));
      return pDate == null;
    } catch (error) {
      print(error);
      return true;
    }
  }

  static String getError({@required error, String defaultError = ''}) {
    if (error == null) return defaultError;
    String staticError = defaultError;
    String message = error is String
        ? (error?.length ?? 0) == 0
            ? staticError
            : error
        : staticError;
    return message;
  }
}
