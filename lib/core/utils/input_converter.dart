import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../features/authentication/domain/entities/login_body.dart';
import '../../features/authentication/domain/entities/sign_up_body.dart';
import '../error/failures.dart';

const int MALE = 1;
const int FEMALE = 2;
const String MALE_STRING = 'مرد';
const String FE_MALE_STRING = 'زن';

class InputConverter {
  Either<Failure, LoginBody> stringToLoginBody({
    @required String username,
    @required String password,
  }) {
    if (username == null) return Left(InvalidUsernameFailure());
    if (password == null) return Left(InvalidPasswordFailure());
    if (username == '') return Left(InvalidUsernameFailure());
    if (password == '') return Left(InvalidPasswordFailure());
    if (!username.isNationalCodeValud) return Left(InvalidUsernameFailure());
    if (!password.isPhoneNumberValid) return Left(InvalidPasswordFailure());
    return Right(LoginBody(
      username: username.farsiToEnglish,
      password: password.farsiToEnglish,
    ));
  }

  Either<Failure, SignUpBody> stringToSignUpBody({
    @required String birthDate,
    int gender,
    @required String lastName,
    @required String mobile,
    @required String nCode,
    @required String name,
    @required String telephone,
  }) {
    if (name == null) return Left(InvalidNameFailure());
    if (name == '') return Left(InvalidNameFailure());
    if (lastName == null) return Left(InvalidLastNameFailure());
    if (lastName == '') return Left(InvalidLastNameFailure());
    if (nCode == null) return Left(InvalidNationalCodeFailure());
    if (nCode == '') return Left(InvalidNationalCodeFailure());
    if (!nCode.isNationalCodeValud) return Left(InvalidNationalCodeFailure());
    if (!gender.isGenderValid) return Left(InvalidGenderFailure());
    if (telephone == null) return Left(InvalidTelephoneFailure());
    if (telephone == '') return Left(InvalidTelephoneFailure());
    if (!telephone.isTelephoneValid) return Left(InvalidTelephoneFailure());
    if (mobile == null) return Left(InvalidMobileFailure());
    if (mobile == '') return Left(InvalidMobileFailure());
    if (!mobile.isPhoneNumberValid) return Left(InvalidMobileFailure());
    if (birthDate == null) return Left(InvalidBirthDayFailure());
    if (birthDate == '') return Left(InvalidBirthDayFailure());
    if (!birthDate.isYearDateValid) return Left(InvalidBirthDayFailure());
    return Right(SignUpBody(
      birthDate: birthDate.farsiToEnglish,
      gender: gender.getGender,
      lastName: lastName.farsiToEnglish,
      mobile: mobile.farsiToEnglish,
      nCode: nCode.farsiToEnglish,
      name: name.farsiToEnglish,
      telephone: telephone.farsiToEnglish,
    ));
  }
}

extension Checker on String {
  bool get isYearDateValid => this.length == 4;

  bool get isNationalCodeValud {
    int nationalCode = int.tryParse(this) ?? 0;
    List<int> arrayNationalCode = List(10);

    //extract digits from number
    for (int i = 0; i < 10; i++) {
      arrayNationalCode[i] = (nationalCode % 10);
      nationalCode = nationalCode ~/ 10;
    }

    //Checking the control digit
    int sum = 0;
    for (int i = 9; i > 0; i--) sum += arrayNationalCode[i] * (i + 1);
    int temp = sum % 11;
    if (temp < 2)
      return arrayNationalCode[0] == temp;
    else
      return arrayNationalCode[0] == 11 - temp;
  }

  bool get isPhoneNumberValid {
    int length = this.length;
    Pattern pattern = r'[0123456789+]';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(this) ||
        !(this.startsWith('09') || this.startsWith('+989')) ||
        !(length == 11 || length == 13)) return false;
    return true;
  }

  bool get isTelephoneValid {
    int length = this.length;
    Pattern pattern = r'[0123456789]';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(this) || !this.startsWith('0') || length != 11)
      return false;
    return true;
  }

  String get farsiToEnglish {
    if (this == null) return "";
    String temp = this;
    const english = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const farsi = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    for (int i = 0; i < english.length; i++) {
      temp = temp.replaceAll(farsi[i], english[i]);
    }
    return temp;
  }
}

extension IntChecker on int {
  bool get isGenderValid {
    if (this != null) {
      if (this != MALE && this != FEMALE) return false;
    }
    return true;
  }

  String get getGender => this == null
      ? null
      : this == MALE
          ? MALE_STRING
          : FE_MALE_STRING;
}

class InvalidUsernameFailure extends Failure {}

class InvalidPasswordFailure extends Failure {}

class InvalidBirthDayFailure extends Failure {}

class InvalidGenderFailure extends Failure {}

class InvalidLastNameFailure extends Failure {}

class InvalidMobileFailure extends Failure {}

class InvalidNationalCodeFailure extends Failure {}

class InvalidNameFailure extends Failure {}

class InvalidTelephoneFailure extends Failure {}
