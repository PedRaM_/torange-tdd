import 'package:flutter/material.dart';

class SelectStatus {
  SelectStatus._();

  static const int NOT_SELECTED = 0;
  static const int HALF_SELECTED = 1;
  static const int SELECTED = 2;
}

class MyColors {
  MyColors._();

  static const Color combuGreen = Color(0xFF243119);
  static const Color hunterGreen = Color(0xFF43633d);
  static const Color hintTextColor = Color(0xFF1C1018);
  static const Color russianGreen = Color(0xFF629460);
  static const Color amazon = Color(0xFF337357);
  static const Color spaceCadet = Color(0xFF2e2d4d);
  static const Color darkSeeGreen = Color(0xFF96be8c);
  static const Color errorColor = Color(0xFFf77b72);
  static const Color redColor = Color(0xFFea3645);
  static const Color greenSmithApple = Color(0xFFaceca1);
  static const Color teaApple = Color(0xFFc9f2c7);
  static const Color unSelectedLabelColor = Color(0x90FFFFFF);
}

class MyFonts {
  MyFonts._();

  static const String roboto = "Roboto";
  static const String yekan = "IranYekan";
  static const String iranSansFarsi = "IranSansFarsi";
  static const FontWeight bold = FontWeight.w700;
  static const FontWeight medium = FontWeight.w600;
  static const FontWeight normal = FontWeight.w500;
  static const FontWeight light = FontWeight.w400;
  static const FontWeight ultraLight = FontWeight.w300;
}

class MyIcons {
  MyIcons._();

  static const IconData menu = IconData(0xe900, fontFamily: 'MyFontIcons');
  static const IconData back = IconData(0xe901, fontFamily: 'MyFontIcons');
  static const IconData add = IconData(0xe902, fontFamily: 'MyFontIcons');
  static const IconData arrow_bottom =
      IconData(0xe903, fontFamily: 'MyFontIcons');
  static const IconData tick = IconData(0xe904, fontFamily: 'MyFontIcons');
  static const IconData minus = IconData(0xe905, fontFamily: 'MyFontIcons');
  static const IconData timeTable = IconData(0xe906, fontFamily: 'MyFontIcons');
}

class Strings {
  Strings._();

  static const String INVALID_NATIONAL_CODE_MESSAGE =
      'کد ملی وارد شده معتبر نمی باشد';

  static const String INVALID_NAME_MESSAGE = 'وارد کردن نام الزامی می باشد';

  static const String INVALID_LAST_NAME_MESSAGE =
      'وارد کردن نام خانوادگی الزامی می باشد';

  static const String INVALID_BIRTH_DATE_MESSAGE =
      'تاریخ تولد (سال) را صحیح وارد کنید';

  static const String INVALID_MOBILE_MESSAGE =
      'شماره تلفن همراه را صحیح وارد کنید';

  static const String INVALID_TELEPHONE_MESSAGE =
      'شماره تلفن ثابت را صحیح وارد کنید';

  static const String INVALID_GENDER_MESSAGE = 'جنسیت خود را صحیح وارد کنید';

  static const String SERVER_FAILURE_MESSAGE =
      'خطایی رخ داد با پشتیبانی تماس بگیرید';

  static const String AUTHENTICATION_FAILURE_MESSAGE =
      'خطا در احراز هویت، لطفا مجددا وارد شوید';

  static const String CACHE_FAILURE_MESSAGE =
      'خطا در احراز هویت، لطفا مجددا وارد شوید';

  static String get generalError => "خطایی رخ داد با پشتیبانی تماس بگیرید";

  static String get noInternetConnectionError => 'عدم اتصال به اینترنت';

  static List<String> get tabNames => ['شرح حال', 'معاینه', 'پاراکلینیک'];

  static String getInvalidSymptomsSelection(String name) {
    return 'حداقل یکی از گزینه های $name را انتخاب کنید';
  }

  static const String DOCTOR_MAIN_BOX = 'Doctor_Main_Box';
  static const String DOCTOR_EXAM_BOX = 'Doctor_Exam_Box';
  static const String DOCTOR_PARA_BOX = 'Doctor_Para_Box';
  static const String DOCTOR_HISTORY_BOX = 'Doctor_History_Box';
}

class Integers {
  Integers._();
}
