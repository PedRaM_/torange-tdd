import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

typedef T FromJson<T>(data);

class BaseResponseModel<T> extends Equatable {
  final bool isSuccessful;
  final T data;
  final String message;

  BaseResponseModel({
    @required this.isSuccessful,
    @required this.message,
    @required this.data,
  });

  @override
  List<Object> get props => [
        isSuccessful,
        data,
        message,
      ];

  factory BaseResponseModel.fromJson(
      Map<String, dynamic> json, FromJson<T> fromJson) {
    final bool isSuccessful = json['IsSuccess'];
    final errorList = json['ErrorList'];
    final String message =
        errorList != null && errorList is List && errorList.length > 0
            ? errorList[0]['ErrorMessage']
            : '';
    final data = json['Data'];
    return BaseResponseModel<T>(
      message: message,
      data: isSuccessful ? fromJson(data) : null,
      isSuccessful: isSuccessful,
    );
  }
}
