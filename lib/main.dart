import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/router/app_router.dart';
import 'core/utils/values.dart';
import 'features/appointment/presentation/bloc/appointment_bloc.dart';
import 'features/appointment/presentation/pages/appointments_page.dart';
import 'features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'features/authentication/presentation/bloc/authentication_bloc.dart';
import 'features/authentication/presentation/cubits/page_switcher/page_switcher_cubit.dart';
import 'features/authentication/presentation/pages/landing_page.dart';
import 'injection_container.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await init();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));

  // Hive
  //   ..initFlutter()
  //   ..registerAdapter(HiveMainSympAdapter())
  //   ..registerAdapter(HiveParaSympAdapter())
  //   ..registerAdapter(HiveExamSympAdapter())
  //   ..registerAdapter(HiveHistorySympAdapter());
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({
    Key key,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AppRouter _appRouter = AppRouter();

  @override
  void dispose() {
    _closeBox();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _setupApp();
  }

  void _setupApp() async {
    // if (!Hive.isBoxOpen(Strings.DOCTOR_MAIN_BOX))
    //   await Hive.openBox<HiveMainSymp>(Strings.DOCTOR_MAIN_BOX);
    // if (!Hive.isBoxOpen(Strings.DOCTOR_HISTORY_BOX))
    //   await Hive.openBox<HiveHistorySymp>(Strings.DOCTOR_HISTORY_BOX);
    // if (!Hive.isBoxOpen(Strings.DOCTOR_PARA_BOX))
    //   await Hive.openBox<HiveParaSymp>(Strings.DOCTOR_PARA_BOX);
    // if (!Hive.isBoxOpen(Strings.DOCTOR_EXAM_BOX))
    //   await Hive.openBox<HiveExamSymp>(Strings.DOCTOR_EXAM_BOX);
    // await Hive.deleteFromDisk();
  }

  void _closeBox() {
    // if (Hive.isBoxOpen(Strings.DOCTOR_MAIN_BOX)) Hive.close();
    // if (Hive.isBoxOpen(Strings.DOCTOR_EXAM_BOX)) Hive.close();
    // if (Hive.isBoxOpen(Strings.DOCTOR_PARA_BOX)) Hive.close();
    // if (Hive.isBoxOpen(Strings.DOCTOR_HISTORY_BOX)) Hive.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthenticationBloc>(
      create: (context) => sl<AuthenticationBloc>(),
      child: MaterialApp(
        title: 'تـــرنـــج',
        builder: (context, child) {
          final MediaQueryData data = MediaQuery.of(context);
          _appRouter = AppRouter();
          return MediaQuery(
            data: data.copyWith(textScaleFactor: 1.1),
            child: child,
          );
        },
        home: sl<AuthenticationLocalDataSource>().isLogged()
            ? BlocProvider<AppointmentBloc>(
                create: (context) => sl<AppointmentBloc>(),
                child: AppointmentsPage(),
              )
            : MultiBlocProvider(
                providers: [
                  BlocProvider<PageSwitcherCubit>(
                    create: (context) => sl<PageSwitcherCubit>(),
                  ),
                  BlocProvider<AuthenticationBloc>(
                    create: (context) => sl<AuthenticationBloc>(),
                  ),
                ],
                child: LandingPage(),
              ),
        theme: ThemeData(
          primaryColor: MyColors.amazon,
          accentColor: MyColors.spaceCadet,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        debugShowCheckedModeBanner: false,
        onGenerateRoute: (RouteSettings settings) =>
            _appRouter.onGenerateRoute(settings),
      ),
    );
  }
}
