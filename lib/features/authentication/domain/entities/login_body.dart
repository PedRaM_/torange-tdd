import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class LoginBody extends Equatable {
  final String username;
  final String password;

  LoginBody({
    @required this.username,
    @required this.password,
  });

  @override
  List<Object> get props => [
        username,
        password,
      ];
}
