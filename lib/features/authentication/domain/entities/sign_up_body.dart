import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class SignUpBody extends Equatable {
  final String nCode;
  final String name;
  final String lastName;
  final String gender;
  final String telephone;
  final String mobile;
  final String birthDate;

  SignUpBody({
    @required this.nCode,
    @required this.name,
    @required this.lastName,
    this.gender,
    @required this.telephone,
    @required this.mobile,
    @required this.birthDate,
  });

  @override
  List<Object> get props => [
        nCode,
        name,
        lastName,
        gender,
        telephone,
        mobile,
        birthDate,
      ];
}
