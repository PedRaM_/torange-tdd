import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class RefreshTokenBody extends Equatable {
  final String username;
  final String accessToken;
  final String refreshToken;

  RefreshTokenBody({
    @required this.username,
    @required this.accessToken,
    @required this.refreshToken,
  });

  @override
  List<Object> get props => [
        username,
        accessToken,
        refreshToken,
      ];
}
