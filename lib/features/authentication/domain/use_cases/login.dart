import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import '../../data/models/login_body_model.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../entities/login_result.dart';
import '../repositories/authentication_repository.dart';

class Login extends UseCase<LoginResult, Params> {
  final AuthenticationRepository repository;

  Login(this.repository);

  Future<Either<Failure, LoginResult>> call(Params params) async =>
      await repository.login(params.body);
}

class Params extends NoParams {
  final LoginBodyModel body;

  Params({@required this.body});

  @override
  List<Object> get props => [body];
}
