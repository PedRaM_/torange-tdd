import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import '../../data/models/sign_up_body_model.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../entities/login_result.dart';
import '../repositories/authentication_repository.dart';

class SignUp extends UseCase<LoginResult, Params> {
  final AuthenticationRepository repository;

  SignUp(this.repository);

  @override
  Future<Either<Failure, LoginResult>> call(Params params) async =>
      await repository.signUp(params.body);
}

class Params extends NoParams {
  final SignUpBodyModel body;

  Params({@required this.body});

  @override
  List<Object> get props => [body];
}
