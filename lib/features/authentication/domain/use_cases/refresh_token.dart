import 'package:dartz/dartz.dart';
import '../../data/models/refresh_token_body_model.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../entities/login_result.dart';
import '../repositories/authentication_repository.dart';

class RefreshToken extends UseCase<LoginResult, Params> {
  final AuthenticationRepository repository;

  RefreshToken(this.repository);

  @override
  Future<Either<Failure, LoginResult>> call(Params params) async =>
      await repository.refreshToken(params.body);
}

class Params extends NoParams {
  final RefreshTokenBodyModel body;

  Params({this.body});

  @override
  List<Object> get props => [body];
}
