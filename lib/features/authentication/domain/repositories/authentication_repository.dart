import 'package:dartz/dartz.dart';
import '../../data/models/login_body_model.dart';
import '../../data/models/refresh_token_body_model.dart';
import '../../data/models/sign_up_body_model.dart';

import '../../../../core/error/failures.dart';
import '../entities/login_result.dart';

abstract class AuthenticationRepository {
  Future<Either<Failure, LoginResult>> login(LoginBodyModel body);
  Future<Either<Failure, LoginResult>> signUp(SignUpBodyModel body);
  Future<Either<Failure, LoginResult>> refreshToken(RefreshTokenBodyModel body);
}
