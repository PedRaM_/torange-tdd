import '../../domain/entities/login_body.dart';
import 'package:meta/meta.dart';

class LoginBodyModel extends LoginBody {
  LoginBodyModel({
    @required String username,
    @required String password,
  }) : super(
          username: username,
          password: password,
        );

  factory LoginBodyModel.fromEntity(LoginBody body) => LoginBodyModel(
        password: body.password,
        username: body.username,
      );

  Map<String, dynamic> toJson() => {
        'Username': username,
        'Password': password,
      };
}
