import '../../domain/entities/refresh_token_body.dart';
import 'package:meta/meta.dart';

class RefreshTokenBodyModel extends RefreshTokenBody {
  RefreshTokenBodyModel({
    @required String username,
    @required String refreshToken,
    @required String accessToken,
  }) : super(
          username: username,
          refreshToken: refreshToken,
          accessToken: accessToken,
        );

  factory RefreshTokenBodyModel.fromEntity(RefreshTokenBody body) =>
      RefreshTokenBodyModel(
        accessToken: body.accessToken,
        refreshToken: body.refreshToken,
        username: body.username,
      );

  Map<String, dynamic> toJson() => {
        'Username': username,
        'AccessToken': accessToken,
        'RefreshToken': refreshToken,
      };
}
