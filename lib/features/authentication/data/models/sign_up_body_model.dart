import '../../domain/entities/sign_up_body.dart';
import 'package:meta/meta.dart';

class SignUpBodyModel extends SignUpBody {
  SignUpBodyModel({
    @required String nCode,
    @required String name,
    @required String lastName,
    @required String gender,
    @required String telephone,
    @required String mobile,
    @required String birthDate,
  }) : super(
          name: name,
          birthDate: birthDate,
          gender: gender,
          lastName: lastName,
          mobile: mobile,
          nCode: nCode,
          telephone: telephone,
        );

  factory SignUpBodyModel.fromEntity(SignUpBody body) => SignUpBodyModel(
        birthDate: body.birthDate,
        gender: body.gender,
        lastName: body.lastName,
        mobile: body.mobile,
        nCode: body.nCode,
        name: body.name,
        telephone: body.telephone,
      );

  Map<String, dynamic> toJson() => {
        'NCode': nCode,
        'Name': name,
        'Family': lastName,
        'Sex': gender,
        'Tel': telephone,
        'Mobile': mobile,
        'DateB': birthDate,
      };
}
