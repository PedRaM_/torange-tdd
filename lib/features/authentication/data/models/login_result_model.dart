import '../../domain/entities/login_result.dart';
import 'package:meta/meta.dart';

class LoginResultModel extends LoginResult {
  LoginResultModel({
    @required String username,
    @required String accessToken,
    @required String refreshToken,
  }) : super(
          username: username,
          refreshToken: refreshToken,
          accessToken: accessToken,
        );

  factory LoginResultModel.fromJson(Map<String, dynamic> json) {
    return LoginResultModel(
      username: json['Username'],
      accessToken: json['AccessToken'],
      refreshToken: json['RefreshToken'],
    );
  }

  Map<String, dynamic> toJson() => {
        'Username': username,
        'AccessToken': accessToken,
        'RefreshToken': refreshToken
      };
}
