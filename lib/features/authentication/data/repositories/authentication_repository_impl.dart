import '../../../../core/error/exceptions.dart';
import '../models/login_body_model.dart';
import '../models/refresh_token_body_model.dart';
import '../models/sign_up_body_model.dart';

import '../../../../core/network/network_info.dart';
import '../data_sources/authentication_data_source.dart';
import '../data_sources/authentication_local_data_source.dart';
import '../../domain/entities/login_result.dart';
import '../../../../core/error/failures.dart';
import 'package:dartz/dartz.dart';
import '../../domain/repositories/authentication_repository.dart';
import 'package:meta/meta.dart';

const String NO_INTERNET_MESSAGE = 'عدم اتصال به اینترنت';
typedef Future<LoginResult> _Authenticate();

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationDataSource remoteDataSource;
  final AuthenticationLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  AuthenticationRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, LoginResult>> login(LoginBodyModel body) async {
    return await _authenticate(() => remoteDataSource.login(body));
  }

  @override
  Future<Either<Failure, LoginResult>> refreshToken(
      RefreshTokenBodyModel body) async {
    return await _authenticate(() => remoteDataSource.refreshToken(body));
  }

  @override
  Future<Either<Failure, LoginResult>> signUp(SignUpBodyModel body) async {
    return await _authenticate(() => remoteDataSource.signUp(body));
  }

  Future<Either<Failure, LoginResult>> _authenticate(
    _Authenticate authenticate,
  ) async {
    if (!(await networkInfo.isConnected))
      return Left(ServerFailure(message: NO_INTERNET_MESSAGE));
    try {
      final remoteLoginBody = await authenticate();
      localDataSource.saveLoginResult(remoteLoginBody);
      localDataSource.setLogged(true);
      return Right(remoteLoginBody);
    } on ServerException catch (error) {
      return Left(ServerFailure(message: error.message));
    } on AuthenticationException {
      return Left(AuthenticationFailure());
    } on AuthorizationException {
      return Left(AuthorizationFailure());
    }
  }
}
