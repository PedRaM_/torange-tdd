import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exceptions.dart';
import '../models/login_result_model.dart';
import 'package:meta/meta.dart';

abstract class AuthenticationLocalDataSource {
  /// Gets the saved [LoginResultModel] which gotten the last time
  /// the user had login.
  ///
  /// Throws [AuthorizationException] if no saved data is present.
  Future<LoginResultModel> getLoginResultModel();

  Future<void> saveLoginResult(LoginResultModel modelToSave);

  /// Gets the [bool] if refresh token is in progress
  ///
  /// return [false] if no saved data is present.
  Future<bool> isRefreshing();

  Future<void> setRefreshing(bool isRefreshing);

  Future<void> setLogged(bool isLogged);

  /// Gets the [bool] if user is logged in.
  ///
  /// return [false] if no saved data is present.
  bool isLogged();
}

const CACHED_LOGIN_RESULT = 'CACHED_LOGIN_RESULT';
const IS_REFRESHING = 'IS_REFRESHING';
const IS_LOGGED = 'IS_LOGGED';

class AuthenticationLocalDataSourceImpl
    implements AuthenticationLocalDataSource {
  final SharedPreferences sharedPreferences;

  AuthenticationLocalDataSourceImpl({@required this.sharedPreferences});
  @override
  Future<LoginResultModel> getLoginResultModel() async {
    final bool isRefreshingToken = await isRefreshing();
    if (isRefreshingToken) {
      await Future.delayed(Duration(milliseconds: 500));
      return getLoginResultModel();
    }
    final jsonString = sharedPreferences.getString(CACHED_LOGIN_RESULT);
    if (jsonString != null)
      return Future.value(LoginResultModel.fromJson(json.decode(jsonString)));
    else
      throw AuthorizationException();
  }

  @override
  Future<void> saveLoginResult(LoginResultModel modelToSave) async {
    sharedPreferences.setString(
      CACHED_LOGIN_RESULT,
      json.encode(modelToSave.toJson()),
    );
  }

  @override
  Future<bool> isRefreshing() async {
    return sharedPreferences.getBool(IS_REFRESHING) ?? false;
  }

  @override
  Future<void> setRefreshing(bool isRefreshing) async {
    await sharedPreferences.setBool(IS_REFRESHING, isRefreshing);
  }

  @override
  Future<void> setLogged(bool isLogged) async {
    sharedPreferences.setBool(IS_LOGGED, isLogged);
  }

  @override
  bool isLogged() {
    return sharedPreferences.getBool(IS_LOGGED) ?? false;
  }
}
