import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/models/base_response_model.dart';
import '../../domain/entities/login_result.dart';
import '../models/login_body_model.dart';
import '../models/login_result_model.dart';
import '../models/refresh_token_body_model.dart';
import '../models/sign_up_body_model.dart';

abstract class AuthenticationDataSource {
  /// Calls the http://torange.mubabol.ac.ir/api/login/GetAppToken endpoint
  /// with contains [Username] and [Password] in the body to get Token.
  ///
  /// Throws a [ServerException] for all error code.
  Future<LoginResult> login(LoginBodyModel body);

  /// Calls the http://torange.mubabol.ac.ir/api/torange/AddPatient endpoint to sign up the new user
  ///
  /// Throws a [ServerException] for all error code.
  Future<LoginResult> signUp(SignUpBodyModel body);

  /// Calls the http://torange.mubabol.ac.ir/api/login/GetAppToken endpoint
  /// with contains [Username] and [AccessToken] and [RefreshToken] in the body to get Token.
  ///
  /// Throws a [ServerException] for all error code.
  Future<LoginResult> refreshToken(RefreshTokenBodyModel body);
}

class AuthenticationDataSourceImpl implements AuthenticationDataSource {
  final http.Client client;

  AuthenticationDataSourceImpl({@required this.client});

  @override
  Future<LoginResult> login(LoginBodyModel body) => _authenticate(
        body: body,
        url: 'http://torange.mubabol.ac.ir/api/login/GetAppToken',
      );

  @override
  Future<LoginResult> refreshToken(RefreshTokenBodyModel body) => _authenticate(
        body: body,
        url: 'http://torange.mubabol.ac.ir/api/login/GetAppToken',
      );

  @override
  Future<LoginResult> signUp(SignUpBodyModel body) => _authenticate(
        body: body,
        url: 'http://torange.mubabol.ac.ir/api/torange/AddPatient',
      );

  Future<LoginResult> _authenticate({
    @required dynamic body,
    @required String url,
  }) async {
    final response = await client.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      encoding: Encoding.getByName('utf-8'),
      body: jsonEncode(body.toJson()),
    );
    if (response.statusCode == 200) {
      final baseResponseModel = BaseResponseModel<LoginResultModel>.fromJson(
          json.decode(response.body),
          (data) => LoginResultModel.fromJson(data));
      if (baseResponseModel.isSuccessful)
        return baseResponseModel.data;
      else
        throw body is RefreshTokenBodyModel
            ? AuthenticationException()
            : ServerException(message: baseResponseModel.message);
    } else if (response.statusCode == 401)
      throw AuthenticationException();
    else
      throw ServerException();
  }
}
