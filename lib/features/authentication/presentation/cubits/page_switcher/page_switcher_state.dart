part of 'page_switcher_cubit.dart';

abstract class PageSwitcherState extends Equatable {
  const PageSwitcherState();
}

class AuthenticateUiStart extends PageSwitcherState {
  @override
  List<Object> get props => [];
}

class IsLoginPage extends PageSwitcherState {
  final bool isLogin;

  IsLoginPage({this.isLogin}) : assert(isLogin != null);

  @override
  List<Object> get props => [isLogin];
}
