import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'page_switcher_state.dart';

class PageSwitcherCubit extends Cubit<PageSwitcherState> {
  PageSwitcherCubit() : super(AuthenticateUiStart());

  void setStartPage() => emit(AuthenticateUiStart());

  void setLoginPage() => emit(IsLoginPage(isLogin: true));

  void setSignUpPage() => emit(IsLoginPage(isLogin: false));
}
