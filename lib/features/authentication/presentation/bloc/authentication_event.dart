part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class LoginEvent extends AuthenticationEvent {
  final String username;
  final String password;

  LoginEvent({
    @required this.username,
    @required this.password,
  });

  @override
  List<Object> get props => [
        username,
        password,
      ];
}

class SignUpEvent extends AuthenticationEvent {
  final String nCode;
  final String name;
  final String lastName;
  final int gender;
  final String telephone;
  final String mobile;
  final String birthDate;

  SignUpEvent({
    @required this.nCode,
    @required this.name,
    @required this.lastName,
    @required this.gender,
    @required this.telephone,
    @required this.mobile,
    @required this.birthDate,
  });

  @override
  List<Object> get props => [
        nCode,
        name,
        lastName,
        gender,
        telephone,
        mobile,
        birthDate,
      ];
}

class RefreshEvent extends AuthenticationEvent {
  final String username;
  final String accessToken;
  final String refreshToken;

  RefreshEvent({
    @required this.username,
    @required this.accessToken,
    @required this.refreshToken,
  });

  @override
  List<Object> get props => [
        username,
        accessToken,
        refreshToken,
      ];
}
