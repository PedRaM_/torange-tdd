part of 'authentication_bloc.dart';

abstract class AuthenticationState extends Equatable {
  const AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {}

class LoadingState extends AuthenticationState {}

class LoginSuccessState extends AuthenticationState {
  final LoginResult body;

  @override
  List<Object> get props => [body];

  LoginSuccessState({@required this.body});
}

class SignUpSuccessState extends AuthenticationState {
  final LoginResult body;

  SignUpSuccessState({@required this.body});

  @override
  List<Object> get props => [body];
}

class RefreshSuccessState extends AuthenticationState {
  final LoginResult body;

  RefreshSuccessState({@required this.body});

  @override
  List<Object> get props => [body];
}

class AuthenticationFailureState extends AuthenticationState {
  final String message;

  AuthenticationFailureState({@required this.message});
  @override
  List<Object> get props => [message];
}

class NationalCodeFailureState extends AuthenticationState {
  final String message;

  NationalCodeFailureState({@required this.message});

  @override
  List<Object> get props => [message];
}

class PhoneNumberFailureState extends AuthenticationState {
  final String message;

  PhoneNumberFailureState({@required this.message});

  @override
  List<Object> get props => [message];
}

class NameFailureState extends AuthenticationState {
  final String message;

  NameFailureState({@required this.message});
  @override
  List<Object> get props => [message];
}

class LastNameFailureState extends AuthenticationState {
  final String message;

  LastNameFailureState({@required this.message});
  @override
  List<Object> get props => [message];
}

class GenderFailureState extends AuthenticationState {
  final String message;

  GenderFailureState({@required this.message});
  @override
  List<Object> get props => [message];
}

class TelephoneFailureState extends AuthenticationState {
  final String message;

  TelephoneFailureState({@required this.message});
  @override
  List<Object> get props => [message];
}

class BirthDateFailureState extends AuthenticationState {
  final String message;

  BirthDateFailureState({@required this.message});
  @override
  List<Object> get props => [message];
}

class RefreshTokenFailureState extends AuthenticationState {
  @override
  List<Object> get props => [];
}
