import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import '../../../../core/utils/values.dart';
import '../../data/models/refresh_token_body_model.dart';
import '../../data/models/sign_up_body_model.dart';
import '../../domain/entities/refresh_token_body.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/utils/input_converter.dart';
import '../../data/models/login_body_model.dart';
import '../../domain/entities/login_result.dart';
import '../../domain/use_cases/login.dart' as lgn;
import '../../domain/use_cases/refresh_token.dart' as rft;
import '../../domain/use_cases/sign_up.dart' as sgn;

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final lgn.Login login;
  final sgn.SignUp signUp;
  final rft.RefreshToken refreshToken;
  final InputConverter inputConverter;

  AuthenticationBloc({
    @required this.login,
    @required this.signUp,
    @required this.refreshToken,
    @required this.inputConverter,
  })  : assert(login != null && signUp != null && refreshToken != null),
        super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is LoginEvent) {
      final inputEither = inputConverter.stringToLoginBody(
          username: event.username, password: event.password);
      yield* inputEither.fold(
        (failure) async* {
          yield failure.mapToState;
        },
        (body) async* {
          yield LoadingState();
          final failureOrResult =
              await login(lgn.Params(body: LoginBodyModel.fromEntity(body)));
          yield* _eitherLoadedOrErrorState(failureOrResult, event);
        },
      );
    } else if (event is SignUpEvent) {
      final inputEither = inputConverter.stringToSignUpBody(
        name: event.name,
        lastName: event.lastName,
        birthDate: event.birthDate,
        gender: event.gender,
        mobile: event.mobile,
        nCode: event.nCode,
        telephone: event.telephone,
      );

      yield* inputEither.fold(
        (failure) async* {
          yield failure.mapToState;
        },
        (body) async* {
          yield LoadingState();
          final failureOrResult =
              await signUp(sgn.Params(body: SignUpBodyModel.fromEntity(body)));
          yield* _eitherLoadedOrErrorState(failureOrResult, event);
        },
      );
    } else if (event is RefreshEvent) {
      RefreshTokenBody body = RefreshTokenBody(
        accessToken: event.accessToken,
        refreshToken: event.refreshToken,
        username: event.username,
      );
      final failureOrResult = await refreshToken(
          rft.Params(body: RefreshTokenBodyModel.fromEntity(body)));
      yield* _eitherLoadedOrErrorState(failureOrResult, event);
    }
  }

  Stream<AuthenticationState> _eitherLoadedOrErrorState(
      Either<Failure, LoginResult> failureOrTrivia,
      AuthenticationEvent event) async* {
    yield failureOrTrivia.fold(
      (failure) => event is RefreshEvent
          ? RefreshTokenFailureState()
          : AuthenticationFailureState(message: failure.mapToString),
      (body) => event is LoginEvent
          ? LoginSuccessState(body: body)
          : event is SignUpEvent
              ? SignUpSuccessState(body: body)
              : RefreshSuccessState(body: body),
    );
  }
}

extension Mapping on Failure {
  String get mapToString {
    switch (this.runtimeType) {
      case ServerFailure:
        return (this as ServerFailure).message ??
            Strings.SERVER_FAILURE_MESSAGE;
      case AuthenticationFailure:
        return Strings.AUTHENTICATION_FAILURE_MESSAGE;
      case AuthorizationFailure:
        return Strings.CACHE_FAILURE_MESSAGE;
      case InvalidNameFailure:
        return Strings.INVALID_NAME_MESSAGE;
      case InvalidLastNameFailure:
        return Strings.INVALID_LAST_NAME_MESSAGE;
      case InvalidBirthDayFailure:
        return Strings.INVALID_BIRTH_DATE_MESSAGE;
      case InvalidMobileFailure:
      case InvalidPasswordFailure:
        return Strings.INVALID_MOBILE_MESSAGE;
      case InvalidNationalCodeFailure:
      case InvalidUsernameFailure:
        return Strings.INVALID_NATIONAL_CODE_MESSAGE;
      case InvalidTelephoneFailure:
        return Strings.INVALID_TELEPHONE_MESSAGE;
      case InvalidGenderFailure:
        return Strings.INVALID_GENDER_MESSAGE;
    }
    return Strings.SERVER_FAILURE_MESSAGE;
  }

  AuthenticationState get mapToState {
    switch (this.runtimeType) {
      case InvalidUsernameFailure:
        return NationalCodeFailureState(message: this.mapToString);
      case InvalidPasswordFailure:
        return PhoneNumberFailureState(message: this.mapToString);
      case InvalidNameFailure:
        return NameFailureState(message: this.mapToString);
      case InvalidLastNameFailure:
        return LastNameFailureState(message: this.mapToString);
      case InvalidBirthDayFailure:
        return BirthDateFailureState(message: this.mapToString);
      case InvalidMobileFailure:
        return PhoneNumberFailureState(message: this.mapToString);
      case InvalidNationalCodeFailure:
        return NationalCodeFailureState(message: this.mapToString);
      case InvalidTelephoneFailure:
        return TelephoneFailureState(message: this.mapToString);
      case InvalidGenderFailure:
        return GenderFailureState(message: this.mapToString);
    }
    return AuthenticationFailureState(message: this.mapToString);
  }
}
