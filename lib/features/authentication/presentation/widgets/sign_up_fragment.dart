import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rxdart/rxdart.dart';

import '../../../../core/components/bottom_sheet/gender_picker_fragment.dart';
import '../../../../core/components/button/my_button.dart';
import '../../../../core/components/input/material_text_field.dart';
import '../../../../core/components/text/my_text.dart';
import '../../../../core/toast/toast.dart';
import '../../../../core/utils/input_converter.dart';
import '../../../../core/utils/tools.dart';
import '../../../../core/utils/values.dart';
import '../../../appointment/presentation/pages/appointments_page.dart';
import '../bloc/authentication_bloc.dart';

class SignUpFragment extends StatefulWidget {
  final bool isForLogin;

  const SignUpFragment({Key key, this.isForLogin = false}) : super(key: key);

  @override
  _SignUpFragmentState createState() => _SignUpFragmentState();
}

class _SignUpFragmentState extends State<SignUpFragment> {
  bool _loading = false;
  int _genderInt = MALE;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _nationalCodeController = TextEditingController();
  TextEditingController _genderController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _cellPhoneController = TextEditingController();
  TextEditingController _birthDayController = TextEditingController();

  // TextEditingController _educationController = TextEditingController();
  // TextEditingController _jobController = TextEditingController();
  // TextEditingController _relationshipController = TextEditingController();
  // TextEditingController _childCountController = TextEditingController();
  // TextEditingController _addressController = TextEditingController();

  FocusNode _nameFocusNode = FocusNode();
  FocusNode _lastNameFocusNode = FocusNode();
  FocusNode _nationalCodeFocusNode = FocusNode();
  FocusNode _genderFocusNode = FocusNode();
  FocusNode _phoneFocusNode = FocusNode();
  FocusNode _cellPhoneFocusNode = FocusNode();
  FocusNode _educationFocusNode = FocusNode();
  FocusNode _jobFocusNode = FocusNode();
  FocusNode _birthDayFocusNode = FocusNode();
  FocusNode _relationshipFocusNode = FocusNode();
  FocusNode _childCountFocusNode = FocusNode();
  FocusNode _addressFocusNode = FocusNode();

  String _nameError = "";
  String _lastNameError = "";
  String _nationalCodeError = "";
  String _genderError = "";
  String _phoneError = "";
  String _cellPhoneError = "";
  // String _educationError = "";
  // String _jobError = "";
  String _birthDayError = "";
  // String _relationshipError = "";
  // String _childCountError = "";
  // String _addressError = "";

  StreamController<void> _genderTextController;

  @override
  void dispose() {
    _genderTextController.close();
    _nameController.dispose();
    _nameFocusNode.dispose();
    _lastNameFocusNode.dispose();
    _nationalCodeFocusNode.dispose();
    _genderFocusNode.dispose();
    _phoneFocusNode.dispose();
    _cellPhoneFocusNode.dispose();
    _educationFocusNode.dispose();
    _jobFocusNode.dispose();
    _birthDayFocusNode.dispose();
    _relationshipFocusNode.dispose();
    _childCountFocusNode.dispose();
    _addressFocusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _genderTextController = BehaviorSubject();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _size.width,
      height: _size.height,
      child: LayoutBuilder(
        builder: (context, constraint) {
          return SingleChildScrollView(
            child: Container(
              constraints: BoxConstraints(minHeight: constraint.maxHeight),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                textDirection: TextDirection.rtl,
                children: <Widget>[
                  _titleAndImage,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    textDirection: TextDirection.rtl,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _name,
                      _lastName,
                      _nationalCode,
                      _gender,
                      _phone,
                      _cellPhone,
                      _birthDate,
                      // _education,
                      // _job,
                      // _relationship,
                      // _childCount,
                      // _address,
                      _signUpBtn,
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget get _titleAndImage => SizedBox(
        width: _size.width * 0.9,
        child: Row(
          textDirection: Tools.textDirection,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _image,
            SizedBox(width: 16),
            Flexible(
              child: Column(
                textDirection: Tools.textDirection,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MyText(
                    text: 'ثــبــت نـــام',
                    color: MyColors.hunterGreen,
                    fontSize: 28,
                    fontWeight: MyFonts.bold,
                  ),
                  SizedBox(height: 6),
                  MyText(
                    textAlign: TextAlign.justify,
                    text: 'با پر کردن مشخصات خود اقدام به ثبت نام کنید',
                    color: MyColors.combuGreen,
                    textOverflow: TextOverflow.clip,
                    fontSize: 15,
                    fontWeight: MyFonts.light,
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  Widget get _image => SvgPicture.asset(
        "assets/svgs/sign_up.svg",
        width: _size.width * 0.3,
        height: _size.width * 0.3,
      );

  Widget get _name => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is NameFailureState)
              _nameError = state.message;
            else
              _nameError = '';
          },
          builder: (context, state) => MaterialTextField(
            errorText: _nameError,
            controller: _nameController,
            focusNode: _nameFocusNode,
            label: "نام",
            inputType: TextInputType.name,
            maxLength: 50,
            textInputAction: TextInputAction.next,
            nextFocusNode: _lastNameFocusNode,
          ),
        ),
      );

  Widget get _lastName => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is LastNameFailureState)
              _lastNameError = state.message;
            else
              _lastNameError = '';
          },
          builder: (context, state) => MaterialTextField(
            errorText: _lastNameError,
            controller: _lastNameController,
            focusNode: _lastNameFocusNode,
            label: "نام خانوادگی",
            inputType: TextInputType.name,
            maxLength: 50,
            textInputAction: TextInputAction.next,
            nextFocusNode: _nationalCodeFocusNode,
          ),
        ),
      );

  Widget get _nationalCode => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is NationalCodeFailureState)
              _nationalCodeError = state.message;
            else
              _nationalCodeError = '';
          },
          builder: (context, state) => MaterialTextField(
            errorText: _nationalCodeError,
            controller: _nationalCodeController,
            focusNode: _nationalCodeFocusNode,
            label: "کد ملی",
            inputType: TextInputType.phone,
            maxLength: 10,
            textInputAction: TextInputAction.next,
            onSubmit: () => showGenderBottomSheet(),
          ),
        ),
      );

  Widget get _gender => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is GenderFailureState)
              _genderError = state.message;
            else
              _genderError = '';
          },
          builder: (context, state) => StreamBuilder<void>(
              stream: _genderTextController.stream,
              builder: (context, snapshot) {
                return MaterialTextField(
                  errorText: _genderError,
                  controller: _genderController,
                  focusNode: _genderFocusNode,
                  label: "جنسیت",
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                    showGenderBottomSheet();
                  },
                  hasInteractive: false,
                  inputType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                  nextFocusNode: _phoneFocusNode,
                );
              }),
        ),
      );

  Widget get _phone => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is TelephoneFailureState)
              _phoneError = state.message;
            else
              _phoneError = '';
          },
          builder: (context, state) => MaterialTextField(
            errorText: _phoneError,
            controller: _phoneController,
            focusNode: _phoneFocusNode,
            label: "شماره تلفن",
            inputType: TextInputType.phone,
            maxLength: 11,
            textInputAction: TextInputAction.next,
            nextFocusNode: _cellPhoneFocusNode,
          ),
        ),
      );

  Widget get _cellPhone => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is PhoneNumberFailureState)
              _cellPhoneError = state.message;
            else
              _cellPhoneError = '';
          },
          builder: (context, state) => MaterialTextField(
            errorText: _cellPhoneError,
            controller: _cellPhoneController,
            focusNode: _cellPhoneFocusNode,
            label: "شماره تلفن همراه",
            inputType: TextInputType.phone,
            maxLength: 11,
            textInputAction: TextInputAction.next,
            nextFocusNode: _birthDayFocusNode,
          ),
        ),
      );

  Widget get _birthDate => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is BirthDateFailureState)
              _birthDayError = state.message;
            else
              _birthDayError = '';
          },
          builder: (context, snapshot) => MaterialTextField(
            errorText: _birthDayError,
            controller: _birthDayController,
            focusNode: _birthDayFocusNode,
            label: "تاریخ تولد (سال)",
            inputType: TextInputType.phone,
            maxLength: 4,
            textInputAction: TextInputAction.done,
            // nextFocusNode: _passwordFocusNode,
          ),
        ),
      );

  // Widget get _education => Container(
  //       margin: EdgeInsets.only(top: 16),
  //       width: _size.width * 0.8,
  //       child: StreamBuilder<String>(
  //         stream: _nameErrorController.stream,
  //         builder: (context, snapshot) {
  //           _nameError = snapshot?.data ?? "";
  //           return MaterialTextField(
  //             errorText: _nameError,
  //             controller: _nameController,
  //             focusNode: _nameFocusNode,
  //             label: "نام",
  //             inputType: TextInputType.name,
  //             maxLength: 11,
  //             textInputAction: TextInputAction.next,
  //             // nextFocusNode: _passwordFocusNode,
  //           );
  //         },
  //       ),
  //     );
  //
  // Widget get _job => Container(
  //       margin: EdgeInsets.only(top: 16),
  //       width: _size.width * 0.8,
  //       child: StreamBuilder<String>(
  //         stream: _nameErrorController.stream,
  //         builder: (context, snapshot) {
  //           _nameError = snapshot?.data ?? "";
  //           return MaterialTextField(
  //             errorText: _nameError,
  //             controller: _nameController,
  //             focusNode: _nameFocusNode,
  //             label: "نام",
  //             inputType: TextInputType.name,
  //             maxLength: 11,
  //             textInputAction: TextInputAction.next,
  //             // nextFocusNode: _passwordFocusNode,
  //           );
  //         },
  //       ),
  //     );
  //
  // Widget get _relationship => Container(
  //       margin: EdgeInsets.only(top: 16),
  //       width: _size.width * 0.8,
  //       child: StreamBuilder<String>(
  //         stream: _nameErrorController.stream,
  //         builder: (context, snapshot) {
  //           _nameError = snapshot?.data ?? "";
  //           return MaterialTextField(
  //             errorText: _nameError,
  //             controller: _nameController,
  //             focusNode: _nameFocusNode,
  //             label: "نام",
  //             inputType: TextInputType.name,
  //             maxLength: 11,
  //             textInputAction: TextInputAction.next,
  //             // nextFocusNode: _passwordFocusNode,
  //           );
  //         },
  //       ),
  //     );
  //
  // Widget get _childCount => Container(
  //       margin: EdgeInsets.only(top: 16),
  //       width: _size.width * 0.8,
  //       child: StreamBuilder<String>(
  //         stream: _nameErrorController.stream,
  //         builder: (context, snapshot) {
  //           _nameError = snapshot?.data ?? "";
  //           return MaterialTextField(
  //             errorText: _nameError,
  //             controller: _nameController,
  //             focusNode: _nameFocusNode,
  //             label: "نام",
  //             inputType: TextInputType.name,
  //             maxLength: 11,
  //             textInputAction: TextInputAction.next,
  //             // nextFocusNode: _passwordFocusNode,
  //           );
  //         },
  //       ),
  //     );
  //
  // Widget get _address => Container(
  //       margin: EdgeInsets.only(top: 16),
  //       width: _size.width * 0.8,
  //       child: StreamBuilder<String>(
  //         stream: _nameErrorController.stream,
  //         builder: (context, snapshot) {
  //           _nameError = snapshot?.data ?? "";
  //           return MaterialTextField(
  //             errorText: _nameError,
  //             controller: _nameController,
  //             focusNode: _nameFocusNode,
  //             label: "نام",
  //             inputType: TextInputType.name,
  //             maxLength: 11,
  //             textInputAction: TextInputAction.next,
  //             // nextFocusNode: _passwordFocusNode,
  //           );
  //         },
  //       ),
  //     );

  Widget get _signUpBtn => Padding(
        padding: EdgeInsets.only(bottom: 24, top: 24),
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            _loading = state is LoadingState;
            if (state is AuthenticationFailureState) {
              MyToast.showErrorToast(context: context, message: state.message);
              return;
            }
            if (state is SignUpSuccessState) {
              if (widget.isForLogin)
                Navigator.of(context).pop(true);
              else
                Navigator.of(context)
                    .pushReplacementNamed(AppointmentsPage.routeName);
              return;
            }
          },
          builder: (context, state) {
            return MyButton(
              btnHeight: 56,
              text: "ثبت نام",
              loading: state is LoadingState,
              onClick: onSignUpClick,
              width: _size.width * 0.6,
            );
          },
        ),
      );

  void showGenderBottomSheet() async {
    int result = await showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return GenderPickerFragment();
        });
    if (result == null) return;
    _genderInt = result;
    FocusScope.of(context).requestFocus(_phoneFocusNode);
    switch (result) {
      case MALE:
        _genderController.text = "مرد";
        break;
      case FEMALE:
        _genderController.text = "زن";
        break;
    }
    _genderTextController.add(null);
  }

  void onSignUpClick() {
    if (_loading) return;
    BlocProvider.of<AuthenticationBloc>(context).add(
      SignUpEvent(
          birthDate: _birthDayController.text,
          gender: _genderInt,
          lastName: _lastNameController.text,
          mobile: _cellPhoneController.text,
          nCode: _nationalCodeController.text,
          name: _nameController.text,
          telephone: _phoneController.text),
    );
  }

  Size get _size => MediaQuery.of(context).size;
}
