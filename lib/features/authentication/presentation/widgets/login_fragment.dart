import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/components/button/my_button.dart';
import '../../../../core/components/input/material_text_field.dart';
import '../../../../core/components/text/my_text.dart';
import '../../../../core/toast/toast.dart';
import '../../../../core/utils/tools.dart';
import '../../../../core/utils/values.dart';
import '../../../appointment/presentation/pages/appointments_page.dart';
import '../bloc/authentication_bloc.dart';

class LoginFragment extends StatefulWidget {
  final bool isForLogin;

  const LoginFragment({Key key, this.isForLogin = false}) : super(key: key);

  @override
  _LoginFragmentState createState() => _LoginFragmentState();
}

class _LoginFragmentState extends State<LoginFragment> {
  bool _loading = false;

  TextEditingController _usernameController = TextEditingController();

  TextEditingController _passwordController = TextEditingController();

  FocusNode _usernameFocusNode = FocusNode();

  FocusNode _passwordFocusNode = FocusNode();

  String _usernameError = "";

  String _passwordError = "";

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _usernameFocusNode.dispose();
    _passwordFocusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _size.width,
      height: _size.height,
      child: LayoutBuilder(
        builder: (context, constraint) {
          return SingleChildScrollView(
            child: Container(
              constraints: BoxConstraints(minHeight: constraint.maxHeight),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                textDirection: TextDirection.rtl,
                children: <Widget>[
                  _titleAndImage,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    textDirection: TextDirection.rtl,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _username,
                      _password,
                      _enterBtn,
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget get _titleAndImage => SizedBox(
        width: _size.width,
        child: Stack(
          textDirection: Tools.textDirection,
          overflow: Overflow.visible,
          children: [
            Positioned(
              bottom: _size.height * 0.15,
              left: 16,
              child: SizedBox(
                width: _size.width * 0.6,
                child: Padding(
                  padding: EdgeInsets.only(right: 16),
                  child: Column(
                    textDirection: Tools.textDirection,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      MyText(
                        text: 'ورود به اپلیکیشن',
                        color: MyColors.hunterGreen,
                        fontSize: 28,
                        fontWeight: MyFonts.bold,
                      ),
                      SizedBox(height: 12),
                      MyText(
                        textAlign: TextAlign.justify,
                        text:
                            'با استفاده از کد ملی و شماره همراه وارد حســاب کاربــری خود شویــد',
                        color: MyColors.combuGreen,
                        textOverflow: TextOverflow.clip,
                        fontSize: 15,
                        fontWeight: MyFonts.light,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _image,
          ],
        ),
      );

  Widget get _image => Transform.translate(
        offset: Offset(_size.width * 0.4, 0),
        child: Transform.scale(
          scale: 1.5,
          child: SvgPicture.asset(
            "assets/svgs/login.svg",
            width: _size.width,
            height: _size.height * 0.5,
            fit: BoxFit.cover,
          ),
        ),
      );

  Widget get _username => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is NationalCodeFailureState)
              _usernameError = state.message;
            else
              _usernameError = '';
          },
          builder: (context, state) {
            return MaterialTextField(
              errorText: _usernameError,
              controller: _usernameController,
              focusNode: _usernameFocusNode,
              label: "نام کاربری (کد ملی)",
              inputType: TextInputType.number,
              maxLength: 10,
              textInputAction: TextInputAction.next,
              nextFocusNode: _passwordFocusNode,
            );
          },
        ),
      );

  Widget get _password => Container(
        margin: EdgeInsets.only(top: 16),
        width: _size.width * 0.8,
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            if (state is PhoneNumberFailureState)
              _passwordError = state.message;
            else
              _passwordError = '';
          },
          builder: (context, state) {
            return MaterialTextField(
              errorText: _passwordError,
              controller: _passwordController,
              focusNode: _passwordFocusNode,
              label: "رمز عبور (شماره همراه)",
              maxLength: 11,
              inputType: TextInputType.number,
              isPassword: true,
              textInputAction: TextInputAction.done,
            );
          },
        ),
      );

  Widget get _enterBtn => Padding(
        padding: EdgeInsets.only(bottom: 24, top: 24),
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
          listener: (context, state) {
            _loading = state is LoadingState;
            if (state is AuthenticationFailureState) {
              MyToast.showErrorToast(context: context, message: state.message);
              return;
            }
            if (state is LoginSuccessState) {
              if (widget.isForLogin)
                Navigator.of(context).pop(true);
              else
                Navigator.of(context)
                    .pushReplacementNamed(AppointmentsPage.routeName);
              return;
            }
          },
          builder: (context, state) {
            return MyButton(
              btnHeight: 56,
              text: "ورود",
              loading: state is LoadingState,
              onClick: onLoginClick,
              width: _size.width * 0.6,
            );
          },
        ),
      );

  void onLoginClick() {
    if (_loading) return;
    // String username = _usernameController.text;
    // String password = _passwordController.text;
    // if (username == "") {
    //   _usernameErrorController.add("این فیلد الزامی است");
    //   _usernameFocusNode.requestFocus();
    //   return;
    // }
    // if (password == "") {
    //   _passwordErrorController.add("این فیلد الزامی است");
    //   _passwordFocusNode.requestFocus();
    //   return;
    // }
    BlocProvider.of<AuthenticationBloc>(context).add(LoginEvent(
      username: _usernameController.text,
      password: _passwordController.text,
    ));
  }

  Size get _size => MediaQuery.of(context).size;
}
