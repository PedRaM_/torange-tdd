import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/components/button/my_button.dart';
import '../../../../core/components/text/my_text.dart';
import '../../../../core/utils/tools.dart';
import '../../../../core/utils/values.dart';
import '../cubits/page_switcher/page_switcher_cubit.dart';
import '../widgets/login_fragment.dart';
import '../widgets/sign_up_fragment.dart';

class LandingPage extends StatefulWidget {
  static const String pageRoute = "/LandingPage";
  final bool isForLogin;

  const LandingPage({Key key, this.isForLogin = false}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  void initState() {
    super.initState();
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
    if (widget.isForLogin)
      BlocProvider.of<PageSwitcherCubit>(context).setLoginPage();
    else
      BlocProvider.of<PageSwitcherCubit>(context).setStartPage();
  }

  Future<bool> _onBackPressed() async {
    if (BlocProvider.of<PageSwitcherCubit>(context).state is IsLoginPage) {
      BlocProvider.of<PageSwitcherCubit>(context).setStartPage();
      return false;
    }
    return true;
  }

  @override
  void dispose() {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: _body,
        ),
      ),
    );
  }

  Widget get _body => SafeArea(
        child: BlocBuilder<PageSwitcherCubit, PageSwitcherState>(
          builder: (context, state) => AnimatedSwitcher(
            duration: Duration(milliseconds: 300),
            switchInCurve: Curves.easeIn,
            switchOutCurve: Curves.easeOut,
            child: state is AuthenticateUiStart
                ? Column(
                    key: ValueKey(1),
                    textDirection: Tools.textDirection,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      _titleAndImage,
                      _actions,
                    ],
                  )
                : state is IsLoginPage
                    ? state.isLogin
                        ? LoginFragment(
                            key: ValueKey(2),
                            isForLogin: widget.isForLogin,
                          )
                        : SignUpFragment(
                            key: ValueKey(3),
                            isForLogin: widget.isForLogin,
                          )
                    : SizedBox(),
          ),
        ),
      );

  Widget get _titleAndImage => SizedBox(
        width: _size.width,
        child: Stack(
          textDirection: Tools.textDirection,
          children: [
            Padding(
              padding: EdgeInsets.only(right: 32, left: 32),
              child: Column(
                textDirection: Tools.textDirection,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  MyText(
                    text: 'تـــرنـــج',
                    color: MyColors.hunterGreen,
                    fontSize: 36,
                    fontWeight: MyFonts.bold,
                  ),
                  SizedBox(height: 12),
                  MyText(
                    text: 'اپلیکیشنی برای کم کردن\nزمان انتظار شما در مـطب',
                    color: MyColors.combuGreen,
                    fontSize: 20,
                    fontWeight: MyFonts.light,
                  ),
                ],
              ),
            ),
            _image,
          ],
        ),
      );

  Widget get _image => Align(
        alignment: Alignment.centerLeft,
        child: Transform.translate(
          offset: Offset(-_size.width * 0.12, 42),
          child: SvgPicture.asset(
            'assets/svgs/main_page_image.svg',
            width: _size.width,
            height: _size.height * 0.65,
            alignment: Alignment.bottomCenter,
          ),
        ),
      );

  Widget get _actions => Column(
        textDirection: Tools.textDirection,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _login,
          SizedBox(
            height: 16,
          ),
          _signUp,
        ],
      );

  Widget get _login => MyButton(
        width: _size.width * 0.6,
        btnHeight: 56,
        text: 'ورود',
        loading: false,
        onClick: () =>
            BlocProvider.of<PageSwitcherCubit>(context).setLoginPage(),
      );

  Widget get _signUp => MyButton(
        width: _size.width * 0.6,
        btnHeight: 56,
        text: 'ثبت نام',
        loading: false,
        colors: [MyColors.hunterGreen, MyColors.hunterGreen],
        onClick: () =>
            BlocProvider.of<PageSwitcherCubit>(context).setSignUpPage(),
      );

  Size get _size => MediaQuery.of(context).size;
}
