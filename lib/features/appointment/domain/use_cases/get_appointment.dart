import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../entities/appointment.dart';
import '../repositories/appointment_repository.dart';

class GetAppointment extends UseCase<List<Appointment>, NoParams> {
  final AppointmentRepository repository;

  GetAppointment(this.repository);

  @override
  Future<Either<Failure, List<Appointment>>> call(NoParams params) async =>
      await repository.getAppointments();
}
