import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Appointment extends Equatable {
  final String date;
  final String time;
  final List<String> descriptions;

  @override
  List<Object> get props => [
        date,
        time,
        descriptions,
      ];

  Appointment({
    @required this.date,
    @required this.time,
    @required this.descriptions,
  });

  String get title {
    String title = '';
    if (descriptions != null && descriptions.length > 0)
      title = descriptions[0];
    return title;
  }

  String get description {
    String _description = '';
    if (descriptions != null && descriptions.length > 1) {
      for (int i = 1; i < descriptions.length; i++)
        _description = '$_description${i == 1 ? '' : '، '}${descriptions[i]}';
    }
    return _description;
  }

  String get getTime {
    List<String> timeArray = time.split(':');
    if (timeArray.length <= 2) return time;
    return '${timeArray[0]}:${timeArray[1]}';
  }
}
