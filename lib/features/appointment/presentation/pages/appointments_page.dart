import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:torange/features/symptoms/presentation/pages/main_symptoms.dart';
import '../../../../injection_container.dart';
import '../../../../core/components/dialog/message_dialog.dart';
import '../../../../core/components/loading/loading.dart';
import '../../../../core/components/scaffold/my_scaffold.dart';
import '../../../../core/components/state/empty_state.dart';
import '../../../../core/components/state/error_state.dart';
import '../../../../core/components/text/my_text.dart';
import '../../../../core/utils/values.dart';
import '../../domain/entities/appointment.dart';
import '../bloc/appointment_bloc.dart';
import '../widgets/appointment_item_view.dart';
import '../../../authentication/presentation/pages/landing_page.dart';

class AppointmentsPage extends StatefulWidget {
  static const String routeName = "/HomePage";

  @override
  _AppointmentsPageState createState() => _AppointmentsPageState();
}

class _AppointmentsPageState extends State<AppointmentsPage>
    with SingleTickerProviderStateMixin {
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  AnimationController _hideFabAnimationController;
  Animation<double> _hideFabAnim;

  Size _size;

  @override
  void dispose() {
    _hideFabAnimationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    _hideFabAnimationController = AnimationController(
        vsync: this,
        duration: Duration(
          milliseconds: 200,
        ));
    _hideFabAnim = Tween<double>(begin: 1.0, end: 0.0).animate(CurvedAnimation(
        parent: _hideFabAnimationController, curve: Curves.ease));
    getAppointments();
  }

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    return NotificationListener(
      onNotification: _handleScrollNotification,
      child: MyScaffold(
        onMoreClick: (GlobalKey key) => onMoreClick(key),
        fabChild: ScaleTransition(
          scale: _hideFabAnim,
          child: FloatingActionButton(
            backgroundColor: MyColors.amazon,
            elevation: 4,
            highlightElevation: 6,
            child: Icon(
              MyIcons.add,
              color: Colors.white,
            ),
            onPressed: () => addAppointment(),
          ),
        ),
        backgroundColor: Color(0xFFFCFCFC),
        hasAppbar: true,
        hasBackArrow: false,
        titleName: "تـــرنـــج",
        scaffoldKey: _scaffoldKey,
        body: SizedBox(
          width: _size.width,
          height: double.infinity,
          child: BlocConsumer<AppointmentBloc, AppointmentState>(
            listener: (context, state) async {
              if (state is AuthorizationFailureState) {
                dynamic isLoggedIn = await Navigator.of(context)
                    .pushNamed(LandingPage.pageRoute, arguments: true);
                if ((isLoggedIn is bool) && isLoggedIn) {
                  getAppointments();
                  return;
                }
                return;
              }
            },
            builder: (context, state) {
              return state is AppointmentInitial
                  ? SizedBox()
                  : AnimatedSwitcher(
                      child: state is LoadingState
                          ? Center(
                              child: Loading(),
                            )
                          : state is GetAppointmentsSuccessState
                              ? _body(state.items)
                              : state is GetAppointmentFailureState
                                  ? _errorWidget(state.message)
                                  : SizedBox(),
                      duration: Duration(milliseconds: 300),
                      switchOutCurve: Curves.easeOut,
                      switchInCurve: Curves.easeIn,
                    );
            },
          ),
        ),
      ),
    );
  }

  Widget _errorWidget(String message) => ErrorState(
        key: ValueKey(1),
        message: message,
        onActionClick: () => getAppointments(),
      );

  Widget _body(List<Appointment> appointments) => appointments.length == 0
      ? EmptyState(
          key: ValueKey(2),
          size: Size(_size.width, _size.height),
          message:
              "لیست قرار ملاقات های شما خالی می باشد. برای افزودن یک قرار ملاقات بر روی افزودن کلیک کنید",
          onActionClick: () => addAppointment(),
        )
      : Align(
          alignment: Alignment.topCenter,
          child: ListView.builder(
            padding: EdgeInsets.only(top: 12, bottom: 12),
            key: ValueKey(3),
            shrinkWrap: true,
            itemBuilder: (context, index) => AppointmentItemView(
              appointment: appointments[index],
            ),
            itemCount: appointments.length,
          ),
        );

  void getAppointments() =>
      BlocProvider.of<AppointmentBloc>(context).add(GetAppointmentsEvent());

  void addAppointment() async {
    var result =
        await Navigator.of(context).pushNamed(AddAppointmentPage.routeName);
    if (result is bool && result) {
      getAppointments();
    }
  }

  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification.depth == 0) {
      if (notification is UserScrollNotification) {
        final UserScrollNotification userScroll = notification;
        switch (userScroll.direction) {
          case ScrollDirection.forward:
            if (userScroll.metrics.maxScrollExtent !=
                userScroll.metrics.minScrollExtent) {
              _hideFabAnimationController.reverse();
            }
            break;
          case ScrollDirection.reverse:
            if (userScroll.metrics.maxScrollExtent !=
                userScroll.metrics.minScrollExtent) {
              _hideFabAnimationController.forward();
            }
            break;
          case ScrollDirection.idle:
            break;
        }
      }
    }
    return true;
  }

  void onMoreClick(GlobalKey key) async {
    final RenderBox _renderBox = key.currentContext.findRenderObject();
    final position = _renderBox.localToGlobal(Offset.zero);
    await showMenu(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2),
      ),
      context: context,
      position: RelativeRect.fromLTRB(position.dx, position.dy + 42, 28, 24),
      items: [
        PopupMenuItem<int>(
          value: 1,
          height: 28,
          textStyle: TextStyle(
            fontWeight: MyFonts.bold,
            fontFamily: MyFonts.yekan,
            color: MyColors.spaceCadet,
            fontSize: 12,
          ),
          child: Align(
            alignment: Alignment.centerRight,
            child: MyText(
              text: "خروج از حساب",
              color: MyColors.spaceCadet,
              fontSize: 12,
            ),
          ),
        ),
      ],
      elevation: 4.0,
    ).then((value) {
      switch (value) {
        case 1:
          TwoActionDialog.show(
            context,
            onAcceptClick: () async {
              sl().setLogged(false);
              await Future.delayed(Duration(milliseconds: 100));
              Navigator.of(context).pushNamedAndRemoveUntil(
                  LandingPage.pageRoute, (Route<dynamic> route) => false,
                  arguments: false);
            },
          );
          break;
      }
    });
  }
}
