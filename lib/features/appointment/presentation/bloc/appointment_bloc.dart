import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../../../../core/utils/values.dart';
import '../../domain/entities/appointment.dart';
import '../../domain/use_cases/get_appointment.dart';

part 'appointment_event.dart';
part 'appointment_state.dart';

class AppointmentBloc extends Bloc<AppointmentEvent, AppointmentState> {
  final GetAppointment getAppointment;

  AppointmentBloc({
    @required this.getAppointment,
  })  : assert(getAppointment != null),
        super(AppointmentInitial());

  @override
  Stream<AppointmentState> mapEventToState(
    AppointmentEvent event,
  ) async* {
    if (event is GetAppointmentsEvent) {
      yield LoadingState();
      final result = await getAppointment(NoParams());
      yield result.fold(
        (failure) => failure.mapToState,
        (items) => GetAppointmentsSuccessState(items: items),
      );
    }
  }
}

extension Mapping on Failure {
  String get mapFailure {
    switch (this.runtimeType) {
      case ServerFailure:
        return (this as ServerFailure).message ??
            Strings.SERVER_FAILURE_MESSAGE;
      case AuthenticationFailure:
        return Strings.AUTHENTICATION_FAILURE_MESSAGE;
    }
    return Strings.SERVER_FAILURE_MESSAGE;
  }

  AppointmentState get mapToState {
    switch (this.runtimeType) {
      case AuthorizationFailure:
        return AuthorizationFailureState();
    }
    return GetAppointmentFailureState(message: this.mapFailure);
  }
}
