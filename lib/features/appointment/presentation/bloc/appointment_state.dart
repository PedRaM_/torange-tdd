part of 'appointment_bloc.dart';

abstract class AppointmentState extends Equatable {
  const AppointmentState();

  @override
  List<Object> get props => [];
}

class AppointmentInitial extends AppointmentState {}

class LoadingState extends AppointmentState {}

class GetAppointmentsSuccessState extends AppointmentState {
  final List<Appointment> items;

  GetAppointmentsSuccessState({@required this.items});

  @override
  List<Object> get props => [items];
}

class AuthorizationFailureState extends AppointmentState {}

class GetAppointmentFailureState extends AppointmentState {
  final String message;

  GetAppointmentFailureState({@required this.message});
  @override
  List<Object> get props => [];
}
