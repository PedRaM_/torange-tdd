import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../core/components/text/my_text.dart';
import '../../../../core/utils/tools.dart';
import '../../../../core/utils/values.dart';
import '../../domain/entities/appointment.dart';

class AppointmentItemView extends StatelessWidget {
  final Appointment appointment;

  const AppointmentItemView({Key key, this.appointment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 16, left: 16, top: 12, bottom: 12),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Color(0x1b2e2d4d),
            offset: Offset(0, 2),
            blurRadius: 10,
            spreadRadius: 0,
          ),
        ],
      ),
      child: Column(
        textDirection: Tools.textDirection,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          _title,
          if (_hasDescription) _description,
          _date,
        ],
      ),
    );
  }

  Widget get _title => Flexible(
        child: Padding(
          padding: EdgeInsets.only(right: 16, left: 16, top: 8),
          child: MyText(
            color: MyColors.spaceCadet,
            text: appointment.title,
            fontSize: 14,
            textAlign: TextAlign.start,
            fontWeight: MyFonts.bold,
            textOverflow: TextOverflow.clip,
          ),
        ),
      );

  Widget get _description => Flexible(
        child: Padding(
          padding: EdgeInsets.only(right: 16, left: 16, top: 4),
          child: MyText(
            color: MyColors.spaceCadet.withOpacity(0.8),
            text: appointment.description,
            fontSize: 12,
            textAlign: TextAlign.start,
            fontWeight: MyFonts.light,
            textOverflow: TextOverflow.clip,
          ),
        ),
      );

  Widget get _date => Padding(
        padding: EdgeInsets.only(right: 16, left: 16, top: 16, bottom: 8),
        child: Row(
          textDirection: TextDirection.ltr,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              MyIcons.timeTable,
              color: Color(0xFFAAAAAA),
              size: 12,
            ),
            SizedBox(width: 6),
            MyText(
              text: appointment.date,
              color: Color(0xFFAAAAAA),
              fontWeight: MyFonts.light,
              fontSize: 11,
            ),
            Container(
              width: 3,
              height: 3,
              margin: EdgeInsets.only(right: 6, left: 6, top: 4),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: MyColors.spaceCadet.withOpacity(0.5),
              ),
            ),
            MyText(
              text: appointment.getTime,
              color: Color(0xFFAAAAAA),
              fontWeight: MyFonts.light,
              fontSize: 12,
            ),
          ],
        ),
      );

  bool get _hasDescription =>
      appointment.description != null && appointment.description.length > 0;
}
