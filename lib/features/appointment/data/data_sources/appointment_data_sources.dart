import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../../core/data_sources/data_sources.dart';
import '../../../../core/error/exceptions.dart';
import '../../../../core/models/base_response_model.dart';
import '../../domain/entities/appointment.dart';
import '../models/appointment_model.dart';

abstract class AppointmentDataSources {
  /// Calls the http://torange.mubabol.ac.ir/api/torange/GetVisit endpoint
  /// with contains [token] in the header to get Appointments.
  ///
  /// Throws a [ServerException] for all error code.
  ///
  Future<List<Appointment>> getAppointments(String token);
}

class AppointmentDataSourcesImpl extends DataSourcesImpl
    implements AppointmentDataSources {
  final http.Client client;

  AppointmentDataSourcesImpl({@required this.client});

  @override
  Future<List<Appointment>> getAppointments(String token) async {
    final response = await client.get(
      'http://torange.mubabol.ac.ir/api/torange/GetVisit',
      headers: {
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      final baseResponse = BaseResponseModel<List<AppointmentModel>>.fromJson(
        json.decode(response.body),
        (data) => (data as List)
            .map((item) => AppointmentModel.fromJson(item))
            .toList(),
      );
      if (baseResponse.isSuccessful)
        return baseResponse.data;
      else
        throw ServerException(message: baseResponse.message);
    } else if (response.statusCode == 401) {
      throw AuthorizationException();
    } else {
      throw ServerException();
    }
  }
}
