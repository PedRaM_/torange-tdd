import 'package:meta/meta.dart';

import '../../domain/entities/appointment.dart';

class AppointmentModel extends Appointment {
  AppointmentModel({
    @required String date,
    @required String time,
    @required List<String> descriptions,
  }) : super(
          date: date,
          descriptions: descriptions,
          time: time,
        );

  factory AppointmentModel.fromJson(Map<String, dynamic> body) =>
      AppointmentModel(
        date: body['DateV'],
        time: body['TimeV'],
        descriptions: body['ChiefComplaintTextList'].cast<String>(),
      );
}
