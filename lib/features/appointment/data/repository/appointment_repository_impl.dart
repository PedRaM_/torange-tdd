import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/data_sources/data_sources.dart';
import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../../authentication/data/data_sources/authentication_local_data_source.dart';
import '../../../authentication/data/models/refresh_token_body_model.dart';
import '../../../authentication/data/repositories/authentication_repository_impl.dart';
import '../../domain/entities/appointment.dart';
import '../../domain/repositories/appointment_repository.dart';
import '../data_sources/appointment_data_sources.dart';

class AppointmentRepositoryImpl implements AppointmentRepository {
  final NetworkInfo networkInfo;
  final AuthenticationLocalDataSource local;
  final AppointmentDataSources remote;
  final DataSources dataSources;

  AppointmentRepositoryImpl({
    @required this.networkInfo,
    @required this.local,
    @required this.remote,
    @required this.dataSources,
  });

  @override
  Future<Either<Failure, List<Appointment>>> getAppointments() async {
    if (!(await networkInfo.isConnected))
      return Left(ServerFailure(message: NO_INTERNET_MESSAGE));
    try {
      final token = await local.getLoginResultModel();
      final items = await remote.getAppointments(token.accessToken);
      return Right(items);
    } on AuthorizationException {
      try {
        final token = await local.getLoginResultModel();
        await local.setRefreshing(true);
        final newToken = await dataSources.refreshToken(RefreshTokenBodyModel(
          accessToken: token.accessToken,
          refreshToken: token.refreshToken,
          username: token.username,
        ));
        await local.setRefreshing(false);
        final items = await remote.getAppointments(newToken.accessToken);
        return Right(items);
      } on AuthenticationException {
        return _handleError();
      } on AuthorizationException {
        return _handleError();
      }
    } on ServerException catch (error) {
      return Left(ServerFailure(message: error.message));
    }
  }

  Future<Either<Failure, List<Appointment>>> _handleError() async {
    await local.setRefreshing(false);
    await local.setLogged(false);
    return Left(AuthorizationFailure());
  }
}
