import '../../../core/utils/values.dart';
import 'package:collection/collection.dart';

const DeepCollectionEquality _equality = DeepCollectionEquality();

class SymptomAdapter {
  final String name;
  final int id;
  final int parentId;
  List<SymptomAdapter> subItems;
  bool isOpen;
  bool isSelected;

  SymptomAdapter({
    this.name,
    this.id,
    this.parentId,
    this.isOpen = false,
    this.isSelected = false,
    this.subItems,
  });

  bool get stringify => null;

  List<Object> get props => [name, id, parentId, subItems, isOpen, isSelected];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SymptomAdapter &&
          runtimeType == other.runtimeType &&
          equals(props, other.props);

  @override
  int get hashCode => runtimeType.hashCode ^ mapPropsToHashCode(props);

  int mapPropsToHashCode(Iterable props) =>
      _finish(props?.fold(0, _combine) ?? 0);

  int _combine(int hash, dynamic object) {
    if (object is Map) {
      object.forEach((dynamic key, dynamic value) {
        hash = hash ^ _combine(hash, <dynamic>[key, value]);
      });
      return hash;
    }
    if (object is Iterable) {
      for (final value in object) {
        hash = hash ^ _combine(hash, value);
      }
      return hash ^ object.length;
    }

    hash = 0x1fffffff & (hash + object.hashCode);
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  int _finish(int hash) {
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }

  bool equals(list1, list2) {
    if (identical(list1, list2)) return true;
    if (list1 == null || list2 == null) return false;
    final length = list1.length;
    if (length != list2.length) return false;

    for (var i = 0; i < length; i++) {
      final dynamic unit1 = list1[i];
      final dynamic unit2 = list2[i];

      if (unit1 is Iterable || unit1 is Map) {
        if (!_equality.equals(unit1, unit2)) return false;
      } else if (unit1?.runtimeType != unit2?.runtimeType) {
        return false;
      } else if (unit1 != unit2) {
        return false;
      }
    }
    return true;
  }

  set setIsSelected(bool isSelected) {
    this.isSelected = isSelected;
    if (subItems != null)
      for (int i = 0; i < subItems.length; i++) {
        subItems[i].setIsSelected = isSelected;
      }
  }

  set setIsOpen(bool isOpen) {
    this.isOpen = isOpen;
    if (!isOpen && subItems != null)
      for (int i = 0; i < subItems.length; i++) {
        subItems[i].setIsOpen = isOpen;
      }
  }

  set setIsOpenAll(bool isOpen) {
    this.isOpen = isOpen;
    if (subItems != null)
      for (int i = 0; i < subItems.length; i++) {
        subItems[i].setIsOpenAll = isOpen;
      }
  }

  int get getSelectedStatus {
    if (!hasChild)
      return isSelected ? SelectStatus.SELECTED : SelectStatus.NOT_SELECTED;
    int selectedCount = 0;
    int halfSelectedCount = 0;
    for (int i = 0; i < subItems.length; i++) {
      int subStatus = subItems[i].getSelectedStatus;
      if (subStatus == SelectStatus.SELECTED) selectedCount = selectedCount + 1;
      if (subStatus == SelectStatus.HALF_SELECTED)
        halfSelectedCount = halfSelectedCount + 1;
    }
    if (selectedCount == subItems.length) {
      isSelected = true;
      return SelectStatus.SELECTED;
    }
    isSelected = false;
    if (halfSelectedCount > 0) return SelectStatus.HALF_SELECTED;
    if (selectedCount == 0) return SelectStatus.NOT_SELECTED;
    return SelectStatus.HALF_SELECTED;
  }

  bool get isChildSelected {
    if (!hasChild) return isSelected;
    for (int i = 0; i < subItems.length; i++)
      if (subItems[i].isChildSelected) return true;
    return isSelected;
  }

  bool get hasChild => (subItems ?? List()).length > 0;

  List<int> get getSelectedItems {
    List<int> result = List();
    if (isSelected) result.add(id);
    if (!hasChild) return result;
    for (int i = 0; i < subItems.length; i++)
      result.addAll(subItems[i].getSelectedItems);
    return result;
  }
}
