import '../domain/entities/symptom.dart';
import 'symptom_adapter.dart';

class SymptomConverter {
  List<SymptomAdapter> symptomToAdapter(List<Symptom> rawItems) {
    List<SymptomAdapter> items = List();
    rawItems.sort((a, b) => a.parentId.compareTo(b.parentId));
    for (int i = 0; i < rawItems.length; i++) {
      SymptomAdapter symptoms = SymptomAdapter(
        id: rawItems[i].id,
        name: rawItems[i].name,
        parentId: rawItems[i].parentId,
      );
      if (rawItems[i].parentId == -1)
        items.add(symptoms);
      else
        _updateItems(items, symptoms);
    }
    items.sort((a, b) => a.id.compareTo(b.id));
    return items;
  }

  void _updateItems(List<SymptomAdapter> items, SymptomAdapter item) {
    if (items == null) return;
    for (int i = 0; i < items.length; i++) {
      if (items[i].id == item.parentId) {
        if (items[i].subItems == null) items[i].subItems = List();
        items[i].subItems.add(item);
      } else
        _updateItems(items[i].subItems, item);
    }
  }
}
