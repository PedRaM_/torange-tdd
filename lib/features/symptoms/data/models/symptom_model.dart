import '../../domain/entities/symptom.dart';
import 'package:meta/meta.dart';

class SymptomModel extends Symptom {
  SymptomModel({
    @required int id,
    @required int parentId,
    @required String name,
    @required List<int> childIds,
  }) : super(
          childIds: childIds,
          id: id,
          name: name,
          parentId: parentId,
        );

  factory SymptomModel.fromJson(Map<String, dynamic> body) => SymptomModel(
        childIds: body['ListHiExPa'] == null
            ? null
            : (body['ListHiExPa'] as List)
                .map((item) => (item as num).toInt())
                .toList(),
        id: (body['ID'] as num)?.toInt(),
        name: body['Text'] ?? body['Txt'],
        parentId: body['Parent'],
      );
}
