import '../../domain/entities/visit_config.dart';
import 'package:meta/meta.dart';

class VisitConfigModel extends VisitConfig {
  VisitConfigModel({
    @required bool examIsRequired,
    @required bool historyIsRequired,
    @required bool paraIsRequired,
  }) : super(
          examIsRequired: examIsRequired,
          historyIsRequired: historyIsRequired,
          paraIsRequired: paraIsRequired,
        );

  factory VisitConfigModel.fromJson(Map<String, dynamic> body) =>
      VisitConfigModel(
        examIsRequired: body['ExamIsRequired'],
        historyIsRequired: body['HistoryIsRequired'],
        paraIsRequired: body['ParaIsRequired'],
      );

  Map<String, dynamic> toJson() => {
        'ExamIsRequired': examIsRequired,
        'HistoryIsRequired': historyIsRequired,
        'ParaIsRequired': paraIsRequired,
      };
}
