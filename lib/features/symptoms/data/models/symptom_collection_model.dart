import 'package:meta/meta.dart';

import '../../domain/entities/symptom_collection.dart';
import 'symptom_model.dart';
import 'visit_config_model.dart';

class SymptomCollectionModel extends SymptomCollection {
  SymptomCollectionModel({
    @required List<SymptomModel> mainSymp,
    @required List<SymptomModel> exam,
    @required List<SymptomModel> para,
    @required List<SymptomModel> history,
    @required VisitConfigModel config,
  }) : super(
          config: config,
          exam: exam,
          history: history,
          mainSymp: mainSymp,
          para: para,
        );

  factory SymptomCollectionModel.fromJson(Map<String, dynamic> body) =>
      SymptomCollectionModel(
        config: VisitConfigModel.fromJson(body['VisitConfig']),
        exam: body['ExamList'] == null
            ? List()
            : (body['ExamList'] as List)
                .map((item) => SymptomModel.fromJson(item))
                .toList(),
        history: body['HistoryList'] == null
            ? List()
            : (body['HistoryList'] as List)
                .map((item) => SymptomModel.fromJson(item))
                .toList(),
        mainSymp: body['ChiefList'] == null
            ? List()
            : (body['ChiefList'] as List)
                .map((item) => SymptomModel.fromJson(item))
                .toList(),
        para: body['ParaList'] == null
            ? List()
            : (body['ParaList'] as List)
                .map((item) => SymptomModel.fromJson(item))
                .toList(),
      );
}
