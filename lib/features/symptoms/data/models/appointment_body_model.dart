import 'package:meta/meta.dart';

import '../../domain/entities/appointment_body.dart';

class AppointmentBodyModel extends AppointmentBody {
  const AppointmentBodyModel({
    @required List<int> mainSymptoms,
    @required List<int> symptoms,
    @required String nationalCode,
  }) : super(
          mainSymptoms: mainSymptoms,
          nationalCode: nationalCode,
          symptoms: symptoms,
        );

  Map<String, dynamic> toJson() => {
        'NCode': nationalCode,
        'FChiefComplaintList': mainSymptoms,
        'FHiExPaList': symptoms,
      };

  @override
  String toString() => toJson.toString();

  factory AppointmentBodyModel.fromEntity(AppointmentBody body) =>
      AppointmentBodyModel(
        mainSymptoms: body.mainSymptoms,
        nationalCode: body.nationalCode,
        symptoms: body.symptoms,
      );
}
