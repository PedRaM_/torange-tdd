import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import '../../../../core/repository/repository.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../../authentication/data/data_sources/authentication_local_data_source.dart';
import '../../../authentication/data/repositories/authentication_repository_impl.dart';
import '../../domain/entities/symptom_collection.dart';
import '../../domain/repositories/symptom_repository.dart';
import '../data_sources/symptoms_data_sources.dart';
import '../data_sources/symptoms_local_data_sources.dart';
import '../models/visit_config_model.dart';

class SymptomRepositoryImpl implements SymptomRepository {
  final NetworkInfo networkInfo;
  final AuthenticationLocalDataSource localAuthDS;
  final SymptomsDataSources remote;
  final SymptomsLocalDataSources local;
  final Repository repository;

  SymptomRepositoryImpl({
    @required this.networkInfo,
    @required this.localAuthDS,
    @required this.remote,
    @required this.local,
    @required this.repository,
  });

  @override
  Future<Either<Failure, SymptomCollection>> getSymptomCollection() async {
    if (!(await networkInfo.isConnected))
      return Left(ServerFailure(message: NO_INTERNET_MESSAGE));
    try {
      final tokenInfo = await localAuthDS.getLoginResultModel();
      final result = await remote.getSymptomsCollection(tokenInfo.accessToken);
      await _saveConfig(result.config);
      return Right(result);
    } on AuthorizationException {
      try {
        final tokenInfo = await localAuthDS.getLoginResultModel();
        final newToken = await repository.refreshToken(tokenInfo);
        final result = await remote.getSymptomsCollection(newToken.accessToken);
        await _saveConfig(result.config);
        return Right(result);
      } on AuthenticationException {
        return Left(AuthenticationFailure());
      } on AuthorizationException {
        return Left(AuthorizationFailure());
      }
    } on ServerException catch (error) {
      return Left(ServerFailure(message: error.message));
    }
  }

  Future<void> _saveConfig(VisitConfigModel config) async =>
      await local.saveConfigInfo(config);
}
