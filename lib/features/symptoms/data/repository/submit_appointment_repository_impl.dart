import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/repository/repository.dart';
import 'package:torange/features/authentication/data/repositories/authentication_repository_impl.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../../authentication/data/data_sources/authentication_local_data_source.dart';
import '../../domain/repositories/appointment_repository.dart';
import '../data_sources/add_appointment_data_sources.dart';
import '../models/appointment_body_model.dart';

class SubmitAppointmentRepositoryImpl extends SubmitAppointmentRepository {
  final AuthenticationLocalDataSource authenticationDS;
  final NetworkInfo networkInfo;
  final AddAppointmentDataSources addAppointmentDS;
  final Repository repository;

  SubmitAppointmentRepositoryImpl({
    @required this.authenticationDS,
    @required this.networkInfo,
    @required this.addAppointmentDS,
    @required this.repository,
  });

  @override
  Future<Either<Failure, void>> addAppointment(
      AppointmentBodyModel body) async {
    if (!(await networkInfo.isConnected))
      return Left(ServerFailure(message: NO_INTERNET_MESSAGE));
    try {
      final tokenInfo = await authenticationDS.getLoginResultModel();
      final result =
          await addAppointmentDS.addAppointment(tokenInfo.accessToken, body);
      return Right(result);
    } on AuthorizationException {
      try {
        final tokenInfo = await authenticationDS.getLoginResultModel();
        final newToken = await repository.refreshToken(tokenInfo);
        final result =
            await addAppointmentDS.addAppointment(newToken.accessToken, body);
        return Right(result);
      } on AuthenticationException {
        return Left(AuthenticationFailure());
      } on AuthorizationException {
        return Left(AuthorizationFailure());
      }
    } on ServerException catch (error) {
      return Left(ServerFailure(message: error.message));
    }
  }
}
