import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exceptions.dart';
import '../models/visit_config_model.dart';

abstract class SymptomsLocalDataSources {
  ///Gets the saved [VisitConfigModel] which gotten the last time
  ///the user had login.
  ///
  ///Throws [CachedException] if no saved data is present.
  Future<VisitConfigModel> getConfigInfo();

  Future<void> saveConfigInfo(VisitConfigModel modelToSave);
}

const CACHED_CONFIG_INFO = 'CACHED_CONFIG_INFO';

class SymptomsLocalDataSourcesImpl implements SymptomsLocalDataSources {
  final SharedPreferences sharedPreferences;

  SymptomsLocalDataSourcesImpl({@required this.sharedPreferences});

  @override
  Future<VisitConfigModel> getConfigInfo() async {
    String jsonValue = sharedPreferences.getString(CACHED_CONFIG_INFO);
    if (jsonValue == null) throw CachedException();
    return Future.value(VisitConfigModel.fromJson(json.decode(jsonValue)));
  }

  @override
  Future<void> saveConfigInfo(VisitConfigModel modelToSave) async =>
      await sharedPreferences.setString(
          CACHED_CONFIG_INFO, json.encode(modelToSave.toJson()));
}
