import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/models/base_response_model.dart';
import '../../domain/entities/symptom_collection.dart';
import '../models/symptom_collection_model.dart';

abstract class SymptomsDataSources {
  /// Call the http://torange.mubabol.ac.ir/api/torange/GetAllEntities endpoint
  /// with contains [token] in the header to get Appointments.
  ///
  /// Throws a [ServerException] for all error code.
  ///
  Future<SymptomCollection> getSymptomsCollection(String token);
}

class SymptomsDataSourcesImpl implements SymptomsDataSources {
  final http.Client client;

  SymptomsDataSourcesImpl({
    @required this.client,
  });

  @override
  Future<SymptomCollection> getSymptomsCollection(String token) async {
    final response = await client.get(
      'http://torange.mubabol.ac.ir/api/torange/GetAllEntities',
      headers: {
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      final baseResponse = BaseResponseModel<SymptomCollectionModel>.fromJson(
          json.decode(response.body),
          (data) => SymptomCollectionModel.fromJson(data));
      if (baseResponse.isSuccessful)
        return baseResponse.data;
      else
        throw ServerException(message: baseResponse.message);
    } else if (response.statusCode == 401)
      throw AuthorizationException();
    else
      throw ServerException();
  }
}
