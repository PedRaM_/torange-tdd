import 'dart:convert';

import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/models/base_response_model.dart';

import '../models/appointment_body_model.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

abstract class AddAppointmentDataSources {
  /// Calls the [http://torange.mubabol.ac.ir/api/torange/AddVisit] endpoint
  /// with contains [AppointmentBodyModel] in the body to submit an Appointment.
  ///
  /// Throws a [ServerException] for all error code.
  ///
  Future<void> addAppointment(String token, AppointmentBodyModel body);
}

class AddAppointmentDataSourcesImpl implements AddAppointmentDataSources {
  final http.Client client;

  AddAppointmentDataSourcesImpl({@required this.client});
  @override
  Future<void> addAppointment(String token, AppointmentBodyModel body) async {
    final response = await client.post(
      'http://torange.mubabol.ac.ir/api/torange/AddVisit',
      headers: {
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode(body.toJson()),
      encoding: Encoding.getByName('utf-8'),
    );
    if (response.statusCode == 200) {
      final baseResponse = BaseResponseModel<void>.fromJson(
          json.decode(response.body), (data) => null);
      if (baseResponse.isSuccessful)
        return baseResponse.data;
      else
        throw ServerException(message: baseResponse.message);
    } else if (response.statusCode == 401)
      throw AuthorizationException();
    else
      throw ServerException();
  }
}
