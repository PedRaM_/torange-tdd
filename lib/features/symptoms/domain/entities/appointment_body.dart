import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class AppointmentBody extends Equatable {
  final List<int> mainSymptoms;
  final List<int> symptoms;
  final String nationalCode;

  const AppointmentBody({
    @required this.mainSymptoms,
    @required this.symptoms,
    @required this.nationalCode,
  });

  @override
  List<Object> get props => [
        symptoms,
        mainSymptoms,
        nationalCode,
      ];
}
