import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Symptom extends Equatable {
  final int id;
  final int parentId;
  final String name;
  final List<int> childIds;

  Symptom({
    @required this.id,
    @required this.parentId,
    @required this.name,
    @required this.childIds,
  });

  @override
  List<Object> get props => [
        id,
        parentId,
        name,
        childIds,
      ];
}
