import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class VisitConfig extends Equatable {
  final bool examIsRequired;
  final bool historyIsRequired;
  final bool paraIsRequired;

  VisitConfig({
    @required this.examIsRequired,
    @required this.historyIsRequired,
    @required this.paraIsRequired,
  });

  @override
  List<Object> get props => [
        examIsRequired,
        historyIsRequired,
        paraIsRequired,
      ];
}
