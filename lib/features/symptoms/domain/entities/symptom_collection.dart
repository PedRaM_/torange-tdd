import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'symptom.dart';
import 'visit_config.dart';

class SymptomCollection extends Equatable {
  final List<Symptom> mainSymp;
  final List<Symptom> exam;
  final List<Symptom> para;
  final List<Symptom> history;
  final VisitConfig config;

  SymptomCollection({
    @required this.mainSymp,
    @required this.exam,
    @required this.para,
    @required this.history,
    @required this.config,
  });

  @override
  List<Object> get props => [
        mainSymp,
        exam,
        para,
        history,
        config,
      ];
}
