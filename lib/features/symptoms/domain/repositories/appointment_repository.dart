import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../data/models/appointment_body_model.dart';

abstract class SubmitAppointmentRepository {
  Future<Either<Failure, void>> addAppointment(AppointmentBodyModel body);
}
