import 'package:dartz/dartz.dart';
import '../../../../core/error/failures.dart';
import '../entities/symptom_collection.dart';

abstract class SymptomRepository {
  Future<Either<Failure, SymptomCollection>> getSymptomCollection();
}
