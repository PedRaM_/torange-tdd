import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../../data/models/appointment_body_model.dart';
import '../repositories/appointment_repository.dart';

class AddAppointment extends UseCase<void, Params> {
  final SubmitAppointmentRepository repository;

  AddAppointment(this.repository);
  @override
  Future<Either<Failure, void>> call(Params params) async =>
      await repository.addAppointment(params.body);
}

class Params extends NoParams {
  final AppointmentBodyModel body;

  Params({@required this.body});

  @override
  List<Object> get props => [body];
}
