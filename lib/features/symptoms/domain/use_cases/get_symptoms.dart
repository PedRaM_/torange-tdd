import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../entities/symptom_collection.dart';
import '../repositories/symptom_repository.dart';

class GetSymptoms extends UseCase<SymptomCollection, NoParams> {
  final SymptomRepository repository;
  GetSymptoms(this.repository);

  @override
  Future<Either<Failure, SymptomCollection>> call(NoParams params) async =>
      await repository.getSymptomCollection();
}
