part of 'addappointment_bloc.dart';

abstract class AddappointmentState extends Equatable {
  const AddappointmentState();

  @override
  List<Object> get props => [];
}

class AddappointmentInitial extends AddappointmentState {}

class AddAppointmentLoadingState extends AddappointmentState {}

class AddAppointmentSuccessState extends AddappointmentState {}

class AddAppointmentAuthorizationFailureState extends AddappointmentState {}

class AddAppointmentFailureState extends AddappointmentState {
  final String message;

  AddAppointmentFailureState({@required this.message});
  @override
  List<Object> get props => [];
}
