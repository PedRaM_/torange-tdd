import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:torange/features/symptoms/adapter/symptom_adapter.dart';
import 'package:torange/features/symptoms/adapter/symptom_converter.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/use_cases/use_case.dart';
import '../../../../core/utils/values.dart';
import '../../domain/entities/symptom_collection.dart';
import '../../domain/use_cases/get_symptoms.dart';

part 'symptom_event.dart';
part 'symptom_state.dart';

class SymptomBloc extends Bloc<SymptomEvent, SymptomState> {
  SymptomBloc({
    @required this.getSymptoms,
    @required this.symptomConverter,
  }) : super(SymptomInitial());

  final GetSymptoms getSymptoms;
  final SymptomConverter symptomConverter;

  @override
  Stream<SymptomState> mapEventToState(SymptomEvent event) async* {
    if (event is GetSymptomsEvent) {
      yield LoadingState();
      final result = await getSymptoms(NoParams());
      yield result.fold(
        (failure) => failure.mapToState,
        (collection) => GetSymptomsCollectionSuccessState(
          collection: collection,
          items: symptomConverter.symptomToAdapter(collection.mainSymp),
        ),
      );
    }
  }
}

extension Mapping on Failure {
  String get mapFailure {
    switch (this.runtimeType) {
      case ServerFailure:
        return (this as ServerFailure).message ??
            Strings.SERVER_FAILURE_MESSAGE;
      case AuthenticationFailure:
        return Strings.AUTHENTICATION_FAILURE_MESSAGE;
    }
    return Strings.SERVER_FAILURE_MESSAGE;
  }

  SymptomState get mapToState {
    switch (this.runtimeType) {
      case AuthorizationFailure:
        return AuthorizationFailureState();
    }
    return GetSymptomsCollectionFailureState(message: this.mapFailure);
  }
}
