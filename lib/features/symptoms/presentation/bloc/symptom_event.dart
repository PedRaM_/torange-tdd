part of 'symptom_bloc.dart';

abstract class SymptomEvent extends Equatable {
  const SymptomEvent();

  @override
  List<Object> get props => [];
}

class GetSymptomsEvent extends SymptomEvent {}
