import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:torange/features/symptoms/converter/appointment_body_converter.dart';
import 'package:torange/features/symptoms/data/models/appointment_body_model.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/utils/values.dart';
import '../../domain/use_cases/add_appointment.dart';

part 'addappointment_event.dart';
part 'addappointment_state.dart';

class AddappointmentBloc extends Bloc<AppointmentEvent, AddappointmentState> {
  AddappointmentBloc({
    @required this.addAppointment,
    @required this.converter,
  }) : super(AddappointmentInitial());
  final AddAppointment addAppointment;
  final AppointmentBodyConverter converter;

  @override
  Stream<AddappointmentState> mapEventToState(AppointmentEvent event) async* {
    if (event is AddAppointmentEvent) {
      yield AddAppointmentLoadingState();
      final convertedInput = await converter.toAppointmentBody(
        mainSymptoms: event.mainSymptoms,
        nationalCode: event.nationalCode,
        examName: event.examName,
        examSymp2: event.examSymp2,
        historyName: event.historyName,
        historySymp1: event.historySymp1,
        paraName: event.paraName,
        paraSymp3: event.paraSymp3,
      );
      yield* convertedInput.fold((failure) async* {
        failure.mapToState;
      }, (body) async* {
        final failureOrResult = await addAppointment(
            Params(body: AppointmentBodyModel.fromEntity(body)));
        yield failureOrResult.fold(
          (failure) => failure.mapToState,
          (res) => AddAppointmentSuccessState(),
        );
      });
    }
  }
}

extension Mapping on Failure {
  String get mapToString {
    switch (this.runtimeType) {
      case ServerFailure:
        return (this as ServerFailure).message ??
            Strings.SERVER_FAILURE_MESSAGE;
      case InvalidNationalCodeFailure:
        return Strings.INVALID_NATIONAL_CODE_MESSAGE;
      case AuthenticationFailure:
        return Strings.AUTHENTICATION_FAILURE_MESSAGE;
      case InvalidMainSymptomsFailure:
      case InvalidSymptomsFailure:
        return Strings.getInvalidSymptomsSelection(
            (this as InvalidSymptomsFailure).name);
    }
    return Strings.SERVER_FAILURE_MESSAGE;
  }

  AddappointmentState get mapToState {
    switch (this.runtimeType) {
      case InvalidNationalCodeFailure:
      case InvalidMainSymptomsFailure:
      case InvalidSymptomsFailure:
        return AddAppointmentFailureState(message: this.mapToString);
      case AuthorizationFailure:
        return AddAppointmentAuthorizationFailureState();
    }
    return AddAppointmentFailureState(message: this.mapToString);
  }
}
