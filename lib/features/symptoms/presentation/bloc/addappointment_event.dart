part of 'addappointment_bloc.dart';

abstract class AppointmentEvent extends Equatable {
  const AppointmentEvent();

  @override
  List<Object> get props => [];
}

class AddAppointmentEvent extends AppointmentEvent {
  final List<int> mainSymptoms;
  final String nationalCode;
  final List<int> historySymp1;
  final String historyName;
  final List<int> examSymp2;
  final String examName;
  final List<int> paraSymp3;
  final String paraName;

  const AddAppointmentEvent({
    @required this.mainSymptoms,
    @required this.nationalCode,
    @required this.historySymp1,
    @required this.historyName,
    @required this.examSymp2,
    @required this.examName,
    @required this.paraSymp3,
    @required this.paraName,
  });

  @override
  List<Object> get props => [
        mainSymptoms,
        nationalCode,
        historySymp1,
        historyName,
        examSymp2,
        examName,
        paraSymp3,
        paraName,
      ];
}
