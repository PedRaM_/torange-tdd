part of 'symptom_bloc.dart';

abstract class SymptomState extends Equatable {
  const SymptomState();

  @override
  List<Object> get props => [];
}

class SymptomInitial extends SymptomState {}

class LoadingState extends SymptomState {}

class GetSymptomsCollectionSuccessState extends SymptomState {
  final SymptomCollection collection;
  final List<SymptomAdapter> items;
  GetSymptomsCollectionSuccessState({
    @required this.collection,
    @required this.items,
  });

  @override
  List<Object> get props => [
        collection,
        items,
      ];
}

class AuthorizationFailureState extends SymptomState {}

class GetSymptomsCollectionFailureState extends SymptomState {
  final String message;

  GetSymptomsCollectionFailureState({@required this.message});
  @override
  List<Object> get props => [];
}
