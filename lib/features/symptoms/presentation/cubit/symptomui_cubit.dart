import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'symptomui_state.dart';

class SymptomuiCubit extends Cubit<SymptomuiState> {
  SymptomuiCubit() : super(SymptomuiInitial());

    void updateIsCheck(int id, bool isCheck) =>
      emit(SymptomsUiIsCheck(isCheck: isCheck, id: id));

  void updateIsOpen(int id, bool isOpen) =>
      emit(SymptomsUiIsOpen(isCheck: isOpen, id: id));
      
}
