part of 'symptomui_cubit.dart';

abstract class SymptomuiState extends Equatable {
  const SymptomuiState();

  @override
  List<Object> get props => [];
}

class SymptomuiInitial extends SymptomuiState {}

class SymptomsUiIsCheck extends SymptomuiState {
  final bool isCheck;
  final int id;

  const SymptomsUiIsCheck({
    this.isCheck,
    this.id,
  }) : assert(isCheck != null && id != null);

  @override
  List<Object> get props => [isCheck, id];

  @override
  String toString() => {
        'isCheck': isCheck,
        'id': id,
      }.toString();
}

class SymptomsUiIsOpen extends SymptomuiState {
  final bool isCheck;
  final int id;

  const SymptomsUiIsOpen({
    this.isCheck,
    this.id,
  }) : assert(isCheck != null && id != null);

  @override
  List<Object> get props => [isCheck, id];

  @override
  String toString() => {
        'isCheck': isCheck,
        'id': id,
      }.toString();
}
