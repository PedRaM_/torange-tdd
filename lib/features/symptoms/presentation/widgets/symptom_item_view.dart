import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/components/text/my_text.dart';
import '../../../../core/utils/tools.dart';
import '../../../../core/utils/values.dart';
import '../../adapter/symptom_adapter.dart';
import '../cubit/symptomui_cubit.dart';

class SymptomsItemView extends StatelessWidget {
  final SymptomAdapter item;
  final Function(SymptomAdapter) onSelectItemClick;
  final Function(SymptomAdapter) onOpenItemClick;

  const SymptomsItemView({
    Key key,
    this.item,
    this.onOpenItemClick,
    this.onSelectItemClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      textDirection: Tools.textDirection,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          textDirection: Tools.textDirection,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 4),
              child: Row(
                textDirection: Tools.textDirection,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _circle,
                  _title,
                ],
              ),
            ),
            BlocBuilder<SymptomuiCubit, SymptomuiState>(
              builder: (context, state) => AnimatedPositioned(
                duration: Duration(milliseconds: 300),
                curve: Curves.ease,
                height: isOpen ? 10 : 0,
                bottom: 0,
                right: _circleSize * 0.5,
                child: Container(
                  width: 1,
                  color: MyColors.spaceCadet.withOpacity(0.3),
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: _browser,
            ),
          ],
        ),
        _child,
      ],
    );
  }

  Widget get _circle => Builder(
        builder: (context) => InkWell(
          onTap: () {
            if (onSelectItemClick != null) onSelectItemClick(item);
          },
          customBorder: CircleBorder(),
          child: BlocConsumer<SymptomuiCubit, SymptomuiState>(
            listener: (context, state) {
              if (state is SymptomsUiIsCheck && item.id == state.id) {
                item.isSelected = state.isCheck;
              }
            },
            builder: (context, state) {
              return AnimatedContainer(
                width: _circleSize,
                height: _circleSize,
                duration: Duration(milliseconds: 300),
                curve: Curves.ease,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(_circleSize * 0.5),
                  color: isSelected ? MyColors.amazon : Colors.transparent,
                  border: Border.all(
                    color: Color(0xFFbababa),
                    width: 1.3,
                    style: isSelected ? BorderStyle.none : BorderStyle.solid,
                  ),
                ),
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 300),
                  curve: Curves.ease,
                  opacity: isSelected ? 1 : 0,
                  child: Center(
                    child: Icon(
                      MyIcons.tick,
                      color: Colors.white,
                      size: 12,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      );

  Widget get _browser => _hasChild
      ? BlocConsumer<SymptomuiCubit, SymptomuiState>(
          listener: (context, state) {
            if (state is SymptomsUiIsOpen && item.id == state.id)
              item.setIsOpen = state.isCheck;
          },
          builder: (context, state) {
            return Material(
              color: Colors.transparent,
              child: InkWell(
                customBorder: CircleBorder(),
                onTap: () {
                  if (onOpenItemClick != null) onOpenItemClick(item);
                },
                child: SizedBox(
                  width: 36,
                  height: 36,
                  child: TweenAnimationBuilder(
                    duration: Duration(milliseconds: 300),
                    curve: Curves.ease,
                    tween:
                        Tween(begin: isOpen ? pi : 0.0, end: isOpen ? pi : 0.0),
                    builder: (BuildContext context, value, Widget child) =>
                        Transform.rotate(
                      alignment: Alignment.center,
                      angle: (value),
                      child: child,
                    ),
                    child: Icon(
                      MyIcons.arrow_bottom,
                      size: 16,
                      color: MyColors.spaceCadet,
                    ),
                  ),
                ),
              ),
            );
          },
        )
      : SizedBox(
          width: 8,
          height: 36,
        );

  Widget get _title => Flexible(
        child: Padding(
          padding: EdgeInsets.only(right: 8, left: 8, top: 1),
          child: BlocBuilder<SymptomuiCubit, SymptomuiState>(
            builder: (context, state) => AnimatedDefaultTextStyle(
              duration: Duration(milliseconds: 300),
              curve: Curves.ease,
              style: TextStyle(
                color: _titleTextColor,
              ),
              child: MyText(
                text: item.name,
                textOverflow: TextOverflow.clip,
                fontSize: 13,
                fontWeight: MyFonts.bold,
              ),
            ),
          ),
        ),
      );

  Widget get _child => BlocBuilder<SymptomuiCubit, SymptomuiState>(
        builder: (context, state) =>
            isOpen ? _childBody : SizedBox(width: double.infinity),
      );

  Widget get _childBody => Padding(
        padding: EdgeInsets.only(right: _circleSize * 0.5),
        child: Stack(
          textDirection: Tools.textDirection,
          children: [
            ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.only(top: 8),
              itemBuilder: (context, index) => Row(
                textDirection: Tools.textDirection,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: _circleSize * 0.7),
                    width: 8,
                    color: MyColors.spaceCadet.withOpacity(0.3),
                    height: 1,
                  ),
                  Flexible(
                    child: SymptomsItemView(
                      item: subItems[index],
                      onOpenItemClick: (item) => onOpenItemClick(item),
                      onSelectItemClick: (item) => onSelectItemClick(item),
                    ),
                  ),
                ],
              ),
              itemCount: (subItems?.length ?? 0),
              shrinkWrap: true,
            ),
            Positioned(
              bottom: 20,
              top: 0,
              child: Container(
                width: 1,
                color: MyColors.spaceCadet.withOpacity(0.3),
              ),
            ),
          ],
        ),
      );

  int get id => item.id;
  String get name => item.name;
  bool get isSelected => item.isSelected;
  bool get isOpen => item.isOpen;
  bool get hasChild => item.hasChild;
  bool get isChildSelected => item.isChildSelected;
  List<int> get selectedIds => item.getSelectedItems;
  List<SymptomAdapter> get subItems => item.subItems;

  Color get _titleTextColor => isChildSelected || (_isInSelectedIds)
      ? MyColors.russianGreen
      : MyColors.spaceCadet;

  bool get _isInSelectedIds {
    if (selectedIds == null) return false;
    for (int i = 0; i < selectedIds.length; i++) {
      if (selectedIds[i] == id) return true;
    }
    return false;
  }

  double get _circleSize => 22;

  bool get _hasChild => hasChild ?? false;
}
