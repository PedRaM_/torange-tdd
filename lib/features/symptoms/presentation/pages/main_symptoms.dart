import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:torange/core/toast/toast.dart';
import 'package:torange/features/symptoms/presentation/cubit/symptomui_cubit.dart';

import '../../../../core/components/button/my_button.dart';
import '../../../../core/components/loading/loading.dart';
import '../../../../core/components/scaffold/my_scaffold.dart';
import '../../../../core/components/state/empty_state.dart';
import '../../../../core/components/state/error_state.dart';
import '../../../../core/components/text/my_text.dart';
import '../../../../core/utils/tools.dart';
import '../../../../core/utils/values.dart';
import '../../../authentication/presentation/pages/landing_page.dart';
import '../../adapter/symptom_adapter.dart';
import '../../domain/entities/symptom_collection.dart';
import '../bloc/symptom_bloc.dart';
import '../widgets/symptom_item_view.dart';

class AddAppointmentPage extends StatefulWidget {
  static const String routeName = "/MianSymptoms";

  @override
  _AddAppointmentPageState createState() => _AddAppointmentPageState();
}

class _AddAppointmentPageState extends State<AddAppointmentPage> {
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  Size _size;
  List<SymptomAdapter> _items = List();
  SymptomCollection _collection;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<SymptomBloc>(context).add(GetSymptomsEvent());
  }

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    return MyScaffold(
      backgroundColor: Color(0xFFFCFCFC),
      hasAppbar: true,
      hasBackArrow: true,
      titleWidget: MyText(
        text: 'شکایت اصلی',
        color: Colors.white,
        fontSize: 18,
        fontWeight: MyFonts.bold,
      ),
      scaffoldKey: _scaffoldKey,
      body: SizedBox(
        width: _size.width,
        height: double.infinity,
        child: BlocConsumer<SymptomBloc, SymptomState>(
          listener: (context, state) async {
            if (state is AuthorizationFailureState) {
              dynamic isLoggedIn = await Navigator.of(context)
                  .pushNamed(LandingPage.pageRoute, arguments: true);
              if ((isLoggedIn is bool) && isLoggedIn) {
                BlocProvider.of<SymptomBloc>(context).add(GetSymptomsEvent());
                return;
              }
              return;
            }
            if (state is GetSymptomsCollectionSuccessState) {
              _items = state.items;
              _collection = state.collection;
            }
          },
          builder: (context, state) {
            return AnimatedSwitcher(
              child: state is LoadingState
                  ? Center(
                      child: Loading(),
                    )
                  : state is GetSymptomsCollectionSuccessState
                      ? _body(state.collection)
                      : state is GetSymptomsCollectionFailureState
                          ? _errorWidget(state.message)
                          : SizedBox(),
              duration: Duration(milliseconds: 300),
              switchInCurve: Curves.easeIn,
              switchOutCurve: Curves.easeOut,
            );
          },
        ),
      ),
    );
  }

  Widget _errorWidget(String message) => ErrorState(
        key: ValueKey(1),
        message: message,
        onActionClick: () =>
            BlocProvider.of<SymptomBloc>(context).add(GetSymptomsEvent()),
      );

  Widget _body(SymptomCollection collection) => collection.mainSymp.length == 0
      ? EmptyState(
          key: ValueKey(2),
          size: Size(_size.width, _size.height),
          message: 'لیست علائم خالی می باشد، لطفا با پشتیبانی تماس بگیرید',
        )
      : Stack(
          textDirection: Tools.textDirection,
          children: [
            ListView.builder(
              key: ValueKey(3),
              padding: EdgeInsets.only(top: 12, bottom: 12 + 16 + 59.0),
              itemBuilder: (context, index) => Container(
                padding: EdgeInsets.only(right: 8, left: 4, top: 8, bottom: 8),
                margin: EdgeInsets.only(right: 16, left: 16, top: 8, bottom: 8),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x1b2e2d4d),
                      offset: Offset(0, 2),
                      blurRadius: 10,
                      spreadRadius: 0,
                    ),
                  ],
                ),
                child: SymptomsItemView(
                  item: _items[index],
                  onOpenItemClick: (item) {
                    BlocProvider.of<SymptomuiCubit>(context)
                        .updateIsOpen(item.id, !item.isOpen);
                  },
                  onSelectItemClick: (item) {
                    BlocProvider.of<SymptomuiCubit>(context)
                        .updateIsCheck(item.id, !item.isSelected);
                    if (item.hasChild && !item.isOpen && !item.isSelected)
                      BlocProvider.of<SymptomuiCubit>(context)
                          .updateIsOpen(item.id, !item.isOpen);
                  },
                ),
              ),
              itemCount: _items.length,
              shrinkWrap: true,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 16),
                child: MyButton(
                  btnHeight: 56,
                  text: "مرحله بعد",
                  loading: false,
                  colors: [MyColors.amazon, MyColors.amazon],
                  onClick: onSubmitClick,
                  width: _size.width * 0.6,
                ),
              ),
            ),
          ],
        );

  void onSubmitClick() async {
    print('$_getSelectedItemsId');
    if (_getSelectedItemsId.length == 0) {
      MyToast.showErrorToast(
          context: context, message: 'حداقل یکی از شکایات اصلی را انتخاب کنید');
      return;
    }
    // var result = await Navigator.of(context).pushNamed(
    //     AddAppointmentSecondPage.routeName,
    //     arguments: _getSelectedItemsId);
    // if (result is bool && result) {
    //   Navigator.of(context).pop(true);
    //   return;
    // }
    // BlocProvider.of<SymptomsBloc>(context)
    //     .add(ReloadSymptoms(items: _collection));
  }

  List<int> get _getSelectedItemsId {
    List<int> result = List();
    if (_items == null || _items.length == 0) return result;
    for (int i = 0; i < _items.length; i++)
      result.addAll(_items[i].getSelectedItems);
    return result;
  }
}
