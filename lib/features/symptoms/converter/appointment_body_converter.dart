import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../core/error/failures.dart';
import '../../../core/utils/input_converter.dart';
import '../data/data_sources/symptoms_local_data_sources.dart';
import '../domain/entities/appointment_body.dart';

abstract class AppointmentBodyConverter {
  Future<Either<Failure, AppointmentBody>> toAppointmentBody({
    @required String nationalCode,
    @required List<int> mainSymptoms,
    @required List<int> historySymp1,
    @required String historyName,
    @required List<int> examSymp2,
    @required String examName,
    @required List<int> paraSymp3,
    @required String paraName,
  });
}

class AppointmentBodyConverterImpl implements AppointmentBodyConverter {
  final SymptomsLocalDataSources localDS;

  AppointmentBodyConverterImpl({
    @required this.localDS,
  });

  @override
  Future<Either<Failure, AppointmentBody>> toAppointmentBody({
    @required String nationalCode,
    @required List<int> mainSymptoms,
    @required List<int> historySymp1,
    @required String historyName,
    @required List<int> examSymp2,
    @required String examName,
    @required List<int> paraSymp3,
    @required String paraName,
  }) async {
    if (nationalCode == null) return Left(InvalidNationalCodeFailure());
    if (nationalCode == '') return Left(InvalidNationalCodeFailure());
    if (!nationalCode.isNationalCodeValud)
      return Left(InvalidNationalCodeFailure());
    if (mainSymptoms == null) return Left(InvalidMainSymptomsFailure());
    if (mainSymptoms.length == 0) return Left(InvalidMainSymptomsFailure());
    bool historyIsRequired = (await localDS.getConfigInfo()).historyIsRequired;
    bool examIsRequired = (await localDS.getConfigInfo()).examIsRequired;
    bool paraIsRequired = (await localDS.getConfigInfo()).paraIsRequired;
    if (historySymp1 == null && historyIsRequired)
      return Left(InvalidSymptomsFailure(name: historyName));
    if (historySymp1.isEmpty && historyIsRequired)
      return Left(InvalidSymptomsFailure(name: historyName));
    if (examSymp2 == null && examIsRequired)
      return Left(InvalidSymptomsFailure(name: examName));
    if (examSymp2.isEmpty && examIsRequired)
      return Left(InvalidSymptomsFailure(name: examName));
    if (paraSymp3 == null && paraIsRequired)
      return Left(InvalidSymptomsFailure(name: paraName));
    if (paraSymp3.isEmpty && paraIsRequired)
      return Left(InvalidSymptomsFailure(name: paraName));
    return Right(AppointmentBody(
      mainSymptoms: mainSymptoms,
      nationalCode: nationalCode,
      symptoms: [
        ...historySymp1,
        ...examSymp2,
        ...paraSymp3,
      ],
    ));
  }
}

class InvalidSymptomsFailure extends Failure {
  final String name;

  InvalidSymptomsFailure({@required this.name});

  @override
  List<Object> get props => [name];
}

class InvalidMainSymptomsFailure extends Failure {}

class InvalidNationalCodeFailure extends Failure {}
