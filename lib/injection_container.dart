import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:torange/core/repository/repository.dart';
import 'package:torange/features/symptoms/adapter/symptom_converter.dart';
import 'package:torange/features/symptoms/data/data_sources/symptoms_data_sources.dart';
import 'package:torange/features/symptoms/data/data_sources/symptoms_local_data_sources.dart';
import 'package:torange/features/symptoms/data/repository/symptoms_repository_impl.dart';
import 'package:torange/features/symptoms/domain/repositories/symptom_repository.dart';
import 'package:torange/features/symptoms/domain/use_cases/get_symptoms.dart';
import 'package:torange/features/symptoms/presentation/bloc/symptom_bloc.dart';
import 'package:torange/features/symptoms/presentation/cubit/symptomui_cubit.dart';

import 'core/data_sources/data_sources.dart';
import 'core/network/network_info.dart';
import 'core/utils/input_converter.dart';
import 'features/appointment/data/data_sources/appointment_data_sources.dart';
import 'features/appointment/data/repository/appointment_repository_impl.dart';
import 'features/appointment/domain/repositories/appointment_repository.dart';
import 'features/appointment/domain/use_cases/get_appointment.dart';
import 'features/appointment/presentation/bloc/appointment_bloc.dart';
import 'features/authentication/data/data_sources/authentication_data_source.dart';
import 'features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'features/authentication/data/repositories/authentication_repository_impl.dart';
import 'features/authentication/domain/repositories/authentication_repository.dart';
import 'features/authentication/domain/use_cases/login.dart';
import 'features/authentication/domain/use_cases/refresh_token.dart';
import 'features/authentication/domain/use_cases/sign_up.dart';
import 'features/authentication/presentation/bloc/authentication_bloc.dart';
import 'features/authentication/presentation/cubits/page_switcher/page_switcher_cubit.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Features
  _initAuthenticationFeatures();
  _initAppointmentFeatures();
  _initSymptomsFeatures();
  //! Core
  sl.registerLazySingleton(() => InputConverter());
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton<DataSources>(() => DataSourcesImpl(client: sl()));
  sl.registerLazySingleton<Repository>(
    () => RepositoryImpl(
      dataSources: sl(),
      localAuthDS: sl(),
    ),
  );

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
}

//! Authentications
void _initAuthenticationFeatures() {
  //  Bloc
  sl.registerFactory(() => AuthenticationBloc(
        inputConverter: sl(),
        login: sl(),
        refreshToken: sl(),
        signUp: sl(),
      ));
  //  Use Cases
  sl.registerLazySingleton(() => Login(sl()));
  sl.registerLazySingleton(() => SignUp(sl()));
  sl.registerLazySingleton(() => RefreshToken(sl()));
  //  Repository
  sl.registerLazySingleton<AuthenticationRepository>(
    () => AuthenticationRepositoryImpl(
      localDataSource: sl(),
      networkInfo: sl(),
      remoteDataSource: sl(),
    ),
  );
  //  Data Soureces
  sl.registerLazySingleton<AuthenticationDataSource>(
      () => AuthenticationDataSourceImpl(client: sl()));

  sl.registerLazySingleton<AuthenticationLocalDataSource>(
      () => AuthenticationLocalDataSourceImpl(sharedPreferences: sl()));
  //  Cubit
  sl.registerFactory(() => PageSwitcherCubit());
}

//! Appointments
void _initAppointmentFeatures() {
  //  Bloc
  sl.registerFactory(() => AppointmentBloc(
        getAppointment: sl(),
      ));
  //  Use Cases
  sl.registerLazySingleton(() => GetAppointment(sl()));
  //  Repository
  sl.registerLazySingleton<AppointmentRepository>(
    () => AppointmentRepositoryImpl(
      dataSources: sl(),
      local: sl(),
      networkInfo: sl(),
      remote: sl(),
    ),
  );
  //  Data Soureces
  sl.registerLazySingleton<AppointmentDataSources>(
      () => AppointmentDataSourcesImpl(client: sl()));
}

//! Symptoms
void _initSymptomsFeatures() {
  //  Bloc
  sl.registerFactory(() => SymptomBloc(
        symptomConverter: sl(),
        getSymptoms: sl(),
      ));
  //  Cubit
  sl.registerFactory(() => SymptomuiCubit());
  //  Use Cases
  sl.registerLazySingleton(() => GetSymptoms(sl()));
  //  Repository
  sl.registerLazySingleton<SymptomRepository>(
    () => SymptomRepositoryImpl(
      repository: sl(),
      local: sl(),
      localAuthDS: sl(),
      networkInfo: sl(),
      remote: sl(),
    ),
  );
  //  Data Soureces
  sl.registerLazySingleton<SymptomsDataSources>(
      () => SymptomsDataSourcesImpl(client: sl()));

  sl.registerLazySingleton<SymptomsLocalDataSources>(
      () => SymptomsLocalDataSourcesImpl(sharedPreferences: sl()));

  // Adapter

  sl.registerLazySingleton(() => SymptomConverter());
}
