import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:torange/core/data_sources/data_sources.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';
import 'package:torange/features/authentication/data/models/refresh_token_body_model.dart';

import '../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  DataSourcesImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = DataSourcesImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.post(
      any,
      headers: anyNamed('headers'),
      body: anyNamed('body'),
      encoding: anyNamed('encoding'),
    )).thenAnswer(
        (_) async => http.Response(fixture('login_result_sucess.json'), 200));
  }

  void setUpMockHttpClientFailure404() {
    when(mockHttpClient.post(
      any,
      headers: anyNamed('headers'),
      body: anyNamed('body'),
      encoding: anyNamed('encoding'),
    )).thenAnswer((_) async => http.Response('something went wrong', 404));
  }
  group('RefreshToken', () {
    final tRefreshTokenBodyModel = RefreshTokenBodyModel(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'u',
    );

    final tLoginResultModel = LoginResultModel.fromJson(
        json.decode(fixture('login_result_cached.json')));

    test(
        'should perform POST request on URL with body and with application/json',
        () async {
      //arange
      setUpMockHttpClientSuccess200();
      //act
      dataSource.refreshToken(tRefreshTokenBodyModel);
      //assert
      verify(mockHttpClient.post(
        'http://torange.mubabol.ac.ir/api/login/GetAppToken',
        headers: {
          'Content-Type': 'application/json',
        },
        encoding: Encoding.getByName('utf-8'),
        body: jsonEncode(tRefreshTokenBodyModel.toJson()),
      ));
    });

    test('should return LoginResult when the response code is 200 (success)',
        () async {
      //arange
      setUpMockHttpClientSuccess200();
      //act
      final result = await dataSource.refreshToken(tRefreshTokenBodyModel);
      //assert
      expect(result, equals(tLoginResultModel));
    });

    test('should throw a ServerException when the response is 404 or other',
        () async {
      //arange
      setUpMockHttpClientFailure404();
      //act
      final call = dataSource.refreshToken;
      //assert
      expect(
          () => call(tRefreshTokenBodyModel), throwsA(isA<ServerException>()));
    });

    test(
        'should throw a ServerException when the response is 401 or isSuccess is false',
        () async {
      //arange
      when(mockHttpClient.post(
        any,
        headers: anyNamed('headers'),
        body: anyNamed('body'),
        encoding: Encoding.getByName('utf-8'),
      )).thenAnswer((_) async => http.Response('something went wrong', 401));
      //act
      final call = dataSource.refreshToken;
      //assert
      expect(() => call(tRefreshTokenBodyModel),
          throwsA(isA<ServerException>()));
    });
    test(
        'should throw AuthenticationException when the responseModel isSuccess is false',
        () async {
      // arange
      when(mockHttpClient.post(
        any,
        headers: anyNamed('headers'),
        body: anyNamed('body'),
        encoding: Encoding.getByName('utf-8'),
      )).thenAnswer(
        (_) async => http.Response(
          fixture('login_result_failure.json'),
          200,
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8',
          },
        ),
      );
      // act
      final call = dataSource.refreshToken;
      // assert
      expect(() => call(tRefreshTokenBodyModel),
          throwsA(isA<AuthenticationException>()));
    });
  });
}
