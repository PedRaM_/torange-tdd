import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/utils/input_converter.dart';
import 'package:torange/features/authentication/domain/entities/login_body.dart';
import 'package:torange/features/authentication/domain/entities/sign_up_body.dart';

class MockInputConverter extends Mock implements InputConverter {}

void main() {
  InputConverter inputConverter;
  setUp(() {
    inputConverter = InputConverter();
  });

  group('stringToLoginBody', () {
    test('should return LoginBody from username and password strings',
        () async {
      //arange
      final strUsername = '5689889512';
      final strPassword = '09385715855';
      //act
      final result = inputConverter.stringToLoginBody(
        password: strPassword,
        username: strUsername,
      );
      //assert
      expect(
          result,
          Right(LoginBody(
            username: strUsername,
            password: strPassword,
          )));
    });

    test('should return Failure when username is null', () async {
      //arange
      final strUsername = null;
      final strPassword = 'p';
      //act
      final result = inputConverter.stringToLoginBody(
        password: strPassword,
        username: strUsername,
      );
      //assert
      expect(result, Left(InvalidUsernameFailure()));
    });

    test('should return Failure when password is null', () async {
      //arange
      final strUsername = 'u';
      final strPassword = null;
      //act
      final result = inputConverter.stringToLoginBody(
        password: strPassword,
        username: strUsername,
      );
      //assert
      expect(result, Left(InvalidPasswordFailure()));
    });

    test('should return Failure when username is empty', () async {
      //arange
      final strUsername = '';
      final strPassword = 'p';
      //act
      final result = inputConverter.stringToLoginBody(
        password: strPassword,
        username: strUsername,
      );
      //assert
      expect(result, Left(InvalidUsernameFailure()));
    });

    test('should return Failure when password is empty', () async {
      //arange
      final strUsername = 'u';
      final strPassword = '';
      //act
      final result = inputConverter.stringToLoginBody(
        password: strPassword,
        username: strUsername,
      );
      //assert
      expect(result, Left(InvalidPasswordFailure()));
    });

    test('should return Failure when username is not valid national code',
        () async {
      //arange
      final strUsername = '5689889514';
      final strPassword = 'p';
      //act
      final result = inputConverter.stringToLoginBody(
        password: strPassword,
        username: strUsername,
      );
      //assert
      expect(result, Left(InvalidUsernameFailure()));
    });

    test('should return Failure when password is not valid phoneNumber',
        () async {
      //arange
      final strUsername = '5689889512';
      final strPassword = '093857158557';
      //act
      final result = inputConverter.stringToLoginBody(
        password: strPassword,
        username: strUsername,
      );
      //assert
      expect(result, Left(InvalidPasswordFailure()));
    });
  });

  group('stringToSignUpBody', () {
    test('should return SignUpBody from string parameters', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tStrGender = MALE_STRING;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephone = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephone,
      );
      //assert
      expect(
          result,
          Right(SignUpBody(
            birthDate: tBirthDay,
            gender: tStrGender,
            lastName: tLastName,
            mobile: tMobile,
            nCode: tNCode,
            name: tName,
            telephone: tTelephone,
          )));
    });

    test('should return SignUpBody from string parameters if gender is not set',
        () async {
      //arange
      final tBirthDay = '1373';
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(
        result,
        Right(SignUpBody(
          birthDate: tBirthDay,
          lastName: tLastName,
          mobile: tMobile,
          nCode: tNCode,
          name: tName,
          telephone: tTelephon,
        )),
      );
    });

    test('should return Failure when name is null', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = null;
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidNameFailure()));
    });

    test('should return Failure when lastName is null', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = null;
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidLastNameFailure()));
    });

    test('should return Failure when birthDay is null', () async {
      //arange
      final tBirthDay = null;
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidBirthDayFailure()));
    });

    test('should return Failure when mobile is null', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = null;
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidMobileFailure()));
    });

    test('should return Failure when nCode is null', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = null;
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidNationalCodeFailure()));
    });

    test('should return Failure when telephon is null', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = null;
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidTelephoneFailure()));
    });

    test('should return Failure when name is empty', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = '';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidNameFailure()));
    });

    test('should return Failure when lastName is empty', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = '';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidLastNameFailure()));
    });

    test('should return Failure when birthDay is empty', () async {
      //arange
      final tBirthDay = '';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidBirthDayFailure()));
    });

    test('should return Failure when mobile is empty', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidMobileFailure()));
    });

    test('should return Failure when nCode is empty', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidNationalCodeFailure()));
    });

    test('should return Failure when telephon is empty', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidTelephoneFailure()));
    });

    test('should return Failure when nCode is not valid national code',
        () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889511';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidNationalCodeFailure()));
    });

    test('should return Failure when mobile is not valid phoneNumber',
        () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '093857158558';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidMobileFailure()));
    });

    test('should return Failure when mobile is not valid telephone', () async {
      //arange
      final tBirthDay = '1373';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '011568578572';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidTelephoneFailure()));
    });

    test('should return Failure when gender is set to an unknown value',
        () async {
      //arange
      final tBirthDay = '1373';
      final tGender = 3;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5689889512';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidGenderFailure()));
    });

    test('should return Failure when date is wrong', () async {
      //arange
      final tBirthDay = '13735';
      final tGender = MALE;
      final tLastName = 'محمدعلی پور';
      final tMobile = '09385715855';
      final tNCode = '5680019426';
      final tName = 'پدرام';
      final tTelephon = '01156857857';
      //act
      final result = inputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      );
      //assert
      expect(result, Left(InvalidBirthDayFailure()));
    });
  });
}
