import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:torange/core/data_sources/data_sources.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/repository/repository.dart';
import 'package:torange/features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';
import 'package:torange/features/authentication/data/models/refresh_token_body_model.dart';
import 'package:torange/features/authentication/domain/entities/login_result.dart';

class MockLocalDataSource extends Mock
    implements AuthenticationLocalDataSource {}

class MockDataSource extends Mock implements DataSources {}

void main() {
  RepositoryImpl repository;
  MockLocalDataSource mockLocalDataSource;
  MockDataSource mockDataSource;

  setUp(() {
    mockLocalDataSource = MockLocalDataSource();
    mockDataSource = MockDataSource();
    repository = RepositoryImpl(
      dataSources: mockDataSource,
      localAuthDS: mockLocalDataSource,
    );
  });

  group('RefreshToken', () {
    final tLoginResultModel = LoginResultModel(
      username: 'n',
      accessToken: 'a',
      refreshToken: 'r',
    );

    final tRefreshTokenBodyModel = RefreshTokenBodyModel(
      accessToken: tLoginResultModel.accessToken,
      refreshToken: tLoginResultModel.refreshToken,
      username: tLoginResultModel.username,
    );

    final LoginResult tLoginResult = tLoginResultModel;

    test(
        'should return remote data when the call to remote data source is successful',
        () async {
      //arange
      when(mockDataSource.refreshToken(any))
          .thenAnswer((_) async => tLoginResultModel);
      //act
      final result = await repository.refreshToken(tLoginResultModel);
      //assert
      verify(mockDataSource.refreshToken(tRefreshTokenBodyModel));
      expect(result, equals(tLoginResult));
    });

    test(
        'should save data localy when the call to remote data source is successful',
        () async {
      //arange
      when(mockDataSource.refreshToken(any))
          .thenAnswer((_) async => tLoginResultModel);
      //act
      await repository.refreshToken(tLoginResultModel);
      //assert
      verify(mockDataSource.refreshToken(tRefreshTokenBodyModel));
      verify(mockLocalDataSource.saveLoginResult(tLoginResultModel));
    });

    test(
        'should throws a server exception when the call to remote data source is unsuccessful',
        () async {
      //arange
      when(mockDataSource.refreshToken(any)).thenThrow(ServerException());
      //act
      final call = repository.refreshToken;
      //assert
      expect(() => call(tLoginResultModel), throwsA(isA<ServerException>()));
    });
  });
}
