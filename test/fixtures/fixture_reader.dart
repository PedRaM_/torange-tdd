import 'dart:io';

String fixture(String name) {
  var dir = Directory.current.path.replaceAll('/', '\\');
  if (dir.endsWith('\\test')) {
    dir = dir.replaceAll('\\test', '');
  }
  String finalPath = '$dir\\test\\fixtures\\$name';
  return File(finalPath).readAsStringSync();
}
