import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/models/base_response_model.dart';
import 'package:torange/core/use_cases/use_case.dart';
import 'package:torange/features/appointment/data/models/appointment_model.dart';
import 'package:torange/features/appointment/domain/repositories/appointment_repository.dart';
import 'package:torange/features/appointment/domain/use_cases/get_appointment.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockAppointmentRepository extends Mock implements AppointmentRepository {}

void main() {
  GetAppointment usecase;
  MockAppointmentRepository mockAppointmentRepository;
  setUp(() {
    mockAppointmentRepository = MockAppointmentRepository();

    usecase = GetAppointment(mockAppointmentRepository);
  });

  final tAppointment =
      AppointmentModel(date: 'd', descriptions: ['d'], time: 't');
  final tAppointments = [tAppointment];

  final tBaseResponseModelSuccess = BaseResponseModel<List<AppointmentModel>>(
    isSuccessful: true,
    data: tAppointments,
    message: '',
  );

  final tBaseResponseModelFail = BaseResponseModel<List<AppointmentModel>>(
    isSuccessful: false,
    data: null,
    message: 'message',
  );

  test('should get appointments for the user from the repository', () async {
    //arange
    when(mockAppointmentRepository.getAppointments())
        .thenAnswer((_) async => Right(tAppointments));
    //act
    final result = await usecase(NoParams());
    //assert
    expect(result, Right(tAppointments));
    verify(mockAppointmentRepository.getAppointments());
    verifyNoMoreInteractions(mockAppointmentRepository);
  });

  group('fromJson', () {
    test('should return valid model from the JSON', () async {
      //arange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('appointment_success.json'));
      //act
      final result = BaseResponseModel<List<AppointmentModel>>.fromJson(
        jsonMap,
        (data) => (data as List)
            .map((item) => AppointmentModel.fromJson(item))
            .toList(),
      );
      //assert
      expect(result, tBaseResponseModelSuccess);
    });

    test('should return a valid model when the JSON response is failure',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('appointment_failure.json'));
      // act
      final result = BaseResponseModel<List<AppointmentModel>>.fromJson(
        jsonMap,
        (data) => (data as List)
            .map((item) => AppointmentModel.fromJson(item))
            .toList(),
      );
      // assert
      expect(result, tBaseResponseModelFail);
    });
  });
}
