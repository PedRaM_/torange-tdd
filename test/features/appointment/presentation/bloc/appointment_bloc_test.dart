import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/use_cases/use_case.dart';
import 'package:torange/core/utils/values.dart';
import 'package:torange/features/appointment/domain/entities/appointment.dart';
import 'package:torange/features/appointment/domain/use_cases/get_appointment.dart';
import 'package:torange/features/appointment/presentation/bloc/appointment_bloc.dart';

class MockGetAppointment extends Mock implements GetAppointment {}

void main() {
  AppointmentBloc bloc;
  MockGetAppointment mockGetAppointment;

  setUp(() {
    mockGetAppointment = MockGetAppointment();
    bloc = AppointmentBloc(
      getAppointment: mockGetAppointment,
    );
  });

  test('initialState should be empty', () {
    //assert
    expect(bloc.state, AppointmentInitial());
  });

  group('getAppointments', () {
    final tAppointments = [
      Appointment(
        date: 'd',
        descriptions: ['d'],
        time: 't',
      ),
    ];
    
    test('should get data from the getAppointment usecase', () async {
      //arange
      when(mockGetAppointment(any))
          .thenAnswer((_) async => Right(tAppointments));
      //act
      bloc.add(GetAppointmentsEvent());
      await untilCalled(mockGetAppointment(any));
      //assert
      verify(mockGetAppointment(NoParams()));
    });

    test(
        'should emit [LoadingState, GetAppointmentsSuccessState] when data is gotten successully',
        () async {
      //arange
      when(mockGetAppointment(any))
          .thenAnswer((_) async => Right(tAppointments));

      //assert later
      final expected = [
        LoadingState(),
        GetAppointmentsSuccessState(items: tAppointments)
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(GetAppointmentsEvent());
    });

    test(
        'should emit [LoadingState, GetAppointmentFailure] when getting data fails',
        () async {
      //arange
      when(mockGetAppointment(any))
          .thenAnswer((_) async => Left(ServerFailure()));

      //assert later
      final expected = [
        LoadingState(),
        GetAppointmentFailureState(message: Strings.SERVER_FAILURE_MESSAGE)
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(GetAppointmentsEvent());
    });

    test(
        'should emit [LoadingState, AuthorizationFailureState] when getting data fails with 401',
        () async {
      //arange
      when(mockGetAppointment(any))
          .thenAnswer((_) async => Left(AuthorizationFailure()));
      //assert later
      final expected = [
        LoadingState(),
        AuthorizationFailureState(),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(GetAppointmentsEvent());
    });
  });
}
