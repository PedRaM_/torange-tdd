import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/data_sources/data_sources.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/network/network_info.dart';
import 'package:torange/features/appointment/data/data_sources/appointment_data_sources.dart';
import 'package:torange/features/appointment/data/models/appointment_model.dart';
import 'package:torange/features/appointment/data/repository/appointment_repository_impl.dart';
import 'package:torange/features/appointment/domain/entities/appointment.dart';
import 'package:torange/features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';
import 'package:torange/features/authentication/data/repositories/authentication_repository_impl.dart';

class MockAppointmentDataSources extends Mock
    implements AppointmentDataSources {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

class MockAuthenticationLocalDataSource extends Mock
    implements AuthenticationLocalDataSource {}

class MockDataSources extends Mock implements DataSources {}

void main() {
  AppointmentRepositoryImpl repository;
  MockNetworkInfo mockNetworkInfo;
  MockAuthenticationLocalDataSource mockAuthenticationLocalDataSource;
  MockAppointmentDataSources mockAppointmentDataSources;
  MockDataSources mockDataSources;

  setUp(() {
    mockAppointmentDataSources = MockAppointmentDataSources();
    mockNetworkInfo = MockNetworkInfo();
    mockAuthenticationLocalDataSource = MockAuthenticationLocalDataSource();
    mockDataSources = MockDataSources();
    repository = AppointmentRepositoryImpl(
      local: mockAuthenticationLocalDataSource,
      networkInfo: mockNetworkInfo,
      remote: mockAppointmentDataSources,
      dataSources: mockDataSources,
    );
  });

  void runTestOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      body();
    });
  }

  void runTestOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      body();
    });
  }

  group('getAppointments', () {
    final tAppointmentModel = AppointmentModel(
      date: 'd',
      time: 't',
      descriptions: ['d'],
    );
    final tAppointments = [tAppointmentModel];
    final List<Appointment> tAppointment = tAppointments;
    final tLoginResultModel = LoginResultModel(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'u',
    );

    test('should call network info isConnected getter', () async {
      //arange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockAuthenticationLocalDataSource.getLoginResultModel())
          .thenAnswer((_) async => tLoginResultModel);
      //act
      repository.getAppointments();
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'should return Token from local data sources when the call to data source is authorize',
          () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        // act
        await repository.getAppointments();
        // assert
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test(
          'should return unAuthorize failure when there is no loginResult saved.',
          () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenThrow(AuthorizationException());
        // act
        final result = await repository.getAppointments();
        // assert
        expect(result, Left(AuthorizationFailure()));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test('should return data when the call to data source is successful',
          () async {
        //arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAppointmentDataSources.getAppointments(any))
            .thenAnswer((_) async => tAppointments);
        //act
        final result = await repository.getAppointments();
        //assert
        verify(mockAppointmentDataSources.getAppointments(any));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
        expect(result, equals(Right(tAppointment)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful',
          () async {
        //arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAppointmentDataSources.getAppointments(any))
            .thenThrow(ServerException());
        //act
        final result = await repository.getAppointments();
        //assert
        verify(mockAppointmentDataSources
            .getAppointments(tLoginResultModel.accessToken));
        expect(result, equals(Left(ServerFailure())));
      });
      test('should return unAuthorize failure when result is 401.', () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAppointmentDataSources.getAppointments(any))
            .thenThrow(AuthorizationException());
        when(mockDataSources.refreshToken(any))
            .thenThrow(AuthenticationException());
        // act
        final result = await repository.getAppointments();
        // assert
        expect(result, Left(AuthorizationFailure()));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test(
          'should refresh token and get data from new token info when result is 401.',
          () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAppointmentDataSources.getAppointments(any))
            .thenThrow(AuthorizationException());
        when(mockDataSources.refreshToken(any))
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAppointmentDataSources.getAppointments(any))
            .thenAnswer((_) async => tAppointments);
        // act
        final result = await repository.getAppointments();
        // assert
        expect(result, Right(tAppointments));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });
    });

    runTestOffline(() {
      test(
          'should return server failure with proper message when the device is offline',
          () async {
        //act
        final result = await repository.getAppointments();
        //assert
        verifyZeroInteractions(mockAppointmentDataSources);
        verifyZeroInteractions(mockAuthenticationLocalDataSource);
        expect(
            result, equals(Left(ServerFailure(message: NO_INTERNET_MESSAGE))));
      });
    });
  });
}
