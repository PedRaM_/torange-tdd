import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:torange/core/models/base_response_model.dart';
import 'package:torange/features/appointment/data/models/appointment_model.dart';
import 'package:torange/features/appointment/domain/entities/appointment.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tAppointmentModel = AppointmentModel(
    date: 'd',
    time: 't',
    descriptions: ['d'],
  );

  final tAppointmentModelList = [tAppointmentModel];

  final tBaseResponseModelSuccess = BaseResponseModel<List<AppointmentModel>>(
    isSuccessful: true,
    data: tAppointmentModelList,
    message: '',
  );

  final tBaseResponseModelFail = BaseResponseModel<List<AppointmentModel>>(
    isSuccessful: false,
    data: null,
    message: 'message',
  );

  test('should be subclass of Appointment', () async {
    //assert
    expect(tAppointmentModel, isA<Appointment>());
  });

  group('fromJson', () {
    test('should return a valid model when the JSON response is success',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('appointment_success.json'));
      // act
      final result = BaseResponseModel<List<AppointmentModel>>.fromJson(
          jsonMap,
          (data) => (data as List)
              .map((item) => AppointmentModel.fromJson(item))
              .toList());
      // assert
      expect(result, tBaseResponseModelSuccess);
    });

    test('should return a valid model when the JSON response is failure',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('appointment_failure.json'));
      // act
      final result = BaseResponseModel<List<AppointmentModel>>.fromJson(
          jsonMap,
          (data) => (data as List)
              .map((item) => AppointmentModel.fromJson(item))
              .toList());
      // assert
      expect(result, tBaseResponseModelFail);
    });
  });
}
