import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:torange/core/models/base_response_model.dart';
import 'package:torange/features/symptoms/data/models/symptom_collection_model.dart';
import 'package:torange/features/symptoms/data/models/symptom_model.dart';
import 'package:torange/features/symptoms/data/models/visit_config_model.dart';
import 'package:torange/features/symptoms/domain/entities/symptom_collection.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tVisitConfigModel = VisitConfigModel(
    examIsRequired: false,
    historyIsRequired: false,
    paraIsRequired: false,
  );

  final tSymptom = SymptomModel(
    childIds: null,
    id: 1,
    name: 'n',
    parentId: 0,
  );
  final tSymptom2 = SymptomModel(
    childIds: [1],
    id: 1,
    name: 'n',
    parentId: 0,
  );
  final tSymptomCollection = SymptomCollectionModel(
    config: tVisitConfigModel,
    exam: [tSymptom],
    history: [tSymptom],
    mainSymp: [tSymptom2],
    para: [tSymptom],
  );

  final tBaseResponseModelSuccess = BaseResponseModel<SymptomCollectionModel>(
    isSuccessful: true,
    data: tSymptomCollection,
    message: '',
  );

  final tBaseResponseModelFail = BaseResponseModel<SymptomCollectionModel>(
    isSuccessful: false,
    data: null,
    message: 'message',
  );

  test('should be subclass of SymptomCollection', () async {
    //assert
    expect(tSymptomCollection, isA<SymptomCollection>());
  });

  group('fromJson', () {
    test('should return a valid model when the JSON response is success',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('symptom_success.json'));
      // act
      final result = BaseResponseModel<SymptomCollectionModel>.fromJson(
          jsonMap, (map) => SymptomCollectionModel.fromJson(map));
      // assert
      expect(result, tBaseResponseModelSuccess);
    });

    test('should return a valid model when the JSON response is failure',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('login_result_failure.json'));
      // act
      final result = BaseResponseModel<SymptomCollectionModel>.fromJson(
          jsonMap, (map) => SymptomCollectionModel.fromJson(map));
      // assert
      expect(result, tBaseResponseModelFail);
    });
  });
}
