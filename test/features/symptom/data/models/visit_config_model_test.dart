import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/symptoms/data/models/visit_config_model.dart';
import 'package:torange/features/symptoms/domain/entities/visit_config.dart';

void main() {
  final tVisitConfigModel = VisitConfigModel(
    examIsRequired: false,
    historyIsRequired: false,
    paraIsRequired: false,
  );
  test('should be subclass of LoginResult class', () async {
    // assert
    expect(tVisitConfigModel, isA<VisitConfig>());
  });

  group('toMap', () {
    test('should return a JSON map containing the proper data', () async {
      // act
      final result = tVisitConfigModel.toJson();
      // assert
      final expectedMap = {
        'ExamIsRequired': false,
        'HistoryIsRequired': false,
        'ParaIsRequired': false,
      };
      expect(result, expectedMap);
    });
  });
}
