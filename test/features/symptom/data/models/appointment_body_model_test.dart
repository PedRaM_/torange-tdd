import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/symptoms/data/models/appointment_body_model.dart';
import 'package:torange/features/symptoms/domain/entities/appointment_body.dart';

void main() {
  final tAppointmentBodyModel = AppointmentBodyModel(
    mainSymptoms: [1, 2, 3],
    nationalCode: '5680019426',
    symptoms: [4, 5, 6],
  );

  test('should be subclass of AppointmentBody', () async {
    //assert
    expect(tAppointmentBodyModel, isA<AppointmentBody>());
  });

  group('toMap', () {
    test('should return a JSON map containing the proper data', () async {
      // act
      final result = tAppointmentBodyModel.toJson();
      // assert
      final expectedMap = {
        'NCode': '5680019426',
        'FChiefComplaintList': [1, 2, 3],
        'FHiExPaList': [4, 5, 6],
      };
      expect(result, expectedMap);
    });
  });
}
