import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/symptoms/data/models/symptom_model.dart';
import 'package:torange/features/symptoms/domain/entities/symptom.dart';

void main() {
  final tSymptomModel = SymptomModel(
    childIds: [1],
    id: 1,
    name: 'n',
    parentId: 0,
  );

  test('should be subclass of Symptom', () async {
    //assert
    expect(tSymptomModel, isA<Symptom>());
  });
}
