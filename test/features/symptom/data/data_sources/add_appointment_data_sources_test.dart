import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/features/symptoms/data/data_sources/add_appointment_data_sources.dart';
import 'package:torange/features/symptoms/data/models/appointment_body_model.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  AddAppointmentDataSourcesImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = AddAppointmentDataSourcesImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.post(
      any,
      headers: anyNamed('headers'),
      body: anyNamed('body'),
      encoding: anyNamed('encoding'),
    )).thenAnswer(
      (_) async => http.Response(fixture('add_appointment_success.json'), 200),
    );
  }

  void setUpHttpClientFailure401() {
    when(mockHttpClient.post(
      any,
      headers: anyNamed('headers'),
      body: anyNamed('body'),
      encoding: anyNamed('encoding'),
    )).thenAnswer((_) async => http.Response('something went wrong', 401));
  }

  void setUpHttpClientFailure400() {
    when(mockHttpClient.post(
      any,
      headers: anyNamed('headers'),
      body: anyNamed('body'),
      encoding: anyNamed('encoding'),
    )).thenAnswer((_) async => http.Response('something went wrong', 404));
  }

  group('addAppointment', () {
    final String tToken = 't';
    final tBody = AppointmentBodyModel(
        mainSymptoms: [1, 2, 3],
        nationalCode: '5680019426',
        symptoms: [4, 5, 6]);

    test(
        'should perform POST request on URL with token in header and json body',
        () async {
      // arange
      setUpMockHttpClientSuccess200();
      // act
      dataSource.addAppointment(tToken, tBody);
      // assert
      verify(mockHttpClient.post(
        'http://torange.mubabol.ac.ir/api/torange/AddVisit',
        headers: {
          'Authorization': 'Bearer $tToken',
        },
        body: jsonEncode(tBody.toJson()),
        encoding: Encoding.getByName('utf-8'),
      ));
    });

    test(
        'should return baseResponseModel with valid data when the response code is 200',
        () async {
      // arange

      setUpMockHttpClientSuccess200();
      // act
      await dataSource.addAppointment(tToken, tBody);
      // assert
      expect(null, null);
    });

    test(
        'should throw ServerException when the response is 404 or (not 200 or 401)',
        () async {
      // arange
      setUpHttpClientFailure400();
      // act
      final call = dataSource.addAppointment;
      // assert
      expect(() => call(tToken, tBody), throwsA(isA<ServerException>()));
    });

    test(
        'should throw ServerException when the responseModel isSuccess is false',
        () async {
      // arange
      when(mockHttpClient.post(
        any,
        headers: anyNamed('headers'),
        body: anyNamed('body'),
        encoding: anyNamed('encoding'),
      )).thenAnswer(
        (_) async => http.Response(
          fixture('appointment_failure.json'),
          200,
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8',
          },
        ),
      );
      // act
      final call = dataSource.addAppointment;
      // assert
      expect(() => call(tToken, tBody), throwsA(isA<ServerException>()));
    });

    test('should throw AuthorizationException when the response is 401',
        () async {
      // arange
      setUpHttpClientFailure401();
      // act
      final call = dataSource.addAppointment;
      // assert
      expect(() => call(tToken, tBody), throwsA(isA<AuthorizationException>()));
    });
  });
}
