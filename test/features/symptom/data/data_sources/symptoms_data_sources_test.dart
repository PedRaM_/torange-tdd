import 'dart:convert';
import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/models/base_response_model.dart';
import 'package:torange/features/symptoms/data/data_sources/symptoms_data_sources.dart';
import 'package:torange/features/symptoms/data/models/symptom_collection_model.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  SymptomsDataSourcesImpl dataSource;
  MockHttpClient mockHttpClient;

  setUp(() {
    mockHttpClient = MockHttpClient();
    dataSource = SymptomsDataSourcesImpl(client: mockHttpClient);
  });

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
      (_) async => http.Response(fixture('symptom_success.json'), 200),
    );
  }

  void setUpHttpClientFailure401() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('something went wrong', 401));
  }

  void setUpHttpClientFailure400() {
    when(mockHttpClient.get(any, headers: anyNamed('headers')))
        .thenAnswer((_) async => http.Response('something went wrong', 404));
  }

  group('getSymptomsCollection', () {
    final String tToken = 't';
    final tBaseResponseModel =
        BaseResponseModel<SymptomCollectionModel>.fromJson(
            json.decode(fixture('symptom_success.json')),
            (data) => SymptomCollectionModel.fromJson(data));

    test('should perform GET request on URL with token in header', () async {
      // arange
      setUpMockHttpClientSuccess200();
      // act
      dataSource.getSymptomsCollection(tToken);
      // assert
      verify(mockHttpClient.get(
        'http://torange.mubabol.ac.ir/api/torange/GetAllEntities',
        headers: {
          'Authorization': 'Bearer $tToken',
        },
      ));
    });

    test(
        'should return baseResponseModel with valid data when the response code is 200',
        () async {
      // arange

      setUpMockHttpClientSuccess200();
      // act
      final result = await dataSource.getSymptomsCollection(tToken);
      // assert
      expect(result, equals(tBaseResponseModel.data));
    });

    test(
        'should throw ServerException when the response is 404 or (not 200 or 401)',
        () async {
      // arange
      setUpHttpClientFailure400();
      // act
      final call = dataSource.getSymptomsCollection;
      // assert
      expect(() => call(tToken), throwsA(isA<ServerException>()));
    });

    test(
        'should throw ServerException when the responseModel isSuccess is false',
        () async {
      // arange
      when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
        (_) async => http.Response(
          fixture('appointment_failure.json'),
          200,
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8',
          },
        ),
      );
      // act
      final call = dataSource.getSymptomsCollection;
      // assert
      expect(() => call(tToken), throwsA(isA<ServerException>()));
    });
    

    
    test('should throw AuthorizationException when the response is 401',
        () async {
      // arange
      setUpHttpClientFailure401();
      // act
      final call = dataSource.getSymptomsCollection;
      // assert
      expect(() => call(tToken), throwsA(isA<AuthorizationException>()));
    });


  });
}
