import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/features/symptoms/data/data_sources/symptoms_local_data_sources.dart';
import 'package:torange/features/symptoms/data/models/visit_config_model.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  MockSharedPreferences mockSharedPreferences;
  SymptomsLocalDataSourcesImpl symptomsLocalDataSources;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    symptomsLocalDataSources =
        SymptomsLocalDataSourcesImpl(sharedPreferences: mockSharedPreferences);
  });

  group('getConfigInfo', () {
    final tConfigModel = VisitConfigModel.fromJson(
        json.decode(fixture('config_info_cached.json')));
    test(
        'should return ConfigModel from SharedPreferences when there is any data saved',
        () async {
      //arange
      when(mockSharedPreferences.getString(any))
          .thenReturn(fixture('config_info_cached.json'));
      //act
      final result = await symptomsLocalDataSources.getConfigInfo();
      //assert
      verify(mockSharedPreferences.getString(CACHED_CONFIG_INFO));
      expect(result, equals(tConfigModel));
    });

    test('should throw CacheException when there is not a saved data',
        () async {
      //arange
      when(mockSharedPreferences.getString(any)).thenReturn(null);
      //act
      final call = symptomsLocalDataSources.getConfigInfo;
      //assert
      expect(() => call(), throwsA(isA<CachedException>()));
    });
  });

  group('saveConfigInfo', () {
    final tConfigInfo = VisitConfigModel(
      examIsRequired: false,
      historyIsRequired: false,
      paraIsRequired: false,
    );

    test('should call SharedPreferences to save the data', () async {
      //act
      symptomsLocalDataSources.saveConfigInfo(tConfigInfo);
      //assert
      final expectedJsonString = json.encode(tConfigInfo.toJson());
      verify(mockSharedPreferences.setString(
          CACHED_CONFIG_INFO, expectedJsonString));
    });
  });
}
