import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/network/network_info.dart';
import 'package:torange/core/repository/repository.dart';
import 'package:torange/features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';
import 'package:torange/features/authentication/data/repositories/authentication_repository_impl.dart';
import 'package:torange/features/symptoms/data/data_sources/symptoms_data_sources.dart';
import 'package:torange/features/symptoms/data/data_sources/symptoms_local_data_sources.dart';
import 'package:torange/features/symptoms/data/models/symptom_collection_model.dart';
import 'package:torange/features/symptoms/data/models/symptom_model.dart';
import 'package:torange/features/symptoms/data/models/visit_config_model.dart';
import 'package:torange/features/symptoms/data/repository/symptoms_repository_impl.dart';
import 'package:torange/features/symptoms/domain/entities/symptom_collection.dart';

class MockSymptomsDataSources extends Mock implements SymptomsDataSources {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

class MockAuthenticationLocalDataSource extends Mock
    implements AuthenticationLocalDataSource {}

class MockRepository extends Mock implements Repository {}

class MockSymptomsLocalDataSources extends Mock
    implements SymptomsLocalDataSources {}

void main() {
  SymptomRepositoryImpl repository;
  MockNetworkInfo mockNetworkInfo;
  MockAuthenticationLocalDataSource mockAuthenticationLocalDataSource;
  MockSymptomsDataSources mockSymptomsDataSources;
  MockRepository mockRepository;
  MockSymptomsLocalDataSources mockLocal;

  setUp(() {
    mockNetworkInfo = MockNetworkInfo();
    mockAuthenticationLocalDataSource = MockAuthenticationLocalDataSource();
    mockSymptomsDataSources = MockSymptomsDataSources();
    mockRepository = MockRepository();
    mockLocal = MockSymptomsLocalDataSources();
    repository = SymptomRepositoryImpl(
      local: mockLocal,
      repository: mockRepository,
      localAuthDS: mockAuthenticationLocalDataSource,
      networkInfo: mockNetworkInfo,
      remote: mockSymptomsDataSources,
    );
  });

  void runTestOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      body();
    });
  }

  void runTestOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      body();
    });
  }

  group('getSymptomCollection', () {
    final tSymptom = SymptomModel(
      childIds: null,
      id: 1,
      name: 'n',
      parentId: 0,
    );
    final tSymptom2 = SymptomModel(
      childIds: [1],
      id: 1,
      name: 'n',
      parentId: 0,
    );
    final tConfig = VisitConfigModel(
      examIsRequired: false,
      historyIsRequired: false,
      paraIsRequired: false,
    );

    final tSymptomCollection = SymptomCollectionModel(
      config: tConfig,
      exam: [tSymptom],
      history: [tSymptom],
      mainSymp: [tSymptom2],
      para: [tSymptom],
    );

    final SymptomCollection tSymptoms = tSymptomCollection;

    final tLoginResultModel = LoginResultModel(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'u',
    );

    test('should call network info isConnected getter', () async {
      //arange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      when(mockSymptomsDataSources.getSymptomsCollection(any))
          .thenAnswer((_) async => tSymptoms);
      when(mockAuthenticationLocalDataSource.getLoginResultModel())
          .thenAnswer((_) async => tLoginResultModel);
      //act
      repository.getSymptomCollection();
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'should return Token from local data sources when the call to data source is authorize',
          () async {
        // arange
        when(mockSymptomsDataSources.getSymptomsCollection(any))
            .thenAnswer((_) async => tSymptoms);
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        // act
        await repository.getSymptomCollection();
        // assert
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test(
          'should return unAuthorize failure when there is no loginResult saved.',
          () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenThrow(AuthorizationException());
        // act
        final result = await repository.getSymptomCollection();
        // assert
        expect(result, Left(AuthorizationFailure()));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test('should return data when the call to data source is successful',
          () async {
        //arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockSymptomsDataSources.getSymptomsCollection(any))
            .thenAnswer((_) async => tSymptoms);
        //act
        final result = await repository.getSymptomCollection();
        //assert
        verify(mockSymptomsDataSources.getSymptomsCollection(any));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
        expect(result, equals(Right(tSymptoms)));
      });

      test(
          'should save data localy when the call to remote data source is successful',
          () async {
        //arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockSymptomsDataSources.getSymptomsCollection(any))
            .thenAnswer((_) async => tSymptoms);
        //act
        await repository.getSymptomCollection();
        //assert
        verify(mockSymptomsDataSources
            .getSymptomsCollection(tLoginResultModel.accessToken));
        verify(mockLocal.saveConfigInfo(tConfig));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful',
          () async {
        //arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockSymptomsDataSources.getSymptomsCollection(any))
            .thenThrow(ServerException());
        //act
        final result = await repository.getSymptomCollection();
        //assert
        verify(mockSymptomsDataSources
            .getSymptomsCollection(tLoginResultModel.accessToken));
        expect(result, equals(Left(ServerFailure())));
      });

      test('should return unAuthorize failure when result is 401.', () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockSymptomsDataSources.getSymptomsCollection(any))
            .thenThrow(AuthorizationException());
        when(mockRepository.refreshToken(any))
            .thenThrow(AuthenticationException());
        // act
        final result = await repository.getSymptomCollection();
        // assert
        expect(result, Left(AuthenticationFailure()));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test(
          'should refresh token and get data from new token info when result is 401.',
          () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockSymptomsDataSources.getSymptomsCollection(any))
            .thenThrow(AuthorizationException());
        when(mockRepository.refreshToken(any))
            .thenAnswer((_) async => tLoginResultModel);
        when(mockSymptomsDataSources.getSymptomsCollection(any))
            .thenAnswer((_) async => tSymptoms);
        // act
        final result = await repository.getSymptomCollection();
        // assert
        expect(result, Right(tSymptoms));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });
    });

    runTestOffline(() {
      test(
          'should return server failure with proper message when the device is offline',
          () async {
        //act
        final result = await repository.getSymptomCollection();
        //assert
        verifyZeroInteractions(mockSymptomsDataSources);
        verifyZeroInteractions(mockAuthenticationLocalDataSource);
        expect(
            result, equals(Left(ServerFailure(message: NO_INTERNET_MESSAGE))));
      });
    });
  });
}
