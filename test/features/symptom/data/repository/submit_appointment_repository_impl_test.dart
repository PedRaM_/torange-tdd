import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/network/network_info.dart';
import 'package:torange/core/repository/repository.dart';
import 'package:torange/features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';
import 'package:torange/features/authentication/data/repositories/authentication_repository_impl.dart';
import 'package:torange/features/symptoms/data/data_sources/add_appointment_data_sources.dart';
import 'package:torange/features/symptoms/data/models/appointment_body_model.dart';
import 'package:torange/features/symptoms/data/repository/submit_appointment_repository_impl.dart';

class MockAddAppointmentDataSources extends Mock
    implements AddAppointmentDataSources {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

class MockAuthenticationLocalDataSource extends Mock
    implements AuthenticationLocalDataSource {}

class MockRepository extends Mock implements Repository {}

void main() {
  MockAddAppointmentDataSources mockAddAppointmentDataSources;
  MockNetworkInfo mockNetworkInfo;
  MockAuthenticationLocalDataSource mockAuthenticationLocalDataSource;
  MockRepository mockRepository;
  SubmitAppointmentRepositoryImpl repository;

  setUp(() {
    mockNetworkInfo = MockNetworkInfo();
    mockAuthenticationLocalDataSource = MockAuthenticationLocalDataSource();
    mockRepository = MockRepository();
    mockAddAppointmentDataSources = MockAddAppointmentDataSources();
    repository = SubmitAppointmentRepositoryImpl(
      addAppointmentDS: mockAddAppointmentDataSources,
      authenticationDS: mockAuthenticationLocalDataSource,
      networkInfo: mockNetworkInfo,
      repository: mockRepository,
    );
  });

  void runTestOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      body();
    });
  }

  void runTestOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      body();
    });
  }

  group('AddAppointment', () {
    final tAppointmentBodyModel = AppointmentBodyModel(
      mainSymptoms: [1, 2, 3],
      nationalCode: '5680019426',
      symptoms: [4, 5, 6],
    );
    final tResult = null;

    final tLoginResultModel = LoginResultModel(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'u',
    );

    test('should check internet connection', () async {
      //arange
      when(mockAuthenticationLocalDataSource.getLoginResultModel())
          .thenAnswer((_) async => tLoginResultModel);
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repository.addAppointment(tAppointmentBodyModel);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'should return Token from local data sources when the call to data source is authorize',
          () async {
        // arange
        when(mockAddAppointmentDataSources.addAppointment(any, any))
            .thenAnswer((_) async => tResult);
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        // act
        await repository.addAppointment(tAppointmentBodyModel);
        // assert
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test(
          'should return unAuthorize failure when there is no loginResult saved.',
          () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenThrow(AuthorizationException());
        // act
        final result = await repository.addAppointment(tAppointmentBodyModel);
        // assert
        expect(result, Left(AuthorizationFailure()));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test('should return when the call to remote data source is successful',
          () async {
        //arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAddAppointmentDataSources.addAppointment(any, any))
            .thenAnswer((_) async => tResult);
        //act
        final result = await repository.addAppointment(tAppointmentBodyModel);
        //assert
        verify(mockAddAppointmentDataSources.addAppointment(
            tLoginResultModel.accessToken, tAppointmentBodyModel));
        expect(result, equals(Right(tResult)));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful',
          () async {
        //arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAddAppointmentDataSources.addAppointment(any, any))
            .thenThrow(ServerException());
        //act
        final result = await repository.addAppointment(tAppointmentBodyModel);
        //assert
        verify(mockAddAppointmentDataSources.addAppointment(
            tLoginResultModel.accessToken, tAppointmentBodyModel));
        expect(result, equals(Left(ServerFailure())));
      });

      test(
          'should refresh token and get data from new token info when result is 401.',
          () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAddAppointmentDataSources.addAppointment(any, any))
            .thenThrow(AuthorizationException());
        when(mockRepository.refreshToken(any))
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAddAppointmentDataSources.addAppointment(any, any))
            .thenAnswer((_) async => tResult);
        // act
        final result = await repository.addAppointment(tAppointmentBodyModel);
        // assert
        expect(result, Right(tResult));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });

      test('should return AuthenticationFailure when result is 401.', () async {
        // arange
        when(mockAuthenticationLocalDataSource.getLoginResultModel())
            .thenAnswer((_) async => tLoginResultModel);
        when(mockAddAppointmentDataSources.addAppointment(any, any))
            .thenThrow(AuthorizationException());
        when(mockRepository.refreshToken(any))
            .thenThrow(AuthenticationException());
        // act
        final result = await repository.addAppointment(tAppointmentBodyModel);
        // assert
        expect(result, Left(AuthenticationFailure()));
        verify(mockAuthenticationLocalDataSource.getLoginResultModel());
      });
    });

    runTestOffline(() {
      test(
          'should return server failure with proper message when the device is offline',
          () async {
        //act
        final result = await repository.addAppointment(tAppointmentBodyModel);
        //assert
        verifyZeroInteractions(mockRepository);
        verifyZeroInteractions(mockAuthenticationLocalDataSource);
        verifyZeroInteractions(mockAddAppointmentDataSources);
        expect(
            result, equals(Left(ServerFailure(message: NO_INTERNET_MESSAGE))));
      });
    });
  });
}
