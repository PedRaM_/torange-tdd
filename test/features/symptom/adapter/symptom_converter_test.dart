import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/symptoms/adapter/symptom_adapter.dart';
import 'package:torange/features/symptoms/adapter/symptom_converter.dart';
import 'package:torange/features/symptoms/domain/entities/symptom.dart';

void main() {
  SymptomConverter converter;

  setUp(() {
    converter = SymptomConverter();
  });

  group('symptomToAdapter', () {
    test('should return List<SymptomAdapter> from List<Symptom>', () async {
      //arange
      final items = [
        Symptom(
          id: 0,
          name: 'n',
          parentId: -1,
          childIds: [2, 3],
        )
      ];
      //act
      final result = converter.symptomToAdapter(items);
      //assert
      final expectedItems = [
        SymptomAdapter(
          id: 0,
          name: 'n',
          parentId: -1,
        ),
      ];
      expect(result, expectedItems);
    });
  });
}
