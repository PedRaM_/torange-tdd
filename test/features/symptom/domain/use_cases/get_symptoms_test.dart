import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/models/base_response_model.dart';
import 'package:torange/core/use_cases/use_case.dart';
import 'package:torange/features/symptoms/data/models/symptom_collection_model.dart';
import 'package:torange/features/symptoms/data/models/symptom_model.dart';
import 'package:torange/features/symptoms/data/models/visit_config_model.dart';
import 'package:torange/features/symptoms/domain/repositories/symptom_repository.dart';
import 'package:torange/features/symptoms/domain/use_cases/get_symptoms.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSymptomRepository extends Mock implements SymptomRepository {}

void main() {
  MockSymptomRepository mockRepository;
  GetSymptoms usecase;

  setUp(() {
    mockRepository = MockSymptomRepository();
    usecase = GetSymptoms(mockRepository);
  });
  final tSymptom = SymptomModel(
    childIds: null,
    id: 1,
    name: 'n',
    parentId: 0,
  );
  final tSymptom2 = SymptomModel(
    childIds: [1],
    id: 1,
    name: 'n',
    parentId: 0,
  );
  final tSymptomCollection = SymptomCollectionModel(
    config: VisitConfigModel(
      examIsRequired: false,
      historyIsRequired: false,
      paraIsRequired: false,
    ),
    exam: [tSymptom],
    history: [tSymptom],
    mainSymp: [tSymptom2],
    para: [tSymptom],
  );

  final tBaseResponseModelSuccess = BaseResponseModel<SymptomCollectionModel>(
    isSuccessful: true,
    data: tSymptomCollection,
    message: '',
  );

  final tBaseResponseModelFail = BaseResponseModel<SymptomCollectionModel>(
    isSuccessful: false,
    data: null,
    message: 'message',
  );

  test('should get symptom collection from the repository', () async {
    // arange
    when(mockRepository.getSymptomCollection())
        .thenAnswer((_) async => Right(tSymptomCollection));
    // act
    final result = await usecase(NoParams());
    // assert
    expect(result, Right(tSymptomCollection));
    verify(mockRepository.getSymptomCollection());
    verifyNoMoreInteractions(mockRepository);
  });

  group('toJson', () {
    test('should return a valid model from JSON', () async {
      // arange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('symptom_success.json'));
      // act
      final result = BaseResponseModel<SymptomCollectionModel>.fromJson(
        jsonMap,
        (data) => SymptomCollectionModel.fromJson(data),
      );
      // assert
      expect(result, tBaseResponseModelSuccess);
    });
    test('should return a valid model when the JSON response is failure',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('appointment_failure.json'));
      // act
      final result = BaseResponseModel<SymptomCollectionModel>.fromJson(
        jsonMap,
        (data) => SymptomCollectionModel.fromJson(data),
      );
      // assert
      expect(result, tBaseResponseModelFail);
    });
  });
}
