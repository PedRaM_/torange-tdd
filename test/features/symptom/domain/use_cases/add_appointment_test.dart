import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/features/symptoms/data/models/appointment_body_model.dart';
import 'package:torange/features/symptoms/domain/repositories/appointment_repository.dart';
import 'package:torange/features/symptoms/domain/use_cases/add_appointment.dart';

class MockSubmitAppointmentRepository extends Mock
    implements SubmitAppointmentRepository {}

void main() {
  MockSubmitAppointmentRepository repository;
  AddAppointment addAppointment;

  setUp(() {
    repository = MockSubmitAppointmentRepository();
    addAppointment = AddAppointment(repository);
  });

  final void tResult = null;
  final tBody = AppointmentBodyModel(
    mainSymptoms: [1, 2, 3],
    nationalCode: '5680019426',
    symptoms: [4, 5, 6],
  );

  test('should post symptom the repository', () async {
    // arange
    when(repository.addAppointment(any))
        .thenAnswer((_) async => Right(tResult));
    // act
    final result = await addAppointment(Params(body: tBody));
    // assert
    expect(result, Right(tResult));
    verify(repository.addAppointment(tBody));
    verifyNoMoreInteractions(repository);
  });
}
