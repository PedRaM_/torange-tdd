import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/features/symptoms/converter/appointment_body_converter.dart';
import 'package:torange/features/symptoms/data/data_sources/symptoms_local_data_sources.dart';
import 'package:torange/features/symptoms/data/models/visit_config_model.dart';
import 'package:torange/features/symptoms/domain/entities/appointment_body.dart';
import 'package:torange/features/symptoms/domain/entities/visit_config.dart';

class MockSymptomsLocalDataSources extends Mock
    implements SymptomsLocalDataSources {}

void main() {
  AppointmentBodyConverter converter;
  MockSymptomsLocalDataSources mockLocalDS;

  setUp(() {
    mockLocalDS = MockSymptomsLocalDataSources();
    converter = AppointmentBodyConverterImpl(
      localDS: mockLocalDS,
    );
  });

  group('stringAppointmentBody', () {
    VisitConfig tConfig = VisitConfigModel(
      examIsRequired: true,
      historyIsRequired: true,
      paraIsRequired: true,
    );
    test('should return AppointmentBody from inputs', () async {
      //arange
      when(mockLocalDS.getConfigInfo()).thenAnswer((_) async => tConfig);
      final strNationalCode = '5689889512';
      final tMainSymptoms = [1, 2, 3];
      final tSymptoms = [4, 5, 6];
      //act
      final result = await converter.toAppointmentBody(
        mainSymptoms: tMainSymptoms,
        nationalCode: strNationalCode,
        examName: 'exam',
        examSymp2: [tSymptoms[1]],
        historyName: 'history',
        historySymp1: [tSymptoms[0]],
        paraName: 'para',
        paraSymp3: [tSymptoms[2]],
      );
      //assert
      expect(
          result,
          Right(AppointmentBody(
            mainSymptoms: tMainSymptoms,
            nationalCode: strNationalCode,
            symptoms: tSymptoms,
          )));
    });

    test('should return Failure when nationalcode is null', () async {
      //arange
      when(mockLocalDS.getConfigInfo()).thenAnswer((_) async => tConfig);
      final strNationalCode = null;
      final tMainSymptoms = [1, 2, 3];
      final tSymptoms = [4, 5, 6];
      //act
      final result = await converter.toAppointmentBody(
        mainSymptoms: tMainSymptoms,
        nationalCode: strNationalCode,
        examName: 'exam',
        examSymp2: [tSymptoms[1]],
        historyName: 'history',
        historySymp1: [tSymptoms[0]],
        paraName: 'para',
        paraSymp3: [tSymptoms[2]],
      );
      //assert
      expect(result, Left(InvalidNationalCodeFailure()));
    });

    test('should return Failure when mainSymptoms is null', () async {
      //arange
      when(mockLocalDS.getConfigInfo()).thenAnswer((_) async => tConfig);
      final strNationalCode = '5689889512';
      final tMainSymptoms = null;
      final tSymptoms = [4, 5, 6];
      //act
      final result = await converter.toAppointmentBody(
        mainSymptoms: tMainSymptoms,
        nationalCode: strNationalCode,
        examName: 'exam',
        examSymp2: [tSymptoms[1]],
        historyName: 'history',
        historySymp1: [tSymptoms[0]],
        paraName: 'para',
        paraSymp3: [tSymptoms[2]],
      );
      //assert
      expect(result, Left(InvalidMainSymptomsFailure()));
    });

    test('should return Failure when nationalcode is empty', () async {
      //arange
      when(mockLocalDS.getConfigInfo()).thenAnswer((_) async => tConfig);
      final strNationalCode = '';
      final tMainSymptoms = [1, 2, 3];
      final tSymptoms = [4, 5, 6];
      //act
      final result = await converter.toAppointmentBody(
        mainSymptoms: tMainSymptoms,
        nationalCode: strNationalCode,
        examName: 'exam',
        examSymp2: [tSymptoms[1]],
        historyName: 'history',
        historySymp1: [tSymptoms[0]],
        paraName: 'para',
        paraSymp3: [tSymptoms[2]],
      );
      //assert
      expect(result, Left(InvalidNationalCodeFailure()));
    });

    test('should return Failure when nationalcode is not a valid NationalCode',
        () async {
      //arange
      when(mockLocalDS.getConfigInfo()).thenAnswer((_) async => tConfig);
      final strNationalCode = '5689889514';
      final tMainSymptoms = [1, 2, 3];
      final tSymptoms = [4, 5, 6];
      //act
      final result = await converter.toAppointmentBody(
        mainSymptoms: tMainSymptoms,
        nationalCode: strNationalCode,
        examName: 'exam',
        examSymp2: [tSymptoms[1]],
        historyName: 'history',
        historySymp1: [tSymptoms[0]],
        paraName: 'para',
        paraSymp3: [tSymptoms[2]],
      );
      //assert
      expect(result, Left(InvalidNationalCodeFailure()));
    });

    test('should return Failure when mainSymptoms is empty', () async {
      //arange
      when(mockLocalDS.getConfigInfo()).thenAnswer((_) async => tConfig);
      final strNationalCode = '5689889512';
      final List<int> tMainSymptoms = [];
      final tSymptoms = [4, 5, 6];
      //act
      final result = await converter.toAppointmentBody(
        mainSymptoms: tMainSymptoms,
        nationalCode: strNationalCode,
        examName: 'exam',
        examSymp2: [tSymptoms[1]],
        historyName: 'history',
        historySymp1: [tSymptoms[0]],
        paraName: 'para',
        paraSymp3: [tSymptoms[2]],
      );
      //assert
      expect(result, Left(InvalidMainSymptomsFailure()));
    });
  });
}
