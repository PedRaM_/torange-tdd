import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/utils/values.dart';
import 'package:torange/features/symptoms/converter/appointment_body_converter.dart';
import 'package:torange/features/symptoms/data/models/appointment_body_model.dart';
import 'package:torange/features/symptoms/domain/entities/appointment_body.dart';
import 'package:torange/features/symptoms/domain/use_cases/add_appointment.dart';
import 'package:torange/features/symptoms/presentation/bloc/addappointment_bloc.dart';

class MockAddAppointment extends Mock implements AddAppointment {}

class MockAppointmentBodyConverter extends Mock
    implements AppointmentBodyConverter {}

void main() {
  AddappointmentBloc bloc;
  MockAddAppointment usecase;
  MockAppointmentBodyConverter mockInputConverter;

  setUp(() {
    usecase = MockAddAppointment();
    mockInputConverter = MockAppointmentBodyConverter();
    bloc = AddappointmentBloc(
      addAppointment: usecase,
      converter: mockInputConverter,
    );
  });

  test('initialState should be empty', () {
    //assert
    expect(bloc.state, AddappointmentInitial());
  });

  group('addAppointment', () {
    final tResult = null;
    final tMianSymptom = [1, 2, 3];
    final tSymptom = [4, 5, 6];
    final tNationalCode = '5680019426';
    final tAppointmentBody = AppointmentBody(
      mainSymptoms: tMianSymptom,
      nationalCode: tNationalCode,
      symptoms: tSymptom,
    );

    void setUpMockConverterSuccess() =>
        when(mockInputConverter.toAppointmentBody(
          mainSymptoms: tMianSymptom,
          nationalCode: tNationalCode,
          examName: 'exam',
          examSymp2: [tSymptom[1]],
          historyName: 'history',
          historySymp1: [tSymptom[0]],
          paraName: 'para',
          paraSymp3: [tSymptom[2]],
        )).thenAnswer((_) async => Right(tAppointmentBody));

    test(
        'should call the Converter to validate and convert input to AppointmentBody',
        () async {
      //arange
      setUpMockConverterSuccess();
      //act
      bloc.add(AddAppointmentEvent(
        mainSymptoms: tMianSymptom,
        nationalCode: tNationalCode,
        examName: 'exam',
        examSymp2: [tSymptom[1]],
        historyName: 'history',
        historySymp1: [tSymptom[0]],
        paraName: 'para',
        paraSymp3: [tSymptom[2]],
      ));
      await untilCalled(mockInputConverter.toAppointmentBody(
        examName: anyNamed('examName'),
        examSymp2: anyNamed('examSymp2'),
        historyName: anyNamed('historyName'),
        historySymp1: anyNamed('historySymp1'),
        mainSymptoms: anyNamed('mainSymptoms'),
        nationalCode: anyNamed('nationalCode'),
        paraName: anyNamed('paraName'),
        paraSymp3: anyNamed('paraSymp3'),
      ));
      //assert
      verify(mockInputConverter.toAppointmentBody(
        mainSymptoms: tMianSymptom,
        nationalCode: tNationalCode,
        examName: 'exam',
        examSymp2: [tSymptom[1]],
        historyName: 'history',
        historySymp1: [tSymptom[0]],
        paraName: 'para',
        paraSymp3: [tSymptom[2]],
      ));
    });

    test('should post data with addAppointment usecase', () async {
      //arange
      when(usecase(any)).thenAnswer((_) async => Right(tResult));
      setUpMockConverterSuccess();
      //act
      bloc.add(AddAppointmentEvent(
        mainSymptoms: tMianSymptom,
        nationalCode: tNationalCode,
        examName: 'exam',
        examSymp2: [tSymptom[1]],
        historyName: 'history',
        historySymp1: [tSymptom[0]],
        paraName: 'para',
        paraSymp3: [tSymptom[2]],
      ));
      await untilCalled(usecase(any));
      await untilCalled(mockInputConverter.toAppointmentBody(
        examName: anyNamed('examName'),
        examSymp2: anyNamed('examSymp2'),
        historyName: anyNamed('historyName'),
        historySymp1: anyNamed('historySymp1'),
        mainSymptoms: anyNamed('mainSymptoms'),
        nationalCode: anyNamed('nationalCode'),
        paraName: anyNamed('paraName'),
        paraSymp3: anyNamed('paraSymp3'),
      ));
      //assert
      verify(usecase(
          Params(body: AppointmentBodyModel.fromEntity(tAppointmentBody))));
      verify(mockInputConverter.toAppointmentBody(
        mainSymptoms: tMianSymptom,
        nationalCode: tNationalCode,
        examName: 'exam',
        examSymp2: [tSymptom[1]],
        historyName: 'history',
        historySymp1: [tSymptom[0]],
        paraName: 'para',
        paraSymp3: [tSymptom[2]],
      ));
    });

    test(
        'should emit [AddAppointmentLoadingState, AddAppointmentSuccessState] when data is gotten successully',
        () async {
      //arange
      setUpMockConverterSuccess();
      when(usecase(any)).thenAnswer((_) async => Right(tResult));
      //assert later
      final expected = [
        AddAppointmentLoadingState(),
        AddAppointmentSuccessState()
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(AddAppointmentEvent(
        mainSymptoms: tMianSymptom,
        nationalCode: tNationalCode,
        examName: 'exam',
        examSymp2: [tSymptom[1]],
        historyName: 'history',
        historySymp1: [tSymptom[0]],
        paraName: 'para',
        paraSymp3: [tSymptom[2]],
      ));
    });

    test(
        'should emit [AddAppointmentLoadingState, AddAppointmentFailureState] when getting data fails',
        () async {
      //arange
      setUpMockConverterSuccess();
      when(usecase(any)).thenAnswer((_) async => Left(ServerFailure()));

      //assert later
      final expected = [
        AddAppointmentLoadingState(),
        AddAppointmentFailureState(message: Strings.SERVER_FAILURE_MESSAGE)
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(AddAppointmentEvent(
        mainSymptoms: tMianSymptom,
        nationalCode: tNationalCode,
        examName: 'exam',
        examSymp2: [tSymptom[1]],
        historyName: 'history',
        historySymp1: [tSymptom[0]],
        paraName: 'para',
        paraSymp3: [tSymptom[2]],
      ));
    });

    test(
        'should emit [AddAppointmentLoadingState, AddAppointmentAuthorizationFailureState] when getting data fails with 401',
        () async {
      //arange
      setUpMockConverterSuccess();
      when(usecase(any)).thenAnswer((_) async => Left(AuthorizationFailure()));
      //assert later
      final expected = [
        AddAppointmentLoadingState(),
        AddAppointmentAuthorizationFailureState(),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(AddAppointmentEvent(
        mainSymptoms: tMianSymptom,
        nationalCode: tNationalCode,
        examName: 'exam',
        examSymp2: [tSymptom[1]],
        historyName: 'history',
        historySymp1: [tSymptom[0]],
        paraName: 'para',
        paraSymp3: [tSymptom[2]],
      ));
    });
  });
}
