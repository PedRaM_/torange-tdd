import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/use_cases/use_case.dart';
import 'package:torange/core/utils/values.dart';
import 'package:torange/features/symptoms/adapter/symptom_adapter.dart';
import 'package:torange/features/symptoms/adapter/symptom_converter.dart';
import 'package:torange/features/symptoms/data/models/symptom_collection_model.dart';
import 'package:torange/features/symptoms/data/models/symptom_model.dart';
import 'package:torange/features/symptoms/data/models/visit_config_model.dart';
import 'package:torange/features/symptoms/domain/use_cases/get_symptoms.dart';
import 'package:torange/features/symptoms/presentation/bloc/symptom_bloc.dart';

class MockGetSymptoms extends Mock implements GetSymptoms {}

class MockSymptomConverter extends Mock implements SymptomConverter {}

void main() {
  SymptomBloc bloc;
  MockGetSymptoms usecase;
  MockSymptomConverter converter;

  setUp(() {
    usecase = MockGetSymptoms();
    converter = MockSymptomConverter();
    bloc = SymptomBloc(
      getSymptoms: usecase,
      symptomConverter: converter,
    );
  });

  test('initialState should be empty', () {
    //assert
    expect(bloc.state, SymptomInitial());
  });

  group('getSymptoms', () {
    final tSymptom = SymptomModel(
      childIds: null,
      id: 1,
      name: 'n',
      parentId: 0,
    );
    final tSymptom2 = SymptomModel(
      childIds: [1],
      id: 1,
      name: 'n',
      parentId: 0,
    );
    final tSymptomCollection = SymptomCollectionModel(
      config: VisitConfigModel(
        examIsRequired: false,
        historyIsRequired: false,
        paraIsRequired: false,
      ),
      exam: [tSymptom],
      history: [tSymptom],
      mainSymp: [tSymptom2],
      para: [tSymptom],
    );

    final tCollection = tSymptomCollection;
    final tItems = [
      SymptomAdapter(
        subItems: [
          SymptomAdapter(
            id: 1,
            name: 'n',
            parentId: 0,
          ),
        ],
        id: 1,
        name: 'n',
        parentId: 0,
      ),
    ];

    test('should get data from the getSymptoms usecase', () async {
      //arange
      when(usecase(any)).thenAnswer((_) async => Right(tCollection));
      //act
      bloc.add(GetSymptomsEvent());
      await untilCalled(usecase(any));
      //assert
      verify(usecase(NoParams()));
    });

    test('should call converter method', () async {
      //arange
      when(usecase(any)).thenAnswer((_) async => Right(tCollection));
      //act
      bloc.add(GetSymptomsEvent());
      await untilCalled(converter.symptomToAdapter(any));
      //assert
      verify(converter.symptomToAdapter(tSymptomCollection.mainSymp));
    });

    test(
        'should emit [LoadingState, GetSymptomsCollectionSuccessState] when data is gotten successully',
        () async {
      //arange
      when(usecase(any)).thenAnswer((_) async => Right(tCollection));
      when(converter.symptomToAdapter(any)).thenReturn(tItems);
      //assert later
      final expected = [
        LoadingState(),
        GetSymptomsCollectionSuccessState(
            collection: tCollection, items: tItems)
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(GetSymptomsEvent());
    });

    test(
        'should emit [LoadingState, GetSymptomsCollectionFailureState] when getting data fails',
        () async {
      //arange
      when(usecase(any)).thenAnswer((_) async => Left(ServerFailure()));

      //assert later
      final expected = [
        LoadingState(),
        GetSymptomsCollectionFailureState(
            message: Strings.SERVER_FAILURE_MESSAGE)
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(GetSymptomsEvent());
    });

    test(
        'should emit [LoadingState, AuthorizationFailureState] when getting data fails with 401',
        () async {
      //arange
      when(usecase(any)).thenAnswer((_) async => Left(AuthorizationFailure()));
      //assert later
      final expected = [
        LoadingState(),
        AuthorizationFailureState(),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(GetSymptomsEvent());
    });
  });
}
