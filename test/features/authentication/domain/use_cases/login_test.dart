import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/features/authentication/data/models/login_body_model.dart';
import 'package:torange/features/authentication/domain/entities/login_result.dart';
import 'package:torange/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/authentication/domain/use_cases/login.dart';

class MockAuthenticationRepository extends Mock
    implements AuthenticationRepository {}

void main() {
  Login login;
  MockAuthenticationRepository mockAuthenticationRepository;
  final tLoginBodyModel = LoginBodyModel(
    password: 'p',
    username: 'u',
  );
  final tLoginResult = LoginResult(
    accessToken: 'a',
    refreshToken: 'r',
    username: 'n',
  );

  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    login = Login(mockAuthenticationRepository);
  });

  test(
      'should get login result for the username and password from the repository',
      () async {
    // arrange
    when(mockAuthenticationRepository.login(any))
        .thenAnswer((_) async => Right(tLoginResult));
    // act
    final result = await login(Params(body: tLoginBodyModel));
    // assert
    expect(result, Right(tLoginResult));
    verify(mockAuthenticationRepository.login(tLoginBodyModel));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
