import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/features/authentication/data/models/sign_up_body_model.dart';
import 'package:torange/features/authentication/domain/entities/login_result.dart';
import 'package:torange/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/authentication/domain/use_cases/sign_up.dart';

class MockAuthenticationRepository extends Mock
    implements AuthenticationRepository {}

void main() {
  SignUp signUp;
  MockAuthenticationRepository mockAuthenticationRepository;
  final tSignUpBodyModel = SignUpBodyModel(
    birthDate: '',
    gender: '',
    lastName: '',
    mobile: '',
    nCode: '',
    name: '',
    telephone: '',
  );
  final tLoginResult = LoginResult(
    accessToken: 'a',
    refreshToken: 'r',
    username: 'n',
  );

  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    signUp = SignUp(mockAuthenticationRepository);
  });

  test('should get login result for the signUp body from the repository',
      () async {
    // arrange
    when(mockAuthenticationRepository.signUp(any))
        .thenAnswer((_) async => Right(tLoginResult));
    // act
    final result = await signUp(Params(body: tSignUpBodyModel));
    // assert
    expect(result, Right(tLoginResult));
    verify(mockAuthenticationRepository.signUp(tSignUpBodyModel));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
