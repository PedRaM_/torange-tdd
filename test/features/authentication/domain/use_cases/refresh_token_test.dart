import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:torange/features/authentication/data/models/refresh_token_body_model.dart';
import 'package:torange/features/authentication/domain/entities/login_result.dart';
import 'package:torange/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/authentication/domain/use_cases/refresh_token.dart';

class MockAuthenticationRepository extends Mock
    implements AuthenticationRepository {}

void main() {
  RefreshToken refreshToken;
  MockAuthenticationRepository mockAuthenticationRepository;
  final tRefreshTokenBodyModel = RefreshTokenBodyModel(
    accessToken: '',
    refreshToken: '',
    username: '',
  );
  final tLoginResult = LoginResult(
    accessToken: 'a',
    refreshToken: 'r',
    username: 'n',
  );

  setUp(() {
    mockAuthenticationRepository = MockAuthenticationRepository();
    refreshToken = RefreshToken(mockAuthenticationRepository);
  });

  test('should refresh token for the refresh token body from the repository',
      () async {
    // arrange
    when(mockAuthenticationRepository.refreshToken(any))
        .thenAnswer((_) async => Right(tLoginResult));
    // act
    final result = await refreshToken(Params(body: tRefreshTokenBodyModel));
    // assert
    expect(result, Right(tLoginResult));
    verify(mockAuthenticationRepository.refreshToken(tRefreshTokenBodyModel));
    verifyNoMoreInteractions(mockAuthenticationRepository);
  });
}
