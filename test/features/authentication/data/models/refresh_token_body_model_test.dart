import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/authentication/data/models/refresh_token_body_model.dart';
import 'package:torange/features/authentication/domain/entities/refresh_token_body.dart';

void main() {
  final tRefreshTokenBody = RefreshTokenBodyModel(
    accessToken: 'a',
    refreshToken: 'r',
    username: 'u',
  );

  test('should be subclass of RefreshTokenBody class', () async {
    // assert
    expect(tRefreshTokenBody, isA<RefreshTokenBody>());
  });

  group('toMap', () {
    test('should return a JSON map containing the proper data for refreshToken',
        () async {
      // act
      final result = tRefreshTokenBody.toJson();
      // assert
      final expectedMap = {
        'Username': 'u',
        'AccessToken': 'a',
        'RefreshToken': 'r',
      };
      expect(result, expectedMap);
    });
  });
}
