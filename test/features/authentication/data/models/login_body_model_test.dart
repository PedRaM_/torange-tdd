import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/authentication/data/models/login_body_model.dart';
import 'package:torange/features/authentication/domain/entities/login_body.dart';

void main() {
  final tLoginBodyModel = LoginBodyModel(
    username: 'u',
    password: 'p',
  );

  test('should be subclass of LoginBody class', () async {
    // assert
    expect(tLoginBodyModel, isA<LoginBody>());
  });

  group('toMap', () {
    test('should return a JSON map containing the proper data', () async {
      // act
      final result = tLoginBodyModel.toJson();
      // assert
      final expectedMap = {
        'Username': 'u',
        'Password': 'p',
      };
      expect(result, expectedMap);
    });
  });
}
