import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:torange/core/models/base_response_model.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';
import 'package:torange/features/authentication/domain/entities/login_result.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  final tLoginResultModel = LoginResultModel(
    accessToken: 'a',
    refreshToken: 'r',
    username: 'n',
  );

  final tBaseResponseModelSuccess = BaseResponseModel<LoginResultModel>(
    isSuccessful: true,
    data: tLoginResultModel,
    message: '',
  );

  final tBaseResponseModelFail = BaseResponseModel<LoginResultModel>(
    isSuccessful: false,
    data: null,
    message: 'message',
  );

  test('should be subclass of LoginResult class', () async {
    // assert
    expect(tLoginResultModel, isA<LoginResult>());
  });

  group('fromJson', () {
    test('should return a valid model when the JSON response is success',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('login_result_sucess.json'));
      // act
      final result = BaseResponseModel<LoginResultModel>.fromJson(
          jsonMap, (map) => LoginResultModel.fromJson(map));
      // assert
      expect(result, tBaseResponseModelSuccess);
    });

    test('should return a valid model when the JSON response is failure',
        () async {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('login_result_failure.json'));
      // act
      final result = BaseResponseModel<LoginResultModel>.fromJson(
          jsonMap, (map) => LoginResultModel.fromJson(map));
      // assert
      expect(result, tBaseResponseModelFail);
    });
  });

  group('toMap', () {
    test('should return a JSON map containing the proper data', () async {
      // act
      final result = tLoginResultModel.toJson();
      // assert
      final expectedMap = {
        'Username': 'n',
        'AccessToken': 'a',
        'RefreshToken': 'r',
      };
      expect(result, expectedMap);
    });
  });
}
