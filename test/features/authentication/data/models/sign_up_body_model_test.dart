import 'package:flutter_test/flutter_test.dart';
import 'package:torange/features/authentication/data/models/sign_up_body_model.dart';
import 'package:torange/features/authentication/domain/entities/sign_up_body.dart';

void main() {
  final tSignUpBody = SignUpBodyModel(
    birthDate: 'b',
    gender: 'g',
    lastName: 'l',
    mobile: 'm',
    nCode: 'n',
    name: 'n',
    telephone: 't',
  );

  test('should be subclass of SignUpBody class', () async {
    // assert
    expect(tSignUpBody, isA<SignUpBody>());
  });

  group('toMap', () {
    test('should return a JSON map containing the proper data', () async {
      // act
      final result = tSignUpBody.toJson();
      // assert
      final expectedMap = {
        "NCode": "n",
        "Name": "n",
        "Family": "l",
        "Sex": "g",
        "Tel": "t",
        "Mobile": "m",
        "DateB": "b"
      };
      expect(result, expectedMap);
    });
  });
}
