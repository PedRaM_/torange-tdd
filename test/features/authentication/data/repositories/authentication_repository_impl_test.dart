import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/network/network_info.dart';
import 'package:torange/features/authentication/data/data_sources/authentication_data_source.dart';
import 'package:torange/features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'package:torange/features/authentication/data/models/login_body_model.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';
import 'package:torange/features/authentication/data/models/refresh_token_body_model.dart';
import 'package:torange/features/authentication/data/models/sign_up_body_model.dart';
import 'package:torange/features/authentication/data/repositories/authentication_repository_impl.dart';
import 'package:torange/features/authentication/domain/entities/login_result.dart';

class MockRemoteDataSource extends Mock implements AuthenticationDataSource {}

class MockLocalDataSource extends Mock
    implements AuthenticationLocalDataSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  AuthenticationRepositoryImpl repository;
  MockRemoteDataSource mockRemoteDataSource;
  MockLocalDataSource mockLocalDataSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteDataSource = MockRemoteDataSource();
    mockLocalDataSource = MockLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = AuthenticationRepositoryImpl(
      remoteDataSource: mockRemoteDataSource,
      localDataSource: mockLocalDataSource,
      networkInfo: mockNetworkInfo,
    );
  });

  void runTestOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      body();
    });
  }

  void runTestOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      body();
    });
  }

  group('Login', () {
    final tLoginBodyModel = LoginBodyModel(
      username: 'u',
      password: 'p',
    );
    final tLoginResultModel = LoginResultModel(
      username: 'n',
      accessToken: 'a',
      refreshToken: 'r',
    );
    final LoginResult tLoginResult = tLoginResultModel;

    test('should call network info isConnected getter', () async {
      //arange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repository.login(tLoginBodyModel);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'should return remote data when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.login(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        final result = await repository.login(tLoginBodyModel);
        //assert
        verify(mockRemoteDataSource.login(tLoginBodyModel));
        expect(result, equals(Right(tLoginResult)));
      });

      test(
          'should save data localy when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.login(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        await repository.login(tLoginBodyModel);
        //assert
        verify(mockRemoteDataSource.login(tLoginBodyModel));
        verify(mockLocalDataSource.saveLoginResult(tLoginResultModel));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful',
          () async {
        //arange
        when(mockRemoteDataSource.login(any)).thenThrow(ServerException());
        //act
        final result = await repository.login(tLoginBodyModel);
        //assert
        verify(mockRemoteDataSource.login(tLoginBodyModel));
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(ServerFailure())));
      });
    });

    runTestOffline(() {
      test(
          'should return server failure with proper message when the device is offline',
          () async {
        //act
        final result = await repository.login(tLoginBodyModel);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        verifyZeroInteractions(mockLocalDataSource);
        expect(
            result, equals(Left(ServerFailure(message: NO_INTERNET_MESSAGE))));
      });
    });
  });

  group('Login', () {
    final tLoginBodyModel = LoginBodyModel(
      username: 'u',
      password: 'p',
    );
    final tLoginResultModel = LoginResultModel(
      username: 'n',
      accessToken: 'a',
      refreshToken: 'r',
    );
    final LoginResult tLoginResult = tLoginResultModel;

    test('should check internet connection', () async {
      //arange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repository.login(tLoginBodyModel);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'should return remote data when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.login(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        final result = await repository.login(tLoginBodyModel);
        //assert
        verify(mockRemoteDataSource.login(tLoginBodyModel));
        expect(result, equals(Right(tLoginResult)));
      });

      test(
          'should save data localy when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.login(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        await repository.login(tLoginBodyModel);
        //assert
        verify(mockRemoteDataSource.login(tLoginBodyModel));
        verify(mockLocalDataSource.saveLoginResult(tLoginResultModel));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful',
          () async {
        //arange
        when(mockRemoteDataSource.login(any)).thenThrow(ServerException());
        //act
        final result = await repository.login(tLoginBodyModel);
        //assert
        verify(mockRemoteDataSource.login(tLoginBodyModel));
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(ServerFailure())));
      });
    });

    runTestOffline(() {
      test(
          'should return server failure with proper message when the device is offline',
          () async {
        //act
        final result = await repository.login(tLoginBodyModel);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        verifyZeroInteractions(mockLocalDataSource);
        expect(
            result, equals(Left(ServerFailure(message: NO_INTERNET_MESSAGE))));
      });
    });
  });

  group('SignUp', () {
    final tSignUpBodyModel = SignUpBodyModel(
      birthDate: 'b',
      gender: 'g',
      lastName: 'l',
      mobile: 'm',
      nCode: 'n',
      name: 'n',
      telephone: 't',
    );
    final tLoginResultModel = LoginResultModel(
      username: 'n',
      accessToken: 'a',
      refreshToken: 'r',
    );
    final LoginResult tLoginResult = tLoginResultModel;

    test('should call network info isConnected getter', () async {
      //arange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repository.signUp(tSignUpBodyModel);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'should return remote data when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.signUp(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        final result = await repository.signUp(tSignUpBodyModel);
        //assert
        verify(mockRemoteDataSource.signUp(tSignUpBodyModel));
        expect(result, equals(Right(tLoginResult)));
      });

      test(
          'should save data localy when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.signUp(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        await repository.signUp(tSignUpBodyModel);
        //assert
        verify(mockRemoteDataSource.signUp(tSignUpBodyModel));
        verify(mockLocalDataSource.saveLoginResult(tLoginResultModel));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful',
          () async {
        //arange
        when(mockRemoteDataSource.signUp(any)).thenThrow(ServerException());
        //act
        final result = await repository.signUp(tSignUpBodyModel);
        //assert
        verify(mockRemoteDataSource.signUp(tSignUpBodyModel));
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(ServerFailure())));
      });
    });

    runTestOffline(() {
      test(
          'should return server failure with proper message when the device is offline',
          () async {
        //act
        final result = await repository.signUp(tSignUpBodyModel);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        verifyZeroInteractions(mockLocalDataSource);
        expect(
            result, equals(Left(ServerFailure(message: NO_INTERNET_MESSAGE))));
      });
    });
  });

  group('RefreshToken', () {
    final tRefreshTokenBodyModel = RefreshTokenBodyModel(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'u',
    );
    final tLoginResultModel = LoginResultModel(
      username: 'n',
      accessToken: 'a',
      refreshToken: 'r',
    );
    final LoginResult tLoginResult = tLoginResultModel;

    test('should call network info isConnected getter', () async {
      //arange
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repository.refreshToken(tRefreshTokenBodyModel);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'should return remote data when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.refreshToken(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        final result = await repository.refreshToken(tRefreshTokenBodyModel);
        //assert
        verify(mockRemoteDataSource.refreshToken(tRefreshTokenBodyModel));
        expect(result, equals(Right(tLoginResult)));
      });

      test(
          'should save data localy when the call to remote data source is successful',
          () async {
        //arange
        when(mockRemoteDataSource.refreshToken(any))
            .thenAnswer((_) async => tLoginResultModel);
        //act
        await repository.refreshToken(tRefreshTokenBodyModel);
        //assert
        verify(mockRemoteDataSource.refreshToken(tRefreshTokenBodyModel));
        verify(mockLocalDataSource.saveLoginResult(tLoginResultModel));
      });

      test(
          'should return server failure when the call to remote data source is unsuccessful',
          () async {
        //arange
        when(mockRemoteDataSource.refreshToken(any))
            .thenThrow(ServerException());
        //act
        final result = await repository.refreshToken(tRefreshTokenBodyModel);
        //assert
        verify(mockRemoteDataSource.refreshToken(tRefreshTokenBodyModel));
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(ServerFailure())));
      });

      test(
          'should return authentication failure when the refresh token is unAuthorize',
          () async {
        //arange
        when(mockRemoteDataSource.refreshToken(any))
            .thenThrow(AuthenticationException());
        //act
        final result = await repository.refreshToken(tRefreshTokenBodyModel);
        //assert
        verify(mockRemoteDataSource.refreshToken(tRefreshTokenBodyModel));
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(AuthenticationFailure())));
      });

      test('should return cache failure when there is no refresh token saved',
          () async {
        //arange
        when(mockRemoteDataSource.refreshToken(any))
            .thenThrow(AuthorizationException());
        //act
        final result = await repository.refreshToken(tRefreshTokenBodyModel);
        //assert
        verify(mockRemoteDataSource.refreshToken(tRefreshTokenBodyModel));
        verifyZeroInteractions(mockLocalDataSource);
        expect(result, equals(Left(AuthorizationFailure())));
      });
    });

    runTestOffline(() {
      test(
          'should return server failure with proper message when the device is offline',
          () async {
        //act
        final result = await repository.refreshToken(tRefreshTokenBodyModel);
        //assert
        verifyZeroInteractions(mockRemoteDataSource);
        verifyZeroInteractions(mockLocalDataSource);
        expect(
            result, equals(Left(ServerFailure(message: NO_INTERNET_MESSAGE))));
      });
    });
  });
}
