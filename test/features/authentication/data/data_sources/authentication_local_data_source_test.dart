import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:torange/core/error/exceptions.dart';
import 'package:torange/features/authentication/data/data_sources/authentication_local_data_source.dart';
import 'package:torange/features/authentication/data/models/login_result_model.dart';

import '../../../../fixtures/fixture_reader.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  AuthenticationLocalDataSourceImpl dataSource;
  MockSharedPreferences mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    dataSource = AuthenticationLocalDataSourceImpl(
        sharedPreferences: mockSharedPreferences);
  });

  group('getLoginResult', () {
    final tLoginResultModel = LoginResultModel.fromJson(
        json.decode(fixture('login_result_cached.json')));
    test(
        'should return LoginResult from SharedPreferences when there is any data saved',
        () async {
      //arange
      when(mockSharedPreferences.getString(any))
          .thenReturn(fixture('login_result_cached.json'));
      //act
      final result = await dataSource.getLoginResultModel();
      //assert
      verify(mockSharedPreferences.getString(CACHED_LOGIN_RESULT));
      expect(result, equals(tLoginResultModel));
    });

    test('should throw CacheException when there is not a saved data',
        () async {
      //arange
      when(mockSharedPreferences.getString(any)).thenReturn(null);
      //act
      final call = dataSource.getLoginResultModel;
      //assert
      expect(() => call(), throwsA(isA<AuthorizationException>()));
    });
  });

  group('saveLoginResult', () {
    final tLoginResultModel =
        LoginResultModel(accessToken: 'a', refreshToken: 'r', username: 'u');

    test('should call SharedPreferences to save the data', () async {
      //act
      dataSource.saveLoginResult(tLoginResultModel);
      //assert
      final expectedJsonString = json.encode(tLoginResultModel.toJson());
      verify(mockSharedPreferences.setString(
          CACHED_LOGIN_RESULT, expectedJsonString));
    });
  });

  group('isRefreshing', () {
    final tbool = true;
    test(
        'should return bool from SharedPreferences when there is any data saved',
        () async {
      //arange
      when(mockSharedPreferences.getBool(any)).thenReturn(true);
      //act
      final result = await dataSource.isRefreshing();
      //assert
      verify(mockSharedPreferences.getBool(IS_REFRESHING));
      expect(result, equals(tbool));
    });

    test('should return false when there is not a saved data', () async {
      //arange
      when(mockSharedPreferences.getBool(any)).thenReturn(null);
      //act
      final result = await dataSource.isRefreshing();
      //assert
      expect(result, equals(false));
    });
  });

  group('setRefreshing', () {
    final tbool = true;

    test('should call SharedPreferences to save the data', () async {
      //act
      dataSource.setRefreshing(tbool);
      //assert
      verify(mockSharedPreferences.setBool(IS_REFRESHING, tbool));
    });
  });

  group('isLogged', () {
    final tbool = true;
    test(
        'should return bool from SharedPreferences when there is any data saved',
        () async {
      //arange
      when(mockSharedPreferences.getBool(any)).thenReturn(true);
      //act
      final result = dataSource.isLogged();
      //assert
      verify(mockSharedPreferences.getBool(IS_LOGGED));
      expect(result, equals(tbool));
    });

    test('should return false when there is not a saved data', () async {
      //arange
      when(mockSharedPreferences.getBool(any)).thenReturn(null);
      //act
      final result = dataSource.isLogged();
      //assert
      expect(result, equals(false));
    });
  });

  group('setLogged', () {
    final tbool = true;

    test('should call SharedPreferences to save the data', () async {
      //act
      dataSource.setLogged(tbool);
      //assert
      verify(mockSharedPreferences.setBool(IS_LOGGED, tbool));
    });
  });
}
