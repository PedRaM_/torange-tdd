import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:dartz/dartz.dart';
import 'package:torange/core/error/failures.dart';
import 'package:torange/core/utils/input_converter.dart';
import 'package:torange/core/utils/values.dart';
import 'package:torange/features/authentication/data/models/login_body_model.dart';
import 'package:torange/features/authentication/data/models/refresh_token_body_model.dart';
import 'package:torange/features/authentication/data/models/sign_up_body_model.dart';
import 'package:torange/features/authentication/domain/entities/login_body.dart';
import 'package:torange/features/authentication/domain/entities/login_result.dart';
import 'package:torange/features/authentication/domain/entities/refresh_token_body.dart';
import 'package:torange/features/authentication/domain/entities/sign_up_body.dart';
import 'package:torange/features/authentication/domain/use_cases/refresh_token.dart'
    as rft;
import 'package:torange/features/authentication/domain/use_cases/sign_up.dart'
    as sgn;
import 'package:torange/features/authentication/presentation/bloc/authentication_bloc.dart';
import 'package:torange/features/authentication/domain/use_cases/login.dart'
    as lgn;

class MockLogin extends Mock implements lgn.Login {}

class MockSignUp extends Mock implements sgn.SignUp {}

class MockRefreshToken extends Mock implements rft.RefreshToken {}

class MockInputConverter extends Mock implements InputConverter {}

void main() {
  AuthenticationBloc bloc;
  MockLogin mockLogin;
  MockSignUp mockSignUp;
  MockRefreshToken mockRefreshToken;
  MockInputConverter mockInputConverter;

  setUp(() {
    mockLogin = MockLogin();
    mockSignUp = MockSignUp();
    mockRefreshToken = MockRefreshToken();
    mockInputConverter = MockInputConverter();
    bloc = AuthenticationBloc(
      login: mockLogin,
      inputConverter: mockInputConverter,
      refreshToken: mockRefreshToken,
      signUp: mockSignUp,
    );
  });

  test('initialState should be empty', () {
    //assert
    expect(bloc.state, AuthenticationInitial());
  });

  group('Login', () {
    final tUsername = '5689889512';
    final tPassword = '09385715855';
    final tLoginBody = LoginBody(
      username: '5689889512',
      password: '09385715855',
    );
    final tLoginResult = LoginResult(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'u',
    );

    void setUpMockInputConverterSuccess() =>
        when(mockInputConverter.stringToLoginBody(
                password: anyNamed('password'), username: anyNamed('username')))
            .thenReturn(Right(tLoginBody));

    test(
        'should call the inputConverter to validate and convert username and password to loginBody',
        () async {
      //arange
      setUpMockInputConverterSuccess();
      //act
      bloc.add(LoginEvent(username: tUsername, password: tPassword));
      await untilCalled(mockInputConverter.stringToLoginBody(
          password: anyNamed('password'), username: anyNamed('username')));
      //assert
      verify(mockInputConverter.stringToLoginBody(
          username: tUsername, password: tPassword));
    });

    test('should emit [NationalCodeFailureState] when username is invalid',
        () async {
      //arange
      when(mockInputConverter.stringToLoginBody(
              password: anyNamed('password'), username: anyNamed('username')))
          .thenReturn(Left(InvalidUsernameFailure()));

      //assert later
      final expected = [
        NationalCodeFailureState(
            message: Strings.INVALID_NATIONAL_CODE_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(LoginEvent(username: tUsername, password: tPassword));
    });

    test('should emit [PhoneNumberFailureState] when password is invalid',
        () async {
      //arange
      when(mockInputConverter.stringToLoginBody(
              password: anyNamed('password'), username: anyNamed('username')))
          .thenReturn(Left(InvalidPasswordFailure()));

      //assert later
      final expected = [
        PhoneNumberFailureState(message: Strings.INVALID_MOBILE_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(LoginEvent(username: tUsername, password: tPassword));
    });

    test('should get data from the login usecase', () async {
      //arange
      setUpMockInputConverterSuccess();
      when(mockLogin(any)).thenAnswer((_) async => Right(tLoginResult));
      //act
      bloc.add(LoginEvent(username: tUsername, password: tPassword));
      await untilCalled(mockLogin(any));
      //assert
      verify(
          mockLogin(lgn.Params(body: LoginBodyModel.fromEntity(tLoginBody))));
    });

    test(
        'should emit [LoadingState, LoginSuccessState] when data is gotten successully',
        () async {
      //arange
      setUpMockInputConverterSuccess();
      when(mockLogin(any)).thenAnswer((_) async => Right(tLoginResult));

      //assert later
      final expected = [LoadingState(), LoginSuccessState(body: tLoginResult)];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(LoginEvent(username: tUsername, password: tPassword));
    });

    test(
        'should emit [LoadingState, AuthenticationFailureState] when getting data fails',
        () async {
      //arange
      setUpMockInputConverterSuccess();
      when(mockLogin(any)).thenAnswer((_) async => Left(ServerFailure()));

      //assert later
      final expected = [
        LoadingState(),
        AuthenticationFailureState(message: Strings.SERVER_FAILURE_MESSAGE)
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(LoginEvent(username: tUsername, password: tPassword));
    });
  });

  group('SignUp', () {
    final tBirthDay = '1373/04/06';
    final tGender = MALE;
    final tLastName = 'محمدعلی پور';
    final tMobile = '09385715855';
    final tNCode = '5689889512';
    final tName = 'پدرام';
    final tTelephon = '01156857857';
    final tSignUpBody = SignUpBody(
      birthDate: '1373/04/06',
      gender: 'مرد',
      lastName: 'محمدعلی پور',
      mobile: '09385715855',
      nCode: '5689889512',
      name: 'پدرام',
      telephone: '01156857857',
    );
    final tLoginResult = LoginResult(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'n',
    );

    void setUpMockInputConverterSuccess() =>
        when(mockInputConverter.stringToSignUpBody(
          gender: anyNamed('gender'),
          name: anyNamed('name'),
          lastName: anyNamed('lastName'),
          birthDate: anyNamed('birthDate'),
          mobile: anyNamed('mobile'),
          nCode: anyNamed('nCode'),
          telephone: anyNamed('telephone'),
        )).thenReturn(Right(tSignUpBody));

    test(
        'should call the inputConverter to validate and convert parametes to SignUpBody',
        () async {
      //arange
      setUpMockInputConverterSuccess();
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
      await untilCalled(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      ));
      //assert
      verify(mockInputConverter.stringToSignUpBody(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should emit [NameFailureState] when name is invalid', () async {
      //arange
      when(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      )).thenReturn(Left(InvalidNameFailure()));

      //assert later
      final expected = [
        NameFailureState(message: Strings.INVALID_NAME_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should emit [LastNameFailureState] when lastName is invalid',
        () async {
      //arange
      when(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      )).thenReturn(Left(InvalidLastNameFailure()));

      //assert later
      final expected = [
        LastNameFailureState(message: Strings.INVALID_LAST_NAME_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should emit [BirthDateFailureState] when birthDate is invalid',
        () async {
      //arange
      when(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      )).thenReturn(Left(InvalidBirthDayFailure()));

      //assert later
      final expected = [
        BirthDateFailureState(message: Strings.INVALID_BIRTH_DATE_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should emit [PhoneNumberFailureState] when mobile is invalid',
        () async {
      //arange
      when(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      )).thenReturn(Left(InvalidMobileFailure()));

      //assert later
      final expected = [
        PhoneNumberFailureState(message: Strings.INVALID_MOBILE_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should emit [NationalCodeFailureState] when national code is invalid',
        () async {
      //arange
      when(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      )).thenReturn(Left(InvalidNationalCodeFailure()));

      //assert later
      final expected = [
        NationalCodeFailureState(
            message: Strings.INVALID_NATIONAL_CODE_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should emit [TelephoneFailureState] when telephone is invalid',
        () async {
      //arange
      when(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      )).thenReturn(Left(InvalidTelephoneFailure()));

      //assert later
      final expected = [
        TelephoneFailureState(message: Strings.INVALID_TELEPHONE_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should emit [GenderFailureState] when gender is invalid', () async {
      //arange
      when(mockInputConverter.stringToSignUpBody(
        gender: anyNamed('gender'),
        name: anyNamed('name'),
        lastName: anyNamed('lastName'),
        birthDate: anyNamed('birthDate'),
        mobile: anyNamed('mobile'),
        nCode: anyNamed('nCode'),
        telephone: anyNamed('telephone'),
      )).thenReturn(Left(InvalidGenderFailure()));

      //assert later
      final expected = [
        GenderFailureState(message: Strings.INVALID_GENDER_MESSAGE),
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test('should get data from the sign up usecase', () async {
      //arange
      setUpMockInputConverterSuccess();
      when(mockSignUp(any)).thenAnswer((_) async => Right(tLoginResult));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
      await untilCalled(mockSignUp(any));
      //assert
      verify(mockSignUp(
          sgn.Params(body: SignUpBodyModel.fromEntity(tSignUpBody))));
    });

    test(
        'should emit [LoadingState, SignUpSuccessState] when data is gotten successully',
        () async {
      //arange
      setUpMockInputConverterSuccess();
      when(mockSignUp(any)).thenAnswer((_) async => Right(tLoginResult));

      //assert later
      final expected = [LoadingState(), SignUpSuccessState(body: tLoginResult)];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });

    test(
        'should emit [LoadingState, AuthenticationFailureState] when getting data fails',
        () async {
      //arange
      setUpMockInputConverterSuccess();
      when(mockSignUp(any)).thenAnswer((_) async => Left(ServerFailure()));

      //assert later
      final expected = [
        LoadingState(),
        AuthenticationFailureState(message: Strings.SERVER_FAILURE_MESSAGE)
      ];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(SignUpEvent(
        birthDate: tBirthDay,
        gender: tGender,
        lastName: tLastName,
        mobile: tMobile,
        nCode: tNCode,
        name: tName,
        telephone: tTelephon,
      ));
    });
  });

  group('RefreshToken', () {
    final tUsername = '5689889512';
    final tAccessToken = 'a';
    final tRefreshToken = 'r';
    final tRefreshTokenBody = RefreshTokenBody(
      username: '5689889512',
      accessToken: 'a',
      refreshToken: 'r',
    );
    final tLoginResult = LoginResult(
      accessToken: 'a',
      refreshToken: 'r',
      username: 'u',
    );

    test('should get data from the RefreshToken usecase', () async {
      //arange
      when(mockRefreshToken(any)).thenAnswer((_) async => Right(tLoginResult));
      //act
      bloc.add(RefreshEvent(
        username: tUsername,
        accessToken: tAccessToken,
        refreshToken: tRefreshToken,
      ));
      await untilCalled(mockRefreshToken(any));
      //assert
      verify(mockRefreshToken(rft.Params(
          body: RefreshTokenBodyModel.fromEntity(tRefreshTokenBody))));
    });

    test('should emit [RefreshSuccessState] when data is gotten successully',
        () async {
      //arange
      when(mockRefreshToken(any)).thenAnswer((_) async => Right(tLoginResult));

      //assert later
      final expected = [RefreshSuccessState(body: tLoginResult)];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(RefreshEvent(
        username: tUsername,
        accessToken: tAccessToken,
        refreshToken: tRefreshToken,
      ));
    });

    test('should emit [RefreshTokenFailureState] when getting data fails',
        () async {
      //arange
      when(mockRefreshToken(any))
          .thenAnswer((_) async => Left(ServerFailure()));

      //assert later
      final expected = [RefreshTokenFailureState()];
      expectLater(bloc, emitsInOrder(expected));
      //act
      bloc.add(RefreshEvent(
        username: tUsername,
        accessToken: tAccessToken,
        refreshToken: tRefreshToken,
      ));
    });
  });
}
